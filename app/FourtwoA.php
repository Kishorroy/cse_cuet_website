<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FourtwoA extends Model
{
    public $timestamps=false;
    protected $fillable = [
        'sub_title', 'teacher_name', 'day','time',
    ];
}
