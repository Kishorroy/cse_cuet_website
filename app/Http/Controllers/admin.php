<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;



class admin extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function teacherRegsForm(){
        if(Auth::user()->chk!=2){
            redirect('/home1');
        }
        else return view('teacherReg');
    }

    public function teacherRegs(Request $request){
        $this->validator($request->all());
        $this->create($request->all());
        return redirect('sign_out');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'chk' =>0,
        ]);
    }
}
