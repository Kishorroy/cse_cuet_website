<?php

namespace App\Http\Controllers;

use App\ThreetwoB;
use Illuminate\Http\Request;

class ThreetwoBcontroller extends Controller
{
    public function store(){
        $objThreetwoB=new ThreetwoB();
        $objThreetwoB->sub_title=$_POST['sub_title'];
        $objThreetwoB->teacher_name=$_POST['teacher_name'];
        $objThreetwoB->day=$_POST['day'];
        $objThreetwoB->time=$_POST['time'];
        $status=$objThreetwoB->save();
        return redirect()->route('ThreetwoBroutine');
    }
    public function index(){
        $objThreetwoB=new ThreetwoB();
        $allData=$objThreetwoB->paginate(20);
        return view("ThreetwoB/index",compact('allData'));
    }
    public function index1(){
        $objThreetwoB=new ThreetwoB();
        $allData=$objThreetwoB->paginate(20);
        return view("ThreetwoB/index1",compact('allData'));
    }

    public function view4Edit($id){
        $objThreetwoB=new ThreetwoB();
        $oneData=$objThreetwoB->find($id);
        return view("ThreetwoB/edit",compact('oneData'));
    }

    public function update(){
        $objThreetwoB=new ThreetwoB();
        $oneData=$objThreetwoB->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('ThreetwoBroutine');
    }

    public function delete($id){
        $objThreetwoB=new ThreetwoB();
        $oneData=$objThreetwoB->find($id)->delete();
        return redirect()->route('ThreetwoBroutine');
    }

}
