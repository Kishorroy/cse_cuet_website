<?php

namespace App\Http\Controllers;

use App\ThreeoneB;
use Illuminate\Http\Request;

class ThreeoneBcontroller extends Controller
{
    public function store(){
        $objThreeoneB=new ThreeoneB();
        $objThreeoneB->sub_title=$_POST['sub_title'];
        $objThreeoneB->teacher_name=$_POST['teacher_name'];
        $objThreeoneB->day=$_POST['day'];
        $objThreeoneB->time=$_POST['time'];
        $status=$objThreeoneB->save();
        return redirect()->route('ThreeoneBroutine');
    }
    public function index(){
        $objThreeoneB=new ThreeoneB();
        $allData=$objThreeoneB->paginate(20);
        return view("ThreeoneB/index",compact('allData'));
    }
    public function index1(){
        $objThreeoneB=new ThreeoneB();
        $allData=$objThreeoneB->paginate(20);
        return view("ThreeoneB/index1",compact('allData'));
    }

    public function view4Edit($id){
        $objThreeoneB=new ThreeoneB();
        $oneData=$objThreeoneB->find($id);
        return view("ThreeoneB/edit",compact('oneData'));
    }

    public function update(){
        $objThreeoneB=new ThreeoneB();
        $oneData=$objThreeoneB->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('ThreeoneBroutine');
    }

    public function delete($id){
        $objThreeoneB=new ThreeoneB();
        $oneData=$objThreeoneB->find($id)->delete();
        return redirect()->route('ThreeoneBroutine');
    }

}
