<?php

namespace App\Http\Controllers;

use App\ThreeoneA;
use Illuminate\Http\Request;

class ThreeoneAcontroller extends Controller
{
    public function store(){
        $objThreeoneA=new ThreeoneA();
        $objThreeoneA->sub_title=$_POST['sub_title'];
        $objThreeoneA->teacher_name=$_POST['teacher_name'];
        $objThreeoneA->day=$_POST['day'];
        $objThreeoneA->time=$_POST['time'];
        $status=$objThreeoneA->save();
        return redirect()->route('ThreeoneAroutine');
    }
    public function index(){
        $objThreeoneA=new ThreeoneA();
        $allData=$objThreeoneA->paginate(20);
        return view("ThreeoneA/index",compact('allData'));
    }
    public function index1(){
        $objThreeoneA=new ThreeoneA();
        $allData=$objThreeoneA->paginate(20);
        return view("ThreeoneA/index1",compact('allData'));
    }

    public function view4Edit($id){
        $objThreeoneA=new ThreeoneA();
        $oneData=$objThreeoneA->find($id);
        return view("ThreeoneA/edit",compact('oneData'));
    }

    public function update(){
        $objThreeoneA=new ThreeoneA();
        $oneData=$objThreeoneA->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('ThreeoneAroutine');
    }

    public function delete($id){
        $objThreeoneA=new ThreeoneA();
        $oneData=$objThreeoneA->find($id)->delete();
        return redirect()->route('ThreeoneAroutine');
    }

}
