<!DOCTYPE html>
<html>
<head>

    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:710px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .modal1 {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content1 {
            position: relative;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
            margin: 20px;
            padding: 0;
            border: 1px solid #888;
            width: 80%;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {top:-300px; opacity:0}
            to {top:0; opacity:1}
        }

        @keyframes animatetop {
            from {top:-300px; opacity:0}
            to {top:0; opacity:1}
        }

        /* The Close Button */
        .close1 {
            color: white;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close1:hover,
        .close1:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        .modal-header1 {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }
        .modal-body1{
            margin-left: 10px;
        }



        .modal-footer1 {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;

        }
    </style>

    <script>
        $(document).ready(function(){

            $("#div1").fadeIn();
            $("#div2").fadeIn("slow");
            $("#div3").fadeIn(3000);

        });
    </script>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li ><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="notice.blade.php">Notice Board</a></li>
                    <li><a href="upcomingevent.blade.php">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li class="active"><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>
    <div class="info" style="height: 460px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">

        <center>

            <br><br>
            <div id="div1" style="width:600px;height:100px;display:none;background: linear-gradient(#ef9a9a ,#e57373 , #ef5350);"><a href="#" id="myBtn1" style="color: white; font-size: 25px; margin-top: 10px;"><b><br>Research Areas</b></a>
                <div id="myModal1" class="modal1">

                    <!-- Modal content -->
                    <div class="modal-content1">
                        <div class="modal-header1">
                            <span class="close1">&times;</span>
                            <h2>Research Areas</h2>
                        </div>
                        <div class="modal-body1" ><br>
                            <p>&#9679; &nbsp;Algorithms and Theory</p>
                            <p>&#9679; &nbsp;Artificial Intelligence</p>
                            <p>&#9679; &nbsp;Bioinformatics</p>
                            <p>&#9679; &nbsp;Computer Graphics</p>
                            <p>&#9679; &nbsp;Computer Vision</p>
                            <p>&#9679; &nbsp;Cryptography and Security</p>
                            <p>&#9679; &nbsp;Data Mining</p>
                            <p>&#9679; &nbsp;Database and Information Systems</p>
                            <p>&#9679; &nbsp;Human Computer Interaction</p>
                            <p>&#9679; &nbsp;Image Processing</p>
                            <p>&#9679; &nbsp;Pattern Recognition</p>
                            <p>&#9679; &nbsp;Programming Language</p>
                            <p>&#9679; &nbsp;Software Engineering</p>
                            <p>&#9679; &nbsp;Systems and Networking</p>
                            <p>&#9679; &nbsp;Ubiquitous and Cloud Computing</p>

                        </div>
                        <div class="modal-footer1">
                            <h5><p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p></h5>
                        </div>
                    </div>

                </div>

                <script>
                    // Get the modal
                    var modal1 = document.getElementById('myModal1');

                    // Get the button that opens the modal
                    var btn = document.getElementById("myBtn1");

                    // Get the <span> element that closes the modal
                    var span = document.getElementsByClassName("close1")[0];

                    // When the user clicks the button, open the modal
                    btn.onclick = function() {
                        modal1.style.display = "block";
                    }

                    // When the user clicks on <span> (x), close the modal
                    span.onclick = function() {
                        modal1.style.display = "none";
                    }

                    // When the user clicks anywhere outside of the modal, close it
                    window.onclick = function(event) {
                        if (event.target == modal1) {
                            modal1.style.display = "none";
                        }
                    }
                </script>

            </div><br><br>
            <div id="div2" style="width:600px;height:100px;display:none;background: linear-gradient(#a5d6a7 ,#81c784 , #66bb6a);"><a href="research1.blade.php" style="color: white;font-size: 25px;margin-top: 10px;"><b><br>Publications</b></a></div><br><br>
            <div id="div3" style="width:600px;height:100px;display:none;background: linear-gradient(#4fc3f7 ,#29b6f6 , #039be5);"><a href="faculty.blade.php" style="color: white;font-size: 25px;margin-top: 10px;"><b><br>Researcher Info</b></a></div>
        </center>
    </div>
    <div class="footer" style="height:60px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>
