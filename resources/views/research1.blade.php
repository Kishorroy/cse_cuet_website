<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>
    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:710px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 780px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .center {
            text-align: center;
        }

        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #000;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {background-color: #ddd;}
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li class="active"><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>
    <div class="map">
        <div class="center" >
            <br>
            <table id="customers">
                <tr>
                    <th>Publication Type</th>
                    <th><center>Publication Details</center></th>
                    <th>Year</th>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>S. A. Chowdhury, M. N. Uddin, M. M. S. Kowsar and K. Deb, "Occlusion handling and human detection based on Histogram of Oriented Gradients for automatic video surveillance", 2016 International Conference on Innovations in Science, Engineering and Technology (ICISET) pp. 1-4., Dhaka, Bangladesh, 2016</td>
                    <td>2016</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Kaushik Deb, S.M. Towhidul Islam, Kazi Zakia Sultana and Kang-Hyun Jo, "Stairway Detection Based on Extracting Longest Increasing Subsequence of Horizontal Edges", Studies in Computational Intelligence, ISSN : 1860-9503 (Online) Vol. 489, pp. 213-218, Springer, Germany, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Kaushik Deb, Md. Sajib Al-Seraj, and Ui-Pil Chong, "A Low Frequency Band Watermarking with Weighted Correction in the Combined Cosine and Wavelet Transform Domain", Journal of the Institute of Signal Processing and Systems, ISSN: 1229-9480 Vol. 1, pp. 13-20, The Korea Institute of Signal Processing and Systems (KISPS), South Korea, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Md. Moshiul Hoque, Y. Kobayashi, Y. Kuno, and Kaushik Deb, "Design A Robotic Head to Attract Human Attention by Considering Viewing Situations", Computer Science & Engineering Research Journal (CSERJ), ISSN: 1990-4010 Vol. 8, pp. 11-21, Bangladesh, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Md. Moshiul Hoque, T. Onuki, Y. Kobayashi, and Y. Kuno, "Effect of Robot’s Gaze Behaviors for Attracting and Controlling Human Attention", Journal of Advanced Robotics Vol. 27, no. 11, pp. 1-17, Taylor & Francis, Japan, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Hongbo Chen, Mohammad Shamsul Arefin, Zhiming Chen, Yasuhiko Morimoto, "Place Recommendation Based on Users Check-in History for Location-Based Services", International Journal of Networking and Computing (ISSN2185-2839, a DBLP Indexed Journal) , , 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Mohammad Shamsul Arefin, Xu Jinhao, Chen Zhiming, and Yasuhiko Morimoto, , "Skyline Query for Selecting Spatial Objects by Utilizing Surrounding Objects", Journal of Computers, ISSN: 1796-203X , , 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Journali</td>
                    <td>Mohammad Shamsul Arefin, Yasuhiko Morimoto, and Mohammad Amir Sharif, "BAENPD: A Bilingual Plagiarism Detector", Journal of Computers. ISSN: 1796-203X , , 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Mohammad Shamsul Arefin and Yasuhiko Morimoto, "Privacy Aware Parallel Computations of Skyline Sets Queries from Distributed Databases", Computing and Informatics, ISSN:1335-9150 , , 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar and Tetsuya Shimamura, "A DWT-DCT-Based Audio Watermarking Method Using Singular Value Decomposition and Quantization", Journal of Signal Processing (JSP) Vol. 17, No. 3, pp. 69-79, , 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Chotan Sheel, Mohammad Ibrahim Khan, Md. Iqbal Hasan Sarker and Tauhidul Alam, , "Algorithm for Optimal Storage of a Distributed Bioinformatics System for Analysis of DNA Sequences", International Journal of Computational Bioinformatics and In Silico Modeling Vol. 2, No. 3 (2013): 106-109, , 2013</td>
                    <td>2013</td>
                </tr>
            </table>
            <div class="pagination">
                <a href="research.blade.php">&laquo;</a>
                <a href="research1.blade.php" class="active">1</a>
                <a href="research2.blade.php" >2</a>
                <a href="research3.blade.php">3</a>
                <a href="research4.blade.php">4</a>
                <a href="research5.blade.php">5</a>
                <a href="research6.blade.php">6</a>
                <a href="research7.blade.php">7</a>
                <a href="research8.blade.php">8</a>
                <a href="research9.blade.php">9</a>
                <a href="research10.blade.php">10</a>
                <a href="research2.blade.php">&raquo;</a>
            </div>
        </div>
    </div>
            <div class="footer" style="height:70px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>