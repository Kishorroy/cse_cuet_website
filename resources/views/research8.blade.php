<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:900px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 1100px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .center {
            text-align: center;
        }

        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #000;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {background-color: #ddd;}
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li class="active"><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">

                </ul>
            </div>
        </nav>

    </div>
    <div class="map">
        <div class="center" >
            <br>
            <table id="customers">
                <tr>
                    <th>Publication Type</th>
                    <th><center>Publication Details</center></th>
                    <th>Year</th>
                </tr>

                <tr>
                    <td>Journal</td>
                    <td>Anik Saha, Dipanjan Das Roy, Tauhidul Alam, Kaushik Deb, "Automated Road Lane Detection for Intelligent Vehicles", Global Journal of Computer Science and Technology Vol. 12 Issue 6 Version 1.0, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Prithwi Raj Chakraborty, Alok Kumar Chowdhury, Sujan Chowdhury, Frhana Shirin Chowdhury, "An Efficient Approach to Access Database in J2ME Applications ", International Journal of Computer Technology and Applications(IJCTA) Vol. 02, Issue 2, pp. 374-378, 2011, ISSN: 2229-6093, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Alok Kumar Chowdhury, Prithwi Raj Chakraborty, Md. Ibrahim Khan, Sujan Chowdhury , "A Robust Approach to Find the Control Points for Wide Variety of 3rd Order Bézier Curves", Global Journal of Computer Science and Technology Vol. 11, Issue 18, pp. 37-43,October 2011, ISSN: 0975-4172., , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Kaushik Deb, Tauhidul Alam, Md. Mohammad Ali and Kang-Hyun Jo, "Lossless Image Compression Technique Based on Snake Ordering", Computer Science & Engineering Research Journal vol. 7, pp. 30-36, Bangladesh, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Muhammad Ibrahim Khan, "Bangla Character Recognition by using Artificial Neural Network", International Conference on Mechanical Engineering and Renewable Energy (ICMERE 2011) 201, Bangladesh, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Kaushik Deb, Md. Ibrahim Khan, Md. Rafiqul Alam, and Kang-Hyun Jo, "Optical Recognition of Vehicle License Plates", In Proceeding of the 6th IEEE International Forum on Strategic Technology (IEEE IFOST) pp. 743-748 , Harbin, China, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Anik Saha and Kaushik Deb, "Bangla Character Recognition by using Artificial Neural Network", In Proceeding of the International Conference on Mechanical Engineering and Renewable Energy (ICMERE 2011), CUET , Bangladesh, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Sujan Chowdhury and Kaushik Deb, "Developing a Bangla Spell Checker With Possible Suggestions by using Longest Common Sub Sequence and Soundex algorithm ", In Proceeding of the International Conference on Mechanical Engineering and Renewable Energy (ICMERE), CUET , Bangladesh, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Md. Moshiul Hoque, T. Onuki, Y. Kobayashi, Y. Kuno, "Controlling Human Attention by Robot's Behavior Depending on his/her Viewing Situations", International Conference on Social Robotics (ICSR) pp. 37-40, Netherlands, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Md. Moshiul Hoque, T. Onuki, Y. Kobayashi, and Y. Kuno, "Controlling Human Attention through Robot’s Gaze Behaviors", International Conference on Human System Interaction (HSI'11) pp. 195–202, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Yasuhiko Morimoto, Md. Anisuzzaman Siddique, and Md. Shamsul Arefin, "Information Filtering by Using Materialized Skyline View", LNCS 7108 pp. 179–189, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Mohammad Shamsul Arefin and Yasuhiko Morimoto, "Privacy Aware Parallel Computation of Skyline Sets Queries from Distributed Databases", Second International Conference on Networking and Computing (IEEE, DBLP) pp. 186-192, Japan, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Mohammad Shamsul Arefin, Y. Morimoto, and A. Yasmin, "Multilingual Content Management in Web Environment", International Conference on Information Science and Applications pp. 1-9, South Korea, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Mohammad Shamsul Arefin, Y. Morimoto, and M. A. Sharif, "Bilingual plagiarism detector", 14th International Conference on Computer and Information Technology (ICCIT) , Bangladesh, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Y. Morimoto, M. A. Siddique, and Mohammad Shamsul Arefin, "Agent-Based Convex Skyline Set Query for Cloud Computing Environment", 5th International Workshop on Data-Mining and Statistical Science , Japan, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Pranab Kumar Dhar and I. Echizen, "Robust FFT Based Watermarking Scheme for Copyright Protection of Digital Audio", 7th IEEE International Conference on Intelligent Information Hiding and Multimedia Signal Processing (IIHMSP2011) pp. 181-184, Dalian, China, 2011</td>
                    <td>2011</td>
                </tr>
            </table>
            <div class="pagination">
                <a href="research7.blade.php">&laquo;</a>
                <a href="research1.blade.php">1</a>
                <a href="research2.blade.php"  >2</a>
                <a href="research3.blade.php" >3</a>
                <a href="research4.blade.php" >4</a>
                <a href="research5.blade.php">5</a>
                <a href="research6.blade.php">6</a>
                <a href="research7.blade.php">7</a>
                <a href="research8.blade.php" class="active">8</a>
                <a href="research9.blade.php">9</a>
                <a href="research10.blade.php">10</a>
                <a href="research9.blade.php">&raquo;</a>
            </div>
        </div>
    </div>
    <div class="footer" style="height:70px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>