<!DOCTYPE html>
<html>
<head>

    <title>CUET CSE</title>
    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:710px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .body{
            height: 800px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }

    </style>

</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li class="active"><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>
    <div class="body">
        <div class="sironam" style="height: 50px; background-color: #9d9d9d">
            <h2 style="margin-left: 280px; color: white;">Message from Head of the Department for P.hd Students</h2>
        </div>
        <div class="details" style="margin-top: 30px; height: 450px;">
            <p style="color: black;  font-size: 18px; opacity: 0.7;">
                <b style="font-size: 25px;">T</b>he graduate Handbook was developed by the Department through the
                recommendations and actions of the Academic Council of Chittagong University of Engineering & Technology (CUET).
                This Handbook contains the rules, policies, and guidelines applicable to the graduate community at CSE Department
                of CUET. This book also contains information related to graduate education, a short description of CSE
                Department, current research activities, staff directory and provides a detailed outline of the course curriculum
                for graduate studies. The policies, rules, regulations and procedures outlined are designed to ensure high
                standards in graduate education at CSE Department. The Academic council expects each graduate program to
                build on this foundation to achieve their programmatic vision of excellence. graduate students should become
                familiar with the policies and degree requirements in the undergraduate Handbook. While flexibility is encouraged,
                regulations will not be waived or exceptions granted just because someone is unaware of specific policies or
                procedures. Computing is an emerging field that is growing very fast and updated rapidly with a variety of research
                dimensions. To cope with the recent advancement, we have been relentlessly trying to modernize to our graduate
                curriculums. The department of CSE provided excellent research facilities and environments in several fields of
                Computing, such as, Robotics, Bioinformatics, Computer Vision, Data Mining, Natural Language Processing, Cloud
                Computing, Digital Signal Processing, Sensor Networks, Human-Computer Interaction, and so on. These research
                facilities will definitely enhance the professional and research skills of young researchers and graduate students.
                The facilities also aim to deals with real problems occurring in the industries/ICT sectors in Bangladesh.
                We hope all the facilities and supports will open up the doors for our graduate students to the development a
                center of excellence in research to solve the technological problems of local and global communities. We firmly
                believe that the prospective graduate students will makes a significant contribution in the ICT sector and high-tech
                industries that boost up to materialize the dreams of digital Bangladesh. The Academic Council of CUET reserves
                the right to make changes to this Handbook at any time. The Department has attempted to include most of the
                regulations governing graduate academic programs; however, some programs have additional requirements and regulations.
                It is the student's responsibility to be aware of, and comply with, all regulations, policies, procedures, and deadlines.
            </p>
        </div>
        <div class="name" style="height: 200px">
            <center>
                <h3 style="color: green;">

                    Correspondence<br>
                    Prof. Dr. Mohammad Shamsul Arefin<br>
                    Head<br>
                    Email : headcse@cuet.ac.bd<br>

                </h3>
                <br>
                <button>
                    <a href="#">Here is the download link for complete P.hd handbook.</a>
                </button>
            </center>
        </div>

    </div>
    <div class="footer" style="height:60px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>
