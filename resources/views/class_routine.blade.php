<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CUET CSE</title>
    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:710px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 430px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
            <br><br>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header" >
                        <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                    </div>
                    <ul class="nav navbar-nav">
                        <li ><a href="home1.blade.php">Home</a></li>
                        <li><a href="admission.blade.php">Admission</a></li>
                        <li><a href="faculty.blade.php">Faculty Members</a></li>
                        <li><a href="#">Notice Board</a></li>
                        <li><a href="#">Upcoming Events</a></li>
                        <li class="active"><a href="class_routine.blade.php">Class Routine</a></li>
                        <li><a href="research.blade.php">Research</a></li>
                        <li><a href="#">Alumni</a></li>
                        <li><a href="contact.blade.php">Contact Info</a></li>
                    </ul>

                    </ul>
                </div>
            </nav>

        </div>
        <div class="map">
            <div class="col-lg-4" style="height: 400px; background: linear-gradient(#90caf9 ,#bbdefb , #e3f2fd );  margin-top: 30px; margin-left: 100px">
                <h3 style="color: white;">Teacher Login</h3><hr>

                <a class="btn btn-success btn-lg" style="width: 200px; margin-top: 100px;" href="{{ route('login') }}">Login</a>



            </div>
            <div class="col-lg-4" style="height: 400px; background: linear-gradient(#90caf9 ,#bbdefb , #e3f2fd );  margin-top: 30px;">

                <h3 style="color: white;">Student Login & signup</h3><hr>

                <a class="btn btn-success btn-lg" style="width: 200px; margin-top: 100px;" href="{{ route('login') }}">Login</a>

                <a class="btn btn-primary btn btn-lg" style="width: 200px; margin-top: 100px;" href="{{ route('register') }}">Register</a>


            </div>


        </div>
    </div>

</body>
</html>