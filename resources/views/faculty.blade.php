<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <style>
        .container{
            height: 3500px;
            width: 1350px;
            background: linear-gradient(white , #afd9ee, #afd9ee);
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }

        .middle{
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(#afd9ee,white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(#afd9ee,white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(#afd9ee,white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(#afd9ee ,white , white, #afd9ee); /* Standard syntax (must be last) */
        }
    </style>

</head>
<body>

<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li class="active"><a href="faculty.blade.php" >Faculty Members</a></li>
                    <li><a href="notice.blade.php">Notice Board</a></li>
                    <li><a href="upcomingevent.blade.php">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li><a href="contact.blade.php">Contact Info</a></li>
                </ul>

            </div>
        </nav>

    </div>

    <div class="container">
        <div class="col-sm-12">
            <h2 align="center"><b>FACULTY MEMBERS</b><hr></h2>
        </div>
        <div class="col-sm-12">
            <h2 align="center">HEAD OF THE DEPARTMENT<hr></h2>
        </div>
        <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/dh.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                     <p style="text-align: center">Dr. MD Samsul Arefin</p>
                     <p style="text-align: center"><b>Designation:</b>Professor</p>

                    <div class="col-sm-4"></div>
                    <div class="button col-sm-4" style="width:134px; height: 30px;background-color: #46b8da;text-align: center;padding-top: 5px;margin-bottom: 10px">
                        <a href="samsul arefin.blade.php" style="color: white;">Read Full Profile</a>
                    </div>
                    <div class="col-sm-4"></div>

                 </div>
            </div>
            <div class="col-md-4"></div>
        </div>

        <div class="col-sm-12">
            <h2 align="center">PROFESSORS<hr></h2>
        </div>

        <div class="col-md-12" style="margin-top: 20px">

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/ibrahim.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">Dr. Md. Ibrahim Khan</p>
                    <p style="text-align: center"><b>Designation:</b>Professor</p>
                    <button style="background-color: #46b8da;  margin-bottom: 15px"><a href="Ibrahim Khan.blade.php" style="color:white;">Read Full Profile</a></button>
                 </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/kaushik.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center"> Dr. Kaushik Deb</p>
                    <p style="text-align: center"><b>Designation:</b>Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Kaushik Deb.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3"  style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/moshiul.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">Dr. Mohammed Moshiul Hoque</p>
                    <p style="text-align: center"><b>Designation:</b>Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="moshiul.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3"  style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/asad.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">Dr. Asaduzzaman</p>
                    <p style="text-align: center"><b>Designation:</b>Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Asaduzzaman.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <h2 align="center">ASSOCIATE PROFESSORS<hr></h2>
        </div>

        <div class="col-md-12" style="margin-top: 20px">

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/pranab.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">Dr. Pranab Kumar Dhar</p>
                    <p style="text-align: center"><b>Designation:</b>Associate Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="pranab.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/mokammel.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">Dr. Md. Mokammel Haque</p>
                    <p style="text-align: center"><b>Designation:</b>Associate Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="moka.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/hasnat.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">Abu Hasnat Mohammad Ashfak Habib</p>
                    <p style="text-align: center"><b>Designation:</b>Associate Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Abu Hasnat Mohammad Ashfak Habib.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/obaidur.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">Mohammad Obaidur Rahman</p>
                    <p style="text-align: center"><b>Designation:</b>Associate Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="obaidur.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <h2 align="center">ASSISTANT PROFESSORS<hr></h2>
        </div>

        <div class="col-md-12" style="margin-top: 20px">

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/saki.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center"> Mir Md. Saki Kowsar</p>
                    <p style="text-align: center"><b>Designation:</b>Assistant Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Saki Kowsar.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/khossen.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center"> Muhammad Kamal Hossen</p>
                    <p style="text-align: center"><b>Designation:</b>Assistant Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="kamal.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/milon.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center"> Md. Iqbal Hasan Sarker</p>
                    <p style="text-align: center"><b>Designation:</b>Assistant Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Iqbal Hasan Sarker.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/monjur.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">	Md. Monjur-Ul-Hasan</p>
                    <p style="text-align: center"><b>Designation:</b>Assistant Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Monjur-Ul-Hasan.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

        </div>
        <div class="col-md-12" style="margin-top: 20px">

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/thomas.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">	Thomas Chowdhury</p>
                    <p style="text-align: center"><b>Designation:</b>Assistant Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="thomas.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>


            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/prince.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center"> Md. Enamul Hoque Prince</p>
                    <p style="text-align: center"><b>Designation:</b>Assistant Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Enamul Hoque Prince.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>


            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/taua.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">Tauhidul Alam</p>
                    <p style="text-align: center"><b>Designation:</b>Assistant Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="alam.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/mhc.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">	Mahfuzulhoq Chowdhury</p>
                    <p style="text-align: center"><b>Designation:</b>Assistant Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Mahfuzulhoq Chowdhury.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 20px">

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/mukta.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center"> Rahma Bintey Mufiz Mukta</p>
                    <p style="text-align: center"><b>Designation:</b>Assistant Professor</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="mukta.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <h2 align="center">LECTURERS<hr></h2>
        </div>

        <div class="col-md-12" style="margin-top: 20px">

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/sujanc.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">	Sujan Chowdhury</p>
                    <p style="text-align: center"><b>Designation:</b>Lecturer</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="sujan.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/priyam.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">Priyam Biswas</p>
                    <p style="text-align: center"><b>Designation:</b>Lecturer</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="priyam.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/monju.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">	Md. Monjurul Islam</p>
                    <p style="text-align: center"><b>Designation:</b>Lecturer</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Monjurul Islam.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/lamia.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">Lamia Alam</p>
                    <p style="text-align: center"><b>Designation:</b>Lecturer</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Lamia Alam.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>
        </div>

        <div class="col-md-12" style="margin-top: 20px">

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/shayla.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center"> Shayla Sharmin</p>
                    <p style="text-align: center"><b>Designation:</b>Lecturer</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="shayla.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/sabir.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center"> Md. Sabir Hossain</p>
                    <p style="text-align: center"><b>Designation:</b>Lecturer</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Sabir Hossain.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/farzana.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center"> Farzana Yasmin</p>
                    <p style="text-align: center"><b>Designation:</b>Lecturer</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Farzana Yasmin.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>


        <div class="col-md-3" style=" text-align: center; ;">
            <img src="{{URL::asset('/images/teachers/roy.jpg')}}" style="border-radius: 100%">
            <div class="text"style="padding-top: 10px">
                <p style="text-align: center"> Animesh Chandra Roy</p>
                <p style="text-align: center"><b>Designation:</b>Lecturer</p>
                <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Animesh Chandra Roy.blade.php" style="color:white;">Read Full Profile</a></button>
            </div>
        </div>
    </div>

        <div class="col-md-12" style="margin-top: 20px">
            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/forhad.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center"> Md. Shafiul Alam Forhad</p>
                    <p style="text-align: center"><b>Designation:</b>Lecturer</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Shafiul Alam Forhad.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/jibon.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center"> Jibon Naher</p>
                    <p style="text-align: center"><b>Designation:</b>Lecturer</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="Jibon Naher.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/tista.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">Sharmistha Chanda Tista</p>
                    <p style="text-align: center"><b>Designation:</b>Lecturer</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="tista.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>

            <div class="col-md-3" style=" text-align: center; ;">
                <img src="{{URL::asset('/images/teachers/tanzina.jpg')}}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">Tanzina Akter</p>
                    <p style="text-align: center"><b>Designation:</b>Lecturer</p>
                    <button style="background-color: #46b8da;margin-bottom: 15px"><a href="tanzina.blade.php" style="color:white;">Read Full Profile</a></button>
                </div>
            </div>
        </div>


</div>




    </div>


</div>
<div class="footer" style="margin-top: 200px; height:90px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
    <br>
    <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

</div>

</body>
</html>