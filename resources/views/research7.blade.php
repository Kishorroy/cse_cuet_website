<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:710px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 950px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .center {
            text-align: center;
        }

        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #000;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {background-color: #ddd;}
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li class="active"><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>
    <div class="map">
        <div class="center" >
            <br>
            <table id="customers">
                <tr>
                    <th>Publication Type</th>
                    <th><center>Publication Details</center></th>
                    <th>Year</th>
                </tr>

                <tr>
                    <td>Journal</td>
                    <td>Asaduzzaman and Hyung Yun Kong, "Ergodic and outage capacity of interference temperature-limited cognitive radio multi-input multi-output channel", IET communication Vol. 5, Iss. 5, pp. 652–659, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Asaduzzaman and Hyung Yun Kong, "Code Combining Cooperative Diversity in Long-haul Transmission of Cluster based Wireless Sensor Networks", KSII Transactions on Internet and Information Systems Vol. 5, Iss. 7, pp. 1293–1310, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Asaduzzaman and Hyung Yun Kong, "Multi-relay cooperative diversity protocol with improved spectral efficiency", Journal of Communications and Networking Vol. 13, Iss. 3, pp. 240–249, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Asaduzzaman and Hyung Yun Kong and Koo In-Soo , "Opportunistic Relaying based Spectrum Leasing for Cognitive Radio Networks", Journal of Communications and Networking Vol. 13, Iss. 1, pp. 50–55, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Hyung Yun Kong and Asaduzzaman , "On the outage behavior of interference temperature limited CR-MISO channel", Journal of Communications and Networking Vol. 13, Iss. 5, pp. 456–462, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Md. Moshiul Hoque, T. Onuki, E. Tsuburaya, Y. Kobayashi, Y. Kuno, T. Sato, and S. Kodama, "An Empirical Framework to Control Human Attention by Robot", ACCV 2010 Workshops Part I, vol. 6468, pp. 430-4392, LNCS, Springer, Heidelberg, Germany, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar and Jong-Myon Kim, "Digital Watermarking Scheme Based on Fast Fourier Transformation for Audio Copyright Protection", International Journal of Security and Its Application (IJSIA) Vol. 5, No. 2, pp. 33-48, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar and Mohammad Ibrahim Khan, "Design and Implementation of Real-Time and Non-Real Time Digital Filter for Audio Signal Processing", Journal of Emerging Trends in Computing and Information Sciences (JETCIS) Vol. 2. No. 3, pp. 149-155, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar, Mohammad Ibrahim Khan and Sujan Chowdhury, "An Efficient Image Watermarking System Based on Error Correcting Codes in DCT Domain", Military Institute of Science and Technology (MIST) Journal Vol. 3, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Abdul Kadar Muhammad Masum, Mohammad Shahjalal, Md. Iqbal Hasan Sarker, Md. Faisal Faruque, "An approach to recognize Bangla Digits from Digital Image", International Journal of Computer Science and Network Security (IJCSNS) Volume-11, No-6, Koria, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Abdul Kadar Muhammad Masum, Mohammad Shahjalal, Md. Faisal Faruque , Md. Iqbal Hasan Sarker, "Solving the Vehicle Routing Problem using Genetic Algorithm", International Journal of Advanced Computer Science and Application (IJACSA) Vol-2, No-7, USA, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Md. Monjurul Islam, "Key Agreement & Authentication Protocol for IEEE 802.11", Global Journal of Computer Science and Engineering Vol. 11 Issue 20 Version 1.0 , , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Md. Monjurul Islam, "Security Thread Analysis & Solution for NGN(Next Generation Network)", International Journal of Engineering Research and Applications(IJERA) Vol. 1,Issue 3,pp.1448-1452, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Enamul Hoque, O. Hoeber, G. Strong, M. Gong, "Combining conceptual query expansion and visual search results exploration for Web image retrieval", Journal of Ambient Intelligence and Humanized Computing pp. 1-12, , 2011</td>
                    <td>2011</td>
                </tr>
            </table>
            <div class="pagination">
                <a href="research6.blade.php">&laquo;</a>
                <a href="research1.blade.php">1</a>
                <a href="research2.blade.php"  >2</a>
                <a href="research3.blade.php" >3</a>
                <a href="research4.blade.php" >4</a>
                <a href="research5.blade.php">5</a>
                <a href="research6.blade.php">6</a>
                <a href="research7.blade.php" class="active">7</a>
                <a href="research8.blade.php">8</a>
                <a href="research9.blade.php">9</a>
                <a href="research10.blade.php">10</a>
                <a href="research8.blade.php">&raquo;</a>
            </div>
        </div>
    </div>
    <div class="footer" style="height:70px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>