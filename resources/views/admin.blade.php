@extends('layouts.app')
<style>
    .dropdown-submenu {
        position: relative;
    }

    .dropdown-submenu .dropdown-menu {
        top: 0;
        left: 100%;
        margin-top: -1px;
    }
</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <div class="collapse navbar-collapse" id="app-navbar-collapse">
                            <!-- Left Side Of Navbar -->
                            <ul class="nav navbar-nav">
                                &nbsp;<button class="btn btn-lg" style="float: left"> <a href="TeacherRegistration">Teacher Registration</a></button>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                <div class="dropdown" style="float: right">
                                    <button class="btn btn-lg btn-primary dropdown-toggle"  data-toggle="dropdown">Routine
                                        <span class="caret "></span></button>
                                    <ul class="dropdown-menu">

                                        <li class="dropdown-submenu">
                                            <a class="test" tabindex="-1" href="#">Level-1 <span class="caret"></span></a>
                                            <ul class="dropdown-menu">

                                                <li class="dropdown-submenu">
                                                    <a class="test" href="#">Term-I <span class="caret"></span></a>

                                                    <ul class="dropdown-menu">
                                                        <li><a href="{!! route('Oneoneroutine') !!}">Sec-A </a></li>
                                                        <li><a href="{!! route('Oneonebroutine') !!}">Sec-B</a></li>
                                                    </ul>

                                                    <a class="test" href="#">Term-II <span class="caret"></span></a>

                                                    <ul class="dropdown-menu">
                                                        <li><a href="{!! route('Onetwoaroutine') !!}">Sec-A </a></li>
                                                        <li><a href="{!! route('Onetwobroutine') !!}">Sec-B</a></li>
                                                    </ul>

                                                </li>
                                            </ul>
                                        </li>

                                        <li class="dropdown-submenu">
                                            <a class="test" tabindex="-1" href="#">Level-2 <span class="caret"></span></a>
                                            <ul class="dropdown-menu">

                                                <li class="dropdown-submenu">
                                                    <a class="test" href="#">Term-I <span class="caret"></span></a>

                                                    <ul class="dropdown-menu">
                                                        <li><a href="{!! route('Twoonearoutine') !!}">Sec-A </a></li>
                                                        <li><a href="{!! route('Twoonebroutine') !!}">Sec-B</a></li>
                                                    </ul>

                                                    <a class="test" href="#">Term-II <span class="caret"></span></a>

                                                    <ul class="dropdown-menu">
                                                        <li><a href="{!! route('Twotwoaroutine') !!}">Sec-A </a></li>
                                                        <li><a href="{!! route('Twotwobroutine') !!}">Sec-B</a></li>
                                                    </ul>

                                                </li>
                                            </ul>
                                        </li>

                                        <li class="dropdown-submenu">
                                            <a class="test" tabindex="-1" href="#">Level-3 <span class="caret"></span></a>
                                            <ul class="dropdown-menu">

                                                <li class="dropdown-submenu">
                                                    <a class="test" href="#">Term-I <span class="caret"></span></a>

                                                    <ul class="dropdown-menu">
                                                        <li><a href="{!! route('ThreeoneAroutine') !!}">Sec-A </a></li>
                                                        <li><a href="{!! route('ThreeoneBroutine') !!}">Sec-B</a></li>
                                                    </ul>

                                                    <a class="test" href="#">Term-II <span class="caret"></span></a>

                                                    <ul class="dropdown-menu">
                                                        <li><a href="{!! route('ThreetwoAroutine') !!}">Sec-A </a></li>
                                                        <li><a href="{!! route('ThreetwoBroutine') !!}">Sec-B</a></li>
                                                    </ul>

                                                </li>
                                            </ul>
                                        </li>

                                        <li class="dropdown-submenu">
                                            <a class="test" tabindex="-1" href="#">Level-4 <span class="caret"></span></a>
                                            <ul class="dropdown-menu">

                                                <li class="dropdown-submenu">
                                                    <a class="test" href="#">Term-I <span class="caret"></span></a>

                                                    <ul class="dropdown-menu">
                                                        <li><a href="{!! route('FouroneAroutine') !!}">Sec-A </a></li>
                                                        <li><a href="{!! route('FouroneBroutine') !!}">Sec-B</a></li>
                                                    </ul>

                                                    <a class="test" href="#">Term-II <span class="caret"></span></a>

                                                    <ul class="dropdown-menu">
                                                        <li><a href="{!! route('FourtwoAroutine') !!}">Sec-A </a></li>
                                                        <li><a href="{!! route('FourtwoBroutine') !!}">Sec-B</a></li>
                                                    </ul>

                                                </li>
                                            </ul>
                                        </li>

                                    </ul>
                                </div>

                            </ul>

                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.dropdown-submenu a.test').on("click", function(e){
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });
        });
    </script>

@endsection