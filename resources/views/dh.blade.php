<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:900px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 1040px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .center {
            text-align: center;
        }

        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #000;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {background-color: #ddd;}
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
        .body{

            height: 1375px;
            width: 800px;
            background-color: #1b6d85;
            border: solid black;

            margin-left: 300px;
            margin-top: 15px;
            border-width: .5px;
            padding-left: 10px;
            padding-top: 10px;
            padding-right: 10px;
            text-align: justify;
            color: white;
            font-size: small;

        }

        .footer{
            width: 780px;
            height: 50px;
            background-color: #46b8da;
            color: black;
            padding-top: 15px;
            text-align: center;
            font-size: smaller;

        }

    </style>

</head>
<body style="background: linear-gradient(white , white, #afd9ee); height: 1600px">
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li ><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>
    <div class="body">

        <h2>  Message from Head </h2>
        <hr style="background-color: white">
        <img src="{{URL::asset('/images/body/dh.jpg')}}" style="margin-left: 300px"> <br> <br> <br>

        <p>
            Greetings! On behalf of the faculty members, staffs, and students of Department of Computer Science & Engineering of Chittagong University of Engineering & Technology (CUET), I welcome you all to the creative world of IT (Information Technology). I am really happy for the excitement periods of computer scientists for the last decade and the time ahead. Because I believe CSE discipline has been widely recognized as an essential source and technique for the advancements in all spheres of human endeavor now and in future. Department of Computer Science & Engineering (CSE) was established in 1998. Since its emergence, the department has been making rapid strides and continues to excel in all aspects impacting the academic and technological progress in our society. From the year 2011, the department is launching post-graduate programs, viz. M. Sc. Engineering and M. Engineering.

            <br> <br>
            <b> Mission & Vision </b>

            <br> <br>
            In order to prepare potential and productive persons in industry and academia, as well as in government sector, in the undergraduate and graduate programs of Department of CSE of CUET an excellent environment of teaching, learning, and research in computing and IT has been maintained from the very past. In particular, we aim:

            <br> <br>
            &nbsp; &nbsp; &nbsp; &nbsp; To produce highly qualified and all-rounded graduate possessing fundamental knowledge of computing and information technology who can provide leadership and service to Bangladesh and the world.

            <br><br>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; To pursue creative research and new innovations in Computer Science and Engineering and across disciplines in order to serve the needs of industry, government, society, and the scientific community by expanding and contributing to the body of knowledge in the field.

            <br><br>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; To develop strong partnerships with industrial and government and non-government agencies, professional societies, and local communities.

            <br><br>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; To be a recognized as the leader having proper and recent education and research in the Computer Science and Engineering arena, specially with a strong impact in the timely and targeted areas of research to attract the excellent faculty and students worldwide.

            <br> <br>
            The department has a vibrant academic culture with high-quality publications in prestigious conferences and journals. Our strengths are our outstanding faculty members, excellent laboratory facilities, and strong commitment to research.

            <br> <br>
            The Department of CSE, CUET arranged 1st National Conference on Intelligent Computing and Information Technology (NCICIT) in 2013. CUET has achieved full membership of IFOST an international research forum comprising 10 universities from seven countries which jointly conducts research works with the member universities. CUET organized IFOST 2014 at its own campus. CSE department also organized NHSPC 2016, NCPC 2017, NHSPC 2017 progamme. First time in Bangladesh 150 teams participate at a time in final contest. To fulfill the mission of VISION 2021 and to ensure effective use of Information and Communication Technology (ICT) in the country, the Government is going to establish the first ICT incubator at CUET. Very early we are going to inaugurate a center for Mobile Games and Application LAB where not only the students of CSE,CUET any one can launch their professional Apps from this center. Beside this a new Lab for Robot lovers, Robotics LAB will be inaugurate soon.

            <br> <br>
            Your comments and suggestions will help us to move forward and provide us encouragement to achieve superiority in Bangladesh as well as in the Global world arena. I thank you again for your interest in the Department of CSE at CUET. If you have any query, please do not hesitate to contact me personally through sarefin_406@yahoo.com

            <br> <br>
            <b>Correspondence

                <br> <br>
                Prof. Dr. Mohammad Shamsul Arefin <br>
                Head <br>
                Email : sarefin@cuet.ac.bd, sarefin_406@yahoo.com <br>
                Phone: 880-31-723336, 01716890204
                <br> </b>

        </p>

        <div class="footer">

            <i> Copyright © 2017 Chittagong University of Engineering and Technology. </i>

        </div>

    </div>

</div>

</body>
</html>
