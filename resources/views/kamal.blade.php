<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Muhammad Kamal Hossen</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height: 1600px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .nopadding{
            padding: 0;
            margin-top: 30px;
            background: linear-gradient(white ,white , #afd9ee);
        }
        .nopadding h4{
            background: linear-gradient( #afd9ee,white , #afd9ee);
            padding: 10px 15px;
        }

        .nopadding ul li{

            background: url("/public/images/arrow.jpg") no-repeat left -1.6rem;
            padding-left: 10px;
            background-size: 25px 35px;
            margin: 15px 17px;
        }





    </style>
</head>
<body>

<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li class="active"><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>
    </div>

    <div class="col-lg-10" style="margin-left: 100px ">
        <div class="col-md-4">
            <img src="{{URL::asset('/images/teachers/khossen.jpg')}}" style="width: 300px;height: 300px;margin-top: 20px">
        </div>
        <div class="col-md-8">
            <h1 style="margin-bottom: 0">Muhammad Kamal Hossen</h1>
            <br>
            <h2 style="margin: 0;padding: 0;">মোঃ কামাল হোসেন</h2><hr>
            <h4>Assistant Professor</h4>
            <b>E-mail: </b>	kamal_cuet@yahoo.com; mkhossen@cuet.ac.bd
            <br><b>Contact: </b>Mobile: +8801816012649 Tel.:+88031714920~22, Ext.-2314<hr>
            <b>Research Interests:</b>
            Digital Image Processing,  	Computer Vision,   Pattern Recognition,   Data Mining
            <hr>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Education :</b></h4>
            <ul>
                <li><b>B.Sc.</b><br>Computer Science<br>Chittagong University Of Engineering & Technology<b>(CUET)</b><br>Bangladesh</li>
                <li><b>M.Sc.</b><br>Computer Science<br>Chittagong University Of Engineering & Technology<b>(CUET)</b><br>Bangladesh</li>
                <li><b>PhD(Persuing)</b><br>Computer Science<br>Bangladesh</li>

            </ul>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Related Courses(Undergraduate):</b></h4>
            <div class="col-md-6">
                <ul>
                    <li> 	Applied Statistics & Queuing Theory</li>
                    <li>Database Systems</li>
                    <li>Computer Peripherals & Interfacing</li>
                    <li>Software Engineering</li>
                    <li>Object Oriented Programming</li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul>
                    <li>ANeural Network & Fuzzy Logic</li>
                    <li> Operating Systems</li>
                    <li>Data Structures</li>
                    <li>Digital Image Processing</li>
                </ul>
            </div>
        </div>

        <div class="col-sm-12 nopadding" style="background: linear-gradient( white ,#afd9ee, #afd9ee);">
            <h4><b>Publications :</b></h4>
            <h5 align="center"><b>Journals </b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>Muhammad Kamal Hossen and Kaushik Deb, “Vehicle License Plate Detection and Tilt Correction Based on HSI Color Model and SUSAN Corner Detector”, Smart Computing Review(SCR), vol. 4, no. 5, pp. 371-388, South Korea, 2014.</li>
                <li>Muhammad Ibrahim Khan, Muhammad Kamal Hossen, Md. Sabbir Ali, “Cell Segmentation from Cellular Image”, Global Journal of Computer Science and Technology (GJCST), Vol. 10 Issue 13 (Ver. 1.0) , Page No: 55-60, October, 2010.</li>
                <li>M. A. Islam, M. K. Hossen, and M. S. A. Chowdhury, ” An Efficient Method of Optical Character Recognition System for Bangla Numerals using Fuzzy Logic”, Computer Science and Engineering Research Journal(CSERJ), Bangladesh ,Vol.-06, Page No:-68-80, 2009-2010.</li>
                <li>M. M. Rahman, M. A. Islam, and M. K. Hossen,”Upper Leg and Knee Design for a Humanoid Walking Robot”, Computer Science and Engineering Research Journal (CSERJ), Bangladesh, Vol.-06, Page No: -81-89, 2009-2010.</li>
                <li>M. T. Alam, M. K. Hossen, A. S. M. Kayes and M. F. Kader, “Globular Sampling with Image Enhancement for Fingerprint Verification”, Computer Science and Engineering Research Journal, Bangladesh, Vol.-05, Page No:-50-56, 2008.</li>
                <li>A. S. M. Kayes, T. Chowdhury, M. K. Hossen, M. S. Kowsar and B. M. Solaiman, “Password, Fingerprint and PKI based Authentication for 4G Mobile Network”, Computer Science and Engineering Research Journal, Bangladesh, Vol.-05, Page No:-32- 38, 2008.</li>
                <li>M. F. Kader, M. K. Hossen, Assaduzzaman and A. S. M. Kayes, “An Off-line Handwritten Signature Verification System as a Knowledge Base”, Computer Science and Engineering Research Journal, Bangladesh, Vol.-04, Page No:-50-55, 2006.</li>
                <li>N. K. Chowdhury, R. U. Faruqui and M. K. Hossen, “Approximate Travel Time Measure from Moving Object Trajectory in Road Network”, Computer Science and Engineering Research Journal, Bangladesh, Vol.-04, Page No: 20-28, 2006.</li>
            </ul>
            <hr>
            <h5 align="center"><b>Conferences </b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>Nuzhat Tabassum, Muhammad Kamal Hossen, Sujan Chowdhury and Salah Uddin Mondal, “An Approach to Recognize Book Title from Multi-Cell Book Shelf Images”, IEEE International Conference on Imaging, Vision & Pattern Recognition (icIVPR), Dhaka, Bangladesh, 13-14 February, 2017.</li>
                <li>Zainal Abedin, Prashengit Dhar, Muhammad Kamal Hossen and Kaushik Deb, “Traffic Sign Detection and Recognition Using Fuzzy Segmentation Approach and Artificial Neural Network Classifier Respectively”, IEEE International Conference on Electrical, Computer and Communication Engineering (ECCE), Cox's Bazar, Bangladesh, 16-18 February, 2017.</li>
                <li>Animesh Chandra Roy, Muhammad Kamal Hossen and Debashis Nag, "License Plate Detection and Character Recognition System for Commercial Vehicles based on Morphological Approach and Template Matching," 3rd International Conference on Electrical Engineering and Information & Communication Technology (iCEEiCT), Dhaka, Bangladesh, 22-24 September, 2016.</li>
                <li>Muhammad Kamal Hossen and Afiya Ayman, "JRanker: An Approach to Evaluate the Prestige of a Journal Using PageRank and Alexa Rank along with Impact Factor", 1st International Conference on Information and Communication Technology (ICAICT), Chittagong, May, 2016.</li>
                <li>Muhammad Kamal Hossen and Sabrina Hoque Tuli, "A Surveillance System Based on Motion Detection and Motion Estimation using Optical Flow", 5th International Conference on Informatics, Electronics & Vision (ICIEV), Dhaka, Bangladesh, May, 2016.</li>
                <li>Kaushik Deb, Muhammad Kamal Hossen, Muhammad Ibrahim Khan and Mohammad Rafiqul Alam, "Bangladeshi Vehicle License Plate Detection Method Based on HSI Color Model and Geometrical Properties", 7th International Forum on Strategic Technology (IFOST), Tomsk, Russia, September, 2012.</li>
                <li>Haosong Gou, Md. Kafil Uddin, Muhammad Kamal Hossen, “An Agent-based Cooperative Communication Method in Wireless Sensor Network for Port logistics “13th International Conference on Computer & Information Technology (ICCIT), Page No:-494-499, Dhaka, December, 2010.</li>
                <li>Md. Kafil Uddin, Muhammad Kamal Hossen, “A Safe System with Safe Logistics Support for Chittagong Port”, 13th International Conference on Computer & Information Technology (ICCIT), Page No:-347-351, Dhaka, December, 2010.</li>
                <li>Md. Moshiul Hoque, Md. Kamal Hossen, Mir Md. Saki Kowsar, Kaushik Deb, Md. Shamsul Arefin, “An Empirical Framework for Network Intrusion Detection using Fuzzy Logic”, 9th International Conference on Computer & Information Technology (ICCIT), Dhaka, 2006.</li>


            </ul>

        </div>




    </div>

    <div class="footer" style="margin-top: 2200px; height:90px; background-color: #afd9ee; ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>

</div>



</body>
</html>