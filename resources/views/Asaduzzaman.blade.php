<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dr. Asaduzzaman</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height: 1600px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .nopadding{
            padding: 0;
            margin-top: 30px;
            background: linear-gradient(white ,white , #afd9ee);
        }
        .nopadding h4{
            background: linear-gradient( #afd9ee,white , #afd9ee);
            padding: 10px 15px;
        }

        .nopadding ul li{

            background: url("/public/images/arrow.jpg") no-repeat left -1.6rem;
            padding-left: 10px;
            background-size: 25px 35px;
            margin: 15px 17px;
        }





    </style>
</head>
<body>

<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li class="active"><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>
    </div>

    <div class="col-lg-10" style="margin-left: 100px ">
        <div class="col-md-4">
            <img src="{{URL::asset('/images/teachers/asad.jpg')}}" style="width: 300px;height: 300px;margin-top: 20px">
        </div>
        <div class="col-md-8">
            <h1 style="margin-bottom: 0">Dr. Asaduzzaman</h1>
            <br>
            <h2 style="margin: 0;padding: 0;">ডঃ আসাদুজ্জামান</h2><hr>
            <h4>Professor</h4>
            <b>E-mail: </b>	asad@cuet.ac.bd
            <br><b>Contact: </b>	01938534828<hr>
            <b>Research Interests:</b>
            Wireless Communication and Networking,  	Mobile computation and mobile applications,      Digital System Design and Computer interfacing
            <hr>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Education :</b></h4>
            <ul>
                 <li><b>PhD</b><br>Computer Science</li>
            </ul>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Related Courses(Graduate):</b></h4>
            <ul>
                <li>Wireless Communication</li>
                <li>Advance Communication & Networking.</li>

            </ul>
        </div>


        <div class="col-sm-12 nopadding">
            <h4><b>Related Courses(Undergraduate):</b></h4>
            <div class="col-md-6">
                <ul>
                    <li>Communication Engineering</li>
                    <li> Data Communication</li>
                    <li> Computer Networks</li>
                    <li>Digital Electronics and Pulse Technique</li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul>
                    <li>Basic Electrical Engineering</li>
                    <li> Computer Peripherals and Interfacing</li>
                    <li> Digital System Design</li>
                    <li> Digital Logic Design</li>
                </ul>
            </div>
        </div>

        <div class="col-sm-12 nopadding" style="background: linear-gradient( white ,#afd9ee, #afd9ee);">
            <h4><b>Publications :</b></h4>
            <h5 align="center"><b>Journals </b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>Asaduzzaman and Hyung Yun Kong, "Cooperative Relaying to Improve the Spectrum Utilization of Multi-channel CR Networks,'' Journal of Wireless Personal Communication (Springer), Vol. 82, No. 4, pp. 523--531, September 2015 (SCI)</li>
                <li>Mahfuzulhoq Chowdhury, Asaduzzaman, and Md. Fazlul Kader, "Cognitive Radio MAC Protocols: A Survey, Some Research Issues and Challenges,'' Smart Computing Review, Vol. 5, No.1, pp. 19-29, January 2015 (SCOPUS)</li>
                <li>Md Fazlul Kader, Asaduzzaman, and Md. Moshiul Hoque "Outage Capacity Analysis of a Cooperative Relaying Scheme in Interference Limited Cognitive Radio Networks'', Journal of Wireless Personal Communication (Springer), vol. (2014)79, August, 2014. (SCI)</li>
                <li>Mahfuzulhoq Chowdhury and Asaduzzaman, "An Opportunistic Two Transceiver Based MAC Protocol for Cognitive Radio Networks,'' International Journal of Multimedia and Ubiquitous Engineering ,Vol. 9, N0. 12, pp. 49-60, Dec. 2014 (SCOPUS)</li>
                <li>Mahfuzulhoq Chowdhury, Asaduzzaman, and Md. Fazlul Kader, "Performance Analysis of Local and Cooperative Spectrum Sensing in Cognitive Radio Networks,'' International Journal of Signal Processing, Image Processing and Pattern Recognition (IJSIP), Vol. 6, No. 6, pp. 397-410, June 2013 (SCOPUS)</li>
                <li>Md Fazlul Kader, Asaduzzaman, and Md. Moshiul Hoque "Hybrid Spectrum Sharing with Cooperative Secondary User Selection in Cognitive Radio Networks'', KSII Transactions on Internet and Information Systems, vol. 7, no. 9, September, 2013 (SCIE)</li>
                <li>Mahfuzulhoq Chowdhury, Asaduzzaman, Md. Fazlul Kader, and Mohammad Obaidur Rahman "Design of an Efficient MAC Protocol for Opportunistic Cognitive Radio Networks'', International Journal of Computer Science \& Information Technology (IJCSIT) Vol 4, No 5, October 2012 (SCOPUS)</li>
                <li>Asaduzzaman and Hyung Yun Kong, "Ergodic and outage capacity of interference temperature-limited cognitive radio multi-input multi-output channel,'' IET communications, Vol. 5, Iss. 5, pp. 652--659, 2011 (SCI)</li>
                <li>Asaduzzaman and Hyung Yun Kong, "Multi-relay cooperative diversity protocol with improved spectral efficiency,'' Journal of Communications and Networking, Vol. 13, Iss. 3, pp. 240--249, 2011 (SCI)	</li>
                <li>Asaduzzaman and Hyung Yun Kong, "Opportunistic Relaying based Spectrum Leasing for Cognitive Radio Networks,'' Journal of Communications and Networking, TVol. 13, Iss. 1, pp. 50--55, 2011 (SCI)</li>
                <li>Asaduzzaman and Hyung Yun Kong, "Code Combining Cooperative Diversity in Long-haul Transmission of Cluster based Wireless Sensor Networks,'' KSII Transactions on Internet and Information Systems, Vol. 5, Iss. 7, pp. 1293--1310 (SCIE)</li>
                <li>Hyung Yun Kong and Asaduzzaman, "On the outage behavior of interference temperature limited CR-MISO channel,'' Journal of Communications and Networking," Journal of Communications and Networking, Vol. 13, Iss. 5, pp. 456--462, 2011 (SCI)</li>
                <li>Asaduzzaman and Hyung Yun Kong, "Energy Efficient Cooperative LEACH Protocol for Wireless Sensor Networks,'' Journal of Communications and Networking, vol. 12, No. 4, pp.358--365, Aug. 2010 (SCI)</li>
                <li>Asaduzzaman and Hyung Yun Kong, "A Cooperative Transmission Scheme for Cluster Based Wireless Sensor Networks,'' Lecture note on Artificial Intelligence, Volume 5755, pp. 728--737, 2010.</li>
                <li>Hyung Yun Kong and Asaduzzaman, "Distributed Clustering Algorithm to Explore Cooperative Diversity in Wireless Sensor Networks", IEICE Transactions on Communications, vol. E93-B, no. 5, pp. 1232--1239, May 2010 (SCI)</li>
                <li>Asaduzzaman and H.-Y. Kong, "Distributed Cooperative Routing Algorithm for Multi-hop Multi-relay Wireless Networks,'' IEICE Transactions on Communications, Vol. E93-B, No.04, pp.1049--1052 , Apr. 2010 (SCI)</li>
                <li>Asaduzzaman and Hyung Yun Kong, "Performance analysis of code combining based cooperative protocol with amplified-and-forward (AF) relaying,'' IEICE Transactions on Communications, vol. E93-B, no. 2, pp. 2275--2278, Feb. 2010 (SCI)</li>
                <li>Asaduzzaman and Hyung Yun Kong, "Coded diversity for cooperative MISO based wireless sensor networks,'' IEEE Communications Letters, vol. 13, No. 7, pp. 516--518, July 2009 (SCI)</li>
                <li>Asaduzzaman and H.-Y. Kong, "Code Combining Based Cooperative LEACH Protocol for Wireless Sensor Networks,'' IEICE Transactions on Communications, vol. E92-B, No. 6, pp. 2275--2278, June 2009 (SCI)</li>
                <li>Asaduzzaman and Hyung Yun Kong, "Automatic Request for Cooperation (ARC) and Relay Selection for Wireless Networks,'' IEICE Transactions on Communications, vol. E92-B, no. 3, pp. 964--972, March 2009 (SCI)</li>
                <li>Asaduzzaman and Hyung Yun Kong, "Composite Signaling Coded Cooperation for Fast and Slow Fading,'' IEICE Transactions on Communications, vol. E91-B, no. 9, pp. 3025--3029, Sept. 2008.</li>
                <li>Asaduzzaman and Hyung Yun Kong, "Code Combining in Cooperative Communication,'' IEICE Transactions on Communications, vol. E91-B, no. 3, pp. 805--813, March 2008 (SCI)</li>

            </ul>
            <hr>
            <h5 align="center"><b>Conferences</b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>Md Fazlul Kader, Asaduzzaman, Soo Young Shin, "Outage Capacity Analysis of a Secondary Network in Interference Limited Cognitive Radio Spectrum Sharing System,'' in proc. of 17 th International Conference on Computer and Information Technology (ICCIT), ICCIT'14, Bangladesh; December 2014.</li>
                <li>Mahfuzulhoq Chowdhury, Asaduzzaman, Md. Fazlul Kader, "An Efficient MAC protocol for Cognitive Radio Networks,'' in Proc of International Conference on Engineering Research, Innovation and Education(ICERIE)’2013, Shahjalal University of Science \& Technology,Bangladesh, pp. 322--327, Jan. 2013.</li>
                <li>Md Fazlul Kader, Asaduzzaman and M. Chowdhury, "Cooperative Secondary User Selection as a Relay for the Primary System in Underlay Cognitive Radio Networks'' in Proc of IEEE 15th International Conference on Computer and Information Technology(ICCIT)’2012, University of Chittagong, Bangladesh, pp. 275--278, December 2012.</li>
                <li>Asaduzzaman, Hyung Yun Kong and Kim Ryum, "Cooperative Relaying in Interference Limited Cognitive Radio Networks,'' 2010 IEEE 6th International Conference on Wireless and Mobile Computing, Networking and Communications, held in Niagara Falls, Canada, 2010.</li>
                <li>Asaduzzaman, Hyung Yun Kong and Ha Nguyen Vu, "A Cooperative Transmission Scheme for Cluster Based Wireless Sensor Networks,'' 16th Asia-Pacific Conference on Comm</li>
                <li>Vo Nguyen Bao, Hyung-Yun Kong, Asaduzzaman, Tran Thanh Truc and Park Jihwan, "Optimal Switching Adaptive M-QAM for Opportunistic Amplify-and-Forward Networks,'' 25th Queen's Biennial Symposium on Communications (QBSC 2010)",Kingston, Ontario, Canada, Canada, 2010.</li>
                <li>V. N. Q. Bao, H. Y. Kong, Asaduzzaman, J.-H. Lee and J.-H. Park, "On The Capacity of Opportunistic Cooperative Networks under Adaptive Transmission,'' in IEEE PIMRC 2009, Tokyo, Japan, 2009.</li>
                <li>Asaduzzaman, Hyung Yun Kong and Kim Ryum, "A Cooperative Transmission Scheme for Cluster Based Wireless Sensor Networks,'' International Conference on Intelligent Computing (ICIC 2009), held in Ulsan, Korea, September 16--19, 2009.</li>
                <li>Ho Van Khuong, Hyung Yun Kong, Yun Kyeong Hwang, Asaduzzaman, Gun Seok Kim, Dea Kyu Choi, "Design of A High Order Diversity Cooperative Transmission Scheme Over Rayleigh Fading Channels,'' Proceedings of the Sixth IASTED International Conferences on communication, internet and information technology, held on Banff, Alberta, Canada July 2--4, 2008.</li>
                <li>Hyung Yun Kong, Ho Van Khuong, Asaduzzaman, Sang-Joon Park, Nae-Soo Kim, "Cooperative Communications in A Novel Hcoc-Qam Ofdm Spread Spectrum System with MMSE Equalizer for High Speed Data Transmission,'' Proceedings of the Seventh IASTED International Conferences on Wireless and Optical Communications, held in Montreal, Quebec, Canada, May 30--June 1, 2007</li>

            </ul>

        </div>




    </div>

    <div class="footer" style="margin-top: 3000px; height:90px; background-color: #afd9ee; ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>

</div>



</body>
</html>