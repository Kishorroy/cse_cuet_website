<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:710px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 880px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .center {
            text-align: center;
        }

        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #000;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {background-color: #ddd;}
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li class="active"><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>
    <div class="map">
        <div class="center" >
            <br>
            <table id="customers">
                <tr>
                    <th>Publication Type</th>
                    <th><center>Publication Details</center></th>
                    <th>Year</th>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Mahfuzulhoq Chowdhury, Asaduzzamanand and Md. Fazlul Kader, "An Efficient MAC protocol for Cognitive Radio Networks", In Proc International Conference on Engineering Research, Innovation and Education(ICERIE)’2013 pp.322-327, Shahjalal University of Science & Technology, Bangladesh, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Thomas Chowdhury, Happy Rani Debi, "Online Advertisement System: A New Intelligent Approach", International Conference on Soft Computing and Software Engineering [SCSE’13] , San Francisco, California, USA. , 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Al Amin Hossain, Anik Saha, "An Appearance based Human Motion Tracking", 2nd International Conference on Informatics, Electronics & Vision (ICIEV2013) Page 38, , 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Priyam Biswas, A.K.M.Ashikur Rahman, Kaushik Deb, "Teoretical Aspect of Broadcasting Algorithms in Ad Hoc Wireless Network", The 8th International Forum on Strategic Technology -IFOST 2013 , , 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Muhammad Ibrahim Khan, "An Audio Watermarking method Based on Short Time Fourier Transformation and Singular Value Decomposition", Advanced Science Letter, ISSN No. 1936-6612 , USA, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Muhammad Ibrahim Khan, "Two-Handed Sign Language Recognition for Bangla Character Using Normalized Cross Correlation", Global Journal of Computer Science and Technology, ISSN: 0975-4350 (Print) Vol. 12, Issue 3, pp. 01-06, USA, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Muhammad Ibrahim Khan, "A Moving Object Detection Method Based on Enhanced Edge Localization and Gradient Directional Masking", Advanced Science Letter, ISSN No 1936-6612 (Print), EISSN No. 1936-7317 , USA, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Md. Fazlul Kader and Kaushik Deb, "Neural Network-Based English Alphanumeric Character Recognition", International Journal of Computer Science, Engineering and Applications (IJCSEA) Vol.2, No.4, pp. 119-128, , 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Muhammad Ibrahim Khan, Md. Iqbal Hasan Sarker, Kaushik Deb, and Md. Hasan Farhad, "A New Audio Watermarking Method Based on Discrete Cosine Transform with a Gray Image", International Journal of Computer Science & Information Technology (IJCSIT), ISSN: 0975-3826 Vol. 4, No. 4, pp. 01-10, , 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Kaushik Deb, Muhammad Ibrahim Khan, Helena Parvin Mony and Sujan Chowdhury, "'Two-Handed Sign Language Recognition for Bangla Character Using Normalized Cross Correlation''", Global Journal of Computer Science and Technology, ISSN: 0975-4172 Vol. 12, Issue 3, pp. 01-06, USA, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Kaushik Deb, Tauhidul Alam, Md. Mahammud Ali and Kang-Hyun Jo, "Lossless Image Compression Technique Based on Snack Ordering", Computer Science & Engineering Research Journal, ISSN: 1990-4010 Vol. 7, pp. 30-36, Bangladesh, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Md. Moshiul Hoque, "A Robotic Framework for Controlling Human’s Attention", Paladyn Journal of Behavioral Robotics (JBR) vol. 03, no. 02, pp. 92-101, Springer, UK, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Md. Moshiul Hoque, D. Das, T. Onuki, Y. Kobayashi, and Y. Kuno, "Robotic System Controlling Target Human’s Attention", Intelligent Computing Theories and Applications, LNCS Vol. 7390, pp. 534-544, , Springer, Heidelberg, Germany, 2012</td>
                    <td>2012</td>
                </tr>
            </table>
            <div class="pagination">
                <a href="research2.blade.php">&laquo;</a>
                <a href="research1.blade.php">1</a>
                <a href="research2.blade.php"  >2</a>
                <a href="research3.blade.php" class="active">3</a>
                <a href="research4.blade.php">4</a>
                <a href="research5.blade.php">5</a>
                <a href="research6.blade.php">6</a>
                <a href="research7.blade.php">7</a>
                <a href="research8.blade.php">8</a>
                <a href="research9.blade.php">9</a>
                <a href="research10.blade.php">10</a>
                <a href="research4.blade.php">&raquo;</a>
            </div>
        </div>
    </div>
    <div class="footer" style="height:70px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>