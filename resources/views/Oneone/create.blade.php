@extends('../master1')

@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3>Level 1 Term 1 Routine(Section A)</h3>
            <hr>

            {!! Form::open(['url'=>'/Oneone/store']) !!}

            {!! Form::label('sub_title','Subject Code Title:') !!}
            {!! Form::text('sub_title','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('teacher_name','Teacher Name:') !!}
            {{ Form::select('teacher_name', [
            'Please Select One',
               'MD. Shamsul Arefin' => 'MD. Shamsul Arefin',
               'MD. Ibrahim Khan' => 'MD. Ibrahim Khan',
               'Kaushik Deb' => 'Kaushik Deb',
               'MD. Moshiul Hoque' => 'MD. Moshiul Hoque',
               'Asaduzzaman' => 'Asaduzzaman',
               'Pranab kumar Dhar' => 'Pranab kumar Dhar',
               'MD. Mokammel Haque' => 'MD. Mokammel Haque',
               'Abu Hasnat Mohammad Ashfak Habib' => 'Abu Hasnat Mohammad Ashfak Habib',
               'MD. Obaidur Rahman' => 'MD. Obaidur Rahman',
                'MD. Saki Kowsar' => ' MD. Saki Kowsar',
                'MD. Kamal Hossen' => 'MD. Kamal Hossen',
                 'Rahma Bintey Mufiz Mukta' => 'Rahma Bintey Mufiz Mukta',
                 'Lamia Alam' => 'Lamia Alam',
                'Shayla Sharmin' => 'Shayla Sharmin',
                'MD. Sabir Hossain' => 'MD. Sabir Hossain',
                'Farzana Yasmin' => 'Farzana Yasmin',
               'Animesh Chandra Roy' => 'Animesh Chandra Roy',
               'MD. Shafiul Alam Forhad ' => 'MD. Shafiul Alam Forhad ',
               'Jibon Naher' => 'Jibon Naher',
               'Sharmistha Chandra Tista' => 'Sharmistha Chandra Tista',
               'Tanzina Akter' => 'Tanzina Akter',
               'Priyam Biswash' => 'priyam Biswash']
            ) }}
            <br>
            {!! Form::label('day',' Day:') !!}
            {{ Form::select('day',['Please Select One',
   'Sunday' => 'Sunday',
   'Monday' => 'Monday',
   'Tuesday' => 'Tuesday',
   'Wednesday' => 'Wednesday',
   'Thursday' => 'Thursday']
) }}
            {!! Form::label('time',' Time:') !!}
            {{ Form::select('time',  ['Please Select One',
   '9.00 a.m-9.50 a.m' => '9.00-9.50',
   '9.50 a.m-10.40 a.m' => '9.50-10.40',
   '11.00 a.m-11.50 a.m' => '11.00-11.50',
   '11.50 a.m-12.40 p.m' => '11.50-12.40',
   '12.40 p.m-1.30 p.m' => '12.40-1.30',
   '2.30 p.m-3.20 p.m' => '2.30-3.20',
   '3.20 p.m-4.10 p.m' => '3.20-4.10',
   '4.10 p.m-5.00 p.m' => '4.10-5.00',
   '9.00 am - 10.40 am' => '9.00 am - 10.40 am',
   '11.00 am - 1.30 pm' => '11.00 am - 1.30 pm',
   '2.30 pm - 5.00 pm' => '2.30 pm - 5.00 pm'
   ]
) }}
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}
            <br><br>
            <a href="index"><button type="button" class="btn btn-primary">Full Routine</button></a>

            {!! Form::close() !!}

        </div>
    </div>

@endsection