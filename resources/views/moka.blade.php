<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dr. Md. Mokammel Haque</title>
    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height: 1600px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .nopadding{
            padding: 0;
            margin-top: 30px;
            background: linear-gradient(white ,white , #afd9ee);
        }
        .nopadding h4{
            background: linear-gradient( #afd9ee,white , #afd9ee);
            padding: 10px 15px;
        }

        .nopadding ul li{

            background: url("/public/images/arrow.jpg") no-repeat left -1.6rem;
            padding-left: 10px;
            background-size: 25px 35px;
            margin: 15px 17px;
        }





    </style>
</head>
<body>

<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li class="active"><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>
    </div>

    <div class="col-lg-10" style="margin-left: 100px ">
        <div class="col-md-4">
            <img src="{{URL::asset('/images/teachers/mokammel.jpg')}}" style="width: 300px;height: 300px;margin-top: 20px">
        </div>
        <div class="col-md-8">
            <h1 style="margin-bottom: 0">Dr. Md. Mokammel Haque</h1>
            <br>
            <h2 style="margin: 0;padding: 0;">ডঃ মোঃ মোকাম্মেল হক</h2><hr>
            <h4>Associate Professor</h4>
            <b>E-mail: </b>	malin405@yahoo.com, mokammel@cuet.ac.bd
            <br><b>Contact: </b>+88-031-723336, cell: +88-0172-7210737<hr>
            <b>Research Interests:</b>
            Wireless Sensor Network,   Mobile Ad-Hoc Network,    Cryptography
            <hr>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Education :</b></h4>
            <ul>
                <li><b>B.Sc.</b><br>Computer Science<br>Chittagong University Of Engineering & Technology<b>(CUET)</b><br>Bangladesh</li>
                <li><b>M.Sc.</b><br>KyungHee University, <br>South Korea</li>
                <li><b>PhD</b><br>Macquarie University,<br> Australia</li>
            </ul>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Related Courses(Undergraduate):</b></h4>

        </div>

        <div class="col-sm-12 nopadding" style="background: linear-gradient( white ,#afd9ee, #afd9ee);">
            <h4><b>Publications :</b></h4>
            <h5 align="center"><b>Conferences</b></h5>
            <hr>
            <ul>
                <li>Md. Mokammel Haque, Jobayer Sheikh and Md. Jihan Al Rashid, "An Improved Steganographic Technique Based on Diamond Encoding Method", ECCE 2017 (International Conference onElectrical Computer and Communication Engineering), February 2017, IEEE, CUET, Cox’s Bazar, Bangladesh.</li>
                <li>ChitraBiswas, Udayan Das Gupta and Md. Mokammel Haque, "A Hierarchical Key derivative Symmetric Key Algorithm using Digital Logic ", ECCE 2017 (International Conference onElectrical Computer and Communication Engineering), February 2017, IEEE, CUET, Cox’s Bazar, Bangladesh.</li>
                <li>Md. Mokammel Haqueand Josef Pieprzyk, "Analysing recursive preprocessing of BKZ lattice reduction algorithm", IET Information Security, Institution of Engineering and Technology, doi: 10.1049/iet-ifs.2016.0049, pp. 1-7, June 2016. (SCI-E)</li>
                <li>AnuvaChowdhury, FarzanaAlamTanzila, ShantaChowdhury and Md. Mokammel Haque, "An efficient security architecture for Wireless Sensor Networks using pseudo-inverse matrix", ICCIT 2015 (18thInternational Conference onComputer and Information Technology), December 2015, IEEE, MIST, Dhaka, Bangladesh.</li>
                <li>Md. Mokammel Haqueand Josef Pieprzyk, "Optimizing preprocessing method of recursive-BKZ lattice reduction algorithm", EICT 2015 (2ndInternational Conference onElectrical Information and Communication Technology), December 2015, IEEE, KUET, Khulna, Bangladesh.</li>
                <li>AnuvaChowdhury, FarzanaAlamTanzila, ShantaChowdhury and Md. Mokammel Haque, "A secret key-based security architecture for wireless sensor networks", ICCIE 2015 (1stInternational Conference onComputer and Information Engineering),November 2015, IEEE, RUET, Rajshahi, Bangladesh.	</li>
                <li>AktheruzzamanChwBabu and Md. Mokammel Haque, "A Scalable Framework of Course Scheduling with Graph Coloring Approach", IFOST 2015 (The 10th International Forum onStrategic Technology), June 2015, UniversitasGadjahMada, Bali, Indonesia.</li>
                <li>Md. Mokammel Haque.Joseph Pieprzyk, AnuvaChowdhury and Asaduzzaman. “Predicting Tours of BKZ Lattice Reduction Algorithm”, Computer Science and Engineering Research Journal (CSERJ), Vol: 10, pp. 8-13, 2014.</li>
                <li>Md. Mokammel Haqueand Josef Pieprzyk, "Evaluating the performance of the practical lattice reduction algorithms", ICECE 2014 (International Conference onElectrical and Computer Engineering),December 2014, IEEE, Dhaka, Bangladesh.</li>
                <li>Md. Mokammel Haqueand Josef Pieprzyk, "Predicting tours and probabilistic simulation for BKZ lattice reduction algorithm", IFOST 2014 (The 9th International Forum onStrategic Technology), October 2014, IEEE, Cox’s Bazar, Bangladesh.</li>
                <li>Md. Mokammel Haque, Mohammad ObaidurRahman, Josef Pieprzyk, "Analysing Progressive-BKZ Lattice Reduction Algorithm", NCICIT 2013 (National Conference on Intelligent Computing and Information Technology), November 21, CUET, Chittagong, Bangladesh.</li>
                <li>Md. Mokammel Haque, Al-Sakib Khan Pathan, ChoongSeon Hong and Eui-Nam Huh, "An Asymmetric Key-Based Security Architecture for Wireless Sensor Networks", KSII TIIS (Transactions on Internet and Information Systems) Volume 2, Number 5, October 2008, pp.265-279. (SCI-E)</li>
                <li>Md. Mokammel Haque, Al-Sakib Khan Pathan, and ChoongSeon Hong, "S-PkSec: An Asymmetric Key Based Security Management Scheme for Sensor Network Operation", Proceedings of KNOM (Korean Network Operations and Management) Conference 2008 (TS6-5), April 24-25, Changwon, Korea</li>
                <li>Md. Mokammel Haque, Al-Sakib Khan Pathan and ChoongSeon Hong, "Securing U-Healthcare Sensor Networks using Public Key Based Scheme", Proceedings of the 10th International Conference on Advanced Communication Technology (IEEE ICACT 2008), Volume II, Phoenix Park, Korea, pp. 1108-1111, February 17-20 2008</li>
                <li>Md. Mokammel Haque, Al-Sakib Khan Pathan, Byung Goo Choi, and ChoongSeon Hong, "An Efficient PKC-Based Security Architecture for Wireless Sensor Networks", Proceedings of the IEEE Military Communications Conference (IEEE MILCOM 2007), October 29-31, Orlando, Florida, USA. (IEEE Student Travel Grant Award)</li>
                <li>Md. Mokammel Haque, Al-Sakib Khan Pathan, and ChoongSeon Hong, "An Efficient Public Key Based Security Architecture for Wireless Sensor Networks", (Poster) Proceedings of the 27th KIPS Spring Conference, 11-12 May 2007, Seongnam, Korea, pp. 1098-1099</li>
                <li>Al-Sakib Khan Pathan, Jae Hyun Ryu, Md. Mokammel Haque, and ChoongSeon Hong, "Security Management in Wireless Sensor Networks with a Public Key Based Scheme", Proceedings of the 10th Asia-Pacific Network Operations and Management Symposium (APNOMS 2007), 10-12 October, 2007, Sapporo, Japan, (Shingo Ata and ChoongSeon Hong Eds.) Lecture Notes in Computer Science (LNCS), Volume 4773, Springer-Verlag 2007, pp. 503-506</li>
                <li>ChoongSeon Hong, Md. Mokammel Haque, Al-Sakib Khan Pathan, and GihyukHeo, "A Scheme for Data Encryption Decryption Based on Asymmetric Key", The Application Number: 10-2007-0067841, South Korean Patents Administration, 2007.07.06 (Korean Patent)</li>
                <li>Md. Mokammel Haque,ChoongSeon Hong and Md.Tazul Islam "Secure in Network Data Aggregation Mechanism for Wireless Sensor Networks", CSERJ (Computer Science and Engineering Research Journal) Vol. 04 (2006) ISSN:1990-4010.</li>
                <li>Md. MoshiulHaque, Md. Mokammel Haqueand PranabKantiDhar “Semantic Analysis of Bangla Sentences: A compositional Semantics Approach”, The Proceeding of International Conference on Computer Processing of Bangla (ICCPB) 2006, Page: 106-112</li>
            </ul>

        </div>




    </div>

    <div class="footer" style="margin-top: 2100px; height:90px; background-color: #afd9ee; ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>

</div>



</body>
</html>