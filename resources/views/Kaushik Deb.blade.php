<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dr. Kaushik Deb</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height: 1600px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .nopadding{
            padding: 0;
            margin-top: 30px;
            background: linear-gradient(white ,white , #afd9ee);
        }
        .nopadding h4{
            background: linear-gradient( #afd9ee,white , #afd9ee);
            padding: 10px 15px;
        }

        .nopadding ul li{

            background: url("/public/images/arrow.jpg") no-repeat left -1.6rem;
            padding-left: 10px;
            background-size: 25px 35px;
            margin: 15px 17px;
        }





    </style>
</head>
<body>

<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li class="active"><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>
    </div>

    <div class="col-lg-10" style="margin-left: 100px ">
        <div class="col-md-4">
            <img src="{{URL::asset('/images/teachers/kaushik.jpg')}}" style="width: 300px;height: 300px;margin-top: 20px">
        </div>
        <div class="col-md-8">
            <h1 style="margin-bottom: 0">Dr. Kaushik Deb</h1>
            <br>
            <h2 style="margin: 0;padding: 0;">ডঃ কৌশিক দেব</h2><hr>
            <h4>Professor Dept. of CSE
                & Dean, Faculty of Electrical & Computer Engineering <b>(ECE)</b></h4>
            <b>E-mail:</b>debkaushik99@cuet.ac.bd, debkaushik99@gmail.com
            <br><b>Contact: </b>01914745508<hr>
            <b>Research Interests:</b>
            Image Information and Vision Technology, Intelligent Transportation Systems, Pattern Recognition<hr>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Education :</b></h4>
            <ul>
                <li><b>B.Sc.</b><br>Computer Science<br>Russia</li>
                <li><b>M.Sc.</b><br>Computer Science<br>Russia</li>
                <li><b>PhD</b><br>South Korea</li>
            </ul>
        </div>
        <div class="col-sm-12 nopadding">
            <h4><b>Related Courses(Graduate):</b></h4>
            <ul>
                <li>Advanced Digital Image Processing</li>
                <li>Pattern Recognition</li>
                <li>Computer Vision</li>
            </ul>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Related Courses(Undergraduate):</b></h4>
            <div class="col-md-6">
                <ul>
                    <li>Digital Image Processing</li>
                    <li>Discrete Mathematics</li>
                    <li>Computer Architecture</li>
                    <li>Computer Basics & Programming</li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul>
                    <li>System Analysis and Design</li>
                    <li> Data Base Management System</li>
                    <li> Artificial Intelligence</li>
                </ul>
            </div>
        </div>
        <div class="col-sm-12 nopadding">
            <h4><b>Reviewer :</b></h4>
            <ul>
                <li>International Journal of Computational Vision and Robotics (IJCVR)</li>
                <li>Neurocomputing</li>
                <li>Security and Communication Networks</li>
                <li>Journal of Computers</li>
                <li>Neural Computing and Application</li>
                <li>International Journal of Control, Automations and Systems (IJCAS)</li>
                <li>International Journal of Modelling, Identification and Control (IJMIC)</li>
                <li>Intelligent Service Robotics (ISR)</li>
                <li>Journal of Zhejiang University SCIENCE (JZUS)</li>
                <li>World Journal of Medicine and Medical Science Research</li>
                <li>Computer Science and Research Journal (CSERJ), Associate Editor (Vol. 7) & Editor (Vol. 8 & 9)</li>
                <li>2013 International Conference on Electrical, Information and Communication Technology (EICT 2013)</li>
                <li>2013 International Conference on Emerging Technologies (ICET-2013)</li>
                <li>2013 International Conference on Intelligent Computing (ICIC 2013)</li>
                <li>2013 8th IEEE International Forum on Strategic Technology (IEEE IFOST 2013)</li>
                <li>2012 International Conference on Intelligent Computing (ICIC 2012)</li>
                <li>2010 5th IEEE International Forum on Strategic Technology (IEEE IFOST 2010)</li>
                <li>2009 International Conference on Intelligent Computing (ICIC 2009)</li>
                <li>2009 15th Korea-Japan Joint Workshop on Frontiers of Computer Vision (FCV 2009)</li>
                <li>2008 International Conference on Intelligent Computing (ICIC 2008)</li>

            </ul>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Awards :</b></h4>
            <ul>
                <li> Received Best Paper Award for the outstanding scientific research and presentation entitled “Stairways Detection and Distance Estimation Approach Based on Three Connected Point and Triangular Similarity”, In Proceeding of the 9th IEEE International Conference on Human System Interaction (IEEE HSI 2016), pp. 330-336, University of Portsmouth, Portsmouth, UK, July 6-8, 2016.</li>
                <li> Received Best Paper Award for the outstanding scientific research and presentation entitled “Optical Recognition of Vehicle License Plates” In Proceeding of the 6th IEEE International Forum on Strategic Technology (IEEE IFOST 2011), Harbin, China, and Aug. 22-24, 2011.</li>
                <li>Received a Korean Government IT Scholarship from Institute of Information Technology Advancement (IITA) for Ph. D program in University of Ulsan, South Korea.</li>
                <li>Received Outstanding Paper Award for the outstanding scientific research and presentation entitled “Vehicle License Plate Tilt Correction Based on the Straight Line Fitting Method” in the 17th ITS World Congress, Busan, S. Korea, Oct. 25-29, 2010.</li>
                <li>Received a Russian Government Scholarship after achieving excellent results in Undergraduate level in Tula State University for Masters Course in Russia.</li>

            </ul>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Professional Key Activities :</b></h4>
            <ul>
                <li>International Program Committee Member, International Conference on Human System Interaction (HSI 2017)</li>
                <li>TPC member, International Conference on Electrical Information and Communication Technology (EICT 2017)</li>
                <li>TPC member, International Conference on Electrical, Computer and Communication Engineering (ECCE 2017)</li>
                <li>TPC member, International Conference on Informatics, Electronics & Vision (ICIEV 2016, 2015)</li>
                <li>TPC member, iciVPR 2016</li>
                <li>TPC member, International Conference on Electrical Engineering and Information & Communication Technology (iCEEiCT 2016, 2015, 2014)</li>
                <li>TPC member, International Conference on Innovations in Science, Engineering and Technology 2016 (ICISET 2016)</li>
                <li>Organizing Committee member, International Conference on Telecommunications and Photonics (ICTP 2015)</li>
                <li>Member, Technical Committee, International Conference on Computer, Communication, Control and Information Technology (C3IT-2015), to be held on 7th and 8th February 2015 at the AOT college campus in Adisaptagram, Hooghly, West Bengal, India.</li>
                <li>Organizing Secretary, 9th International Forum on Strategic Technology (IFOST 2014). This International conference was held on 21-23 October, 2014 at Cox’s Bazar, Bangladesh</li>
                <li>Member, Steering committee, ICACE 2014</li>
                <li>Member, Advisory Committee, ICMERE 2013</li>
                <li>Coordinator of International Forum on Strategic Technology (IFOST), CUET</li>
                <li>Organizing Chair, 1st National Conference on Intelligent Computing & Information Technology (NCICIT 2013). This National Conference was held on 21st November, 2013 at CUET.</li>
                <li>Member, International Journal of Computer Science Applications & Information Technologies (IJCSAIT)</li>
                <li>Member, Technical Program Committee of the 9th IEEE International Conference on Emerging Technologies (ICET-2013)</li>
                <li>Member, IEEE</li>
                <li>Member, IEB</li>

            </ul>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Keynote Talk :</b></h4>
            “The Study of Vehicle License Plate Recognition for Intelligent Transportation Systems” National Conference on Natural Sciences & Technology (NCNST-2014), Asian University for Women (AUW), April 24-25, 2014.
        </div>

        <div class="col-sm-12 nopadding" style="background: linear-gradient( white ,#afd9ee, #afd9ee);">
            <h4><b>Publications :</b></h4>
            <h5 align="center"><b>Journals </b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>Md. Khaliluzzaman and Kaushik Deb, “Stairways Detection Based on Approach Evaluation and Vertical Vanishing Point”, International Journal of Computational Vision and Robotics (IJCVR), ISSN Print: 1752-9131, Vol. , No., pp. - , Inderscience Publications, 2017. (Accepted for publication)</li>
                <li>Linkon Chowdhury, Mohammad Ibrahim Khan, Kaushik Deb, and Sarwar Kamal, “MetaG: a graph-based metagenomic gene analysis for big DNA data”, Network Modeling Analysis in Health Informatics and Bioinformatics, DOI 10.1007/s13721-016-0132-7, Online ISSN 2192-6670, Vol. 05, pp. 1-16, Springer Vienna, 2016.</li>
                <li>Md. Moshiul Hoque, Quazi Delwar Hossian, Kaushik Deb, Dipankar Das, Y. Kobayashi, and Y. Kuno, “An Empirical Robotic Framework for Initiating Interaction with the Target Human in Multiparty Settings”, Journal of Circuits, Systems, and Computers, DOI: 10.1142/S0218126615400034, ISSN: 0218-1266 , Vol. 24, No.2, pp. 1540001-25, World Scientific Publishing Company, 2015./li>
                <li>Kaushik Deb, Animash Kar and Ashraful Huq Suny, “ Cast Shadow Detection and Removal of Moving Objects from Video Based on HSV Color Space”, Smart Computing Review, DOI: 10.6029/smartcr.2014.01.003, Vol. 5, No.1, pp. 38-50, The Korea Academia-Industrial Cooperation Society (KAIS), South Korea, 2015.</li>
                <li>Kaushik Deb, Kazi Zakia Sultana and Tahmina Khanam, “Detecting Anomalous Behavior of Credit Card Transaction using Statistical Approach”, The Chittagong University Journal of Science, Vol-37,pp.-62-75,2015.</li>
                <li>Kaushik Deb and Ashraful Huq Suny, “Shadow Detection and Removal Based on YCbCr Color Space”, Smart Computing Review, DOI: 10.6029/smartcr.2014.01.003, Vol. 4, No.1, pp. 23-33, The Korea Academia-Industrial Cooperation Society (KAIS), South Korea, 2014.</li>
                <li>Kaushik Deb, Sayem Mohammad Imtiaz, and Priyam Biswas, “A Motion Region Detection and Tracking Method”, Smart Computing Review, DOI: 10.6029/smartcr.2014.01.008, Vol. 4, No.1, pp.79-90, The Korea Academia-Industrial Cooperation Society (KAIS), South Korea, 2014.</li>
                <li>Kaushik Deb, Md. Ashikur Rahaman, Kazi Zakia Sultana, Md. Iqbal Hasan Sarker, and Ui-Pil Chong, “DCT and DWT Based Robust Audio Watermarking Scheme for Copyright Protection”, Journal of theInstitute of Signal Processing and Systems, ISSN: 1229-9480, Vol. 15, No. 1, pp. 1-8, The Korea Institute of Signal Processing and Systems (KISPS), South Korea, 2014.</li>
                <li>Muhammad Kamal Hossen and Kaushik Deb, “ Vehicle License Plate Detection and Tilt Correction Based on HSI Color Model and SUSAN Corner Detector”, Smart Computing Review, DOI: 10.6029/smartcr.2014.01.003, Vol. 4, No.5, pp. 371-388, The Korea Academia-Industrial Cooperation Society (KAIS), South Korea, 2014.</li>
                <li>Md. Moshiul Hoque, Y. Kobayashi, Y. Kuno, and Kaushik Deb, “Design A Robotic Head to Attract Human Attention by Considering Viewing Situations”, Computer Science & Engineering Research Journal (CSERJ), ISSN: 1990-4010, Vol. 8, pp. 11-21, Bangladesh, 2013.</li>
                <li>Kaushik Deb, Md. Sajib Al-Seraj, and Ui-Pil Chong, “A Low Frequency Band Watermarking with Weighted Correction in the Combined Cosine and Wavelet Transform Domain”, Journal of the Institute of Signal Processing and Systems, ISSN: 1229-9480, Vol. 14, No. 1, pp. 13-20, The Korea Institute of Signal Processing and Systems (KISPS), South Korea, 2013.</li>
                <li>Md. Fazlul Kader and Kaushik Deb, ''Neural Network-Based English Alphanumeric Character Recognition” International Journal of Computer Science, Engineering and Applications (IJCSEA) Vol.2, No.4, pp. 119-128, 2012.</li>
                <li>Muhammad Ibrahim Khan, Md. Iqbal Hasan Sarker, Kaushik Deb, and Md. Hasan Farhad, ''A New Audio Watermarking Method Based on Discrete Cosine Transform with a Gray Image”, International Journal of Computer Science & Information Technology (IJCSIT), ISSN: 0975-3826, Vol. 4, No. 4, pp. 01-10, 2012.</li>
                <li>Anik Saha, Dipanjan Das Roy, Tauhidul Alam and Kaushik Deb, ''Automated Road Line Detection for Intelligent Vehicle'' Global Journal of Computer Science and Technology, ISSN: 0975-4172, Vol. 12, Issue 6, pp. 01-05, Global Journals Inc., USA, 2012</li>
                <li>Kaushik Deb, Muhammad Ibrahim Khan, Helena Parvin Mony and Sujan Chowdhury, ''Two-Handed Sign Language Recognition for Bangla Character Using Normalized Cross Correlation'' Global Journal of Computer Science and Technology, ISSN: 0975-4172, Vol. 12, Issue 3, pp. 01-06, Global Journals Inc., USA, 2012.</li>
                <li>Kaushik Deb, Tauhidul Alam, Md. Mahammud Ali and Kang-Hyun Jo, ''Lossless Image Compression Technique Based on Snack Ordering” Computer Science & Engineering Research Journal, ISSN: 1990-4010, Vol. 7, pp. 30-36, Bangladesh, 2012.</li>
                <li>Md. Iqbal Hasan Sarker, Muhammad Ibrahim Khan, Kaushik Deb, and Md. Faisal Faruque, “FFT –based Audio Watermarking Method with a Gray Image for Copyright Protection”, International Journal of Advanced Science & Technology (IJAST), ISSN: 2005-4238, Vol. 47, pp. 65-76, 2012.</li>
                <li>Kaushik Deb, My Ha Le, Byung-Seok Woo and Kang-Hyun Jo, ''Automatic Vehicle Identification by Plate Recognition for Intelligent Transportation Systems'' Journal of Lecture Notes in Artificial Intelligence, ISSN: 0302-9743, Part II, Vol. 6704, pp. 163-172, Springer, Germany, 2011.</li>
                <li>Kaushik Deb and Kang-Hyun Jo, ''Vehicle License Plate Detection Method Based on HDR Image Generation from Multiple Computers” Computer Science & Engineering Research Journal, ISSN: 1990-4010, Vol. 6, pp. 27-34, Bangladesh, 2011.</li>
                <li>Kaushik Deb, Andrey Vavilin, Jung-Won Kim, and Kang-Hyun Jo, ''Vehicle License Plate Tilt correction Based on the Straight Line Fitting Method and Minimizing Variance of Coordinates of Projection Points'' International Journal of Control, Automations and Systems, ISSN: 2005-4092 (Electronic), Vol. 8, No.5, pp. 27-34, Springer, Germany, Oct. 2010.</li>
                <li>Pranab Kumar Dhar, Mohammed Ibrahim Khan, Kaushik Deb, and Jong-Myon Kim, ''A Modified Spectral Modeling Synthesis Algorithm for Whale Sound'' International Journal of Computer Science and Network Security, ISSN: 1738-7906, pp. 34-41, Vol. 10, No.9, S. Korea, Sep. 2010.</li>
                <li>Andrey Vavilin, Kaushik Deb, and Kang-Hyun Jo, ''Fast HDR Image Generation Technique based on Exposure Blending'' Journal of Lecture Notes in Artificial Intelligence, ISSN: 0302-9743 print, Part III, Springer, Germany, 2010.</li>
                <li>Kaushik Deb and Kang-Hyun Jo, ''A Vehicle License Plate Detection Method for Intelligent Transportation System Applications'' Cybernetics and Systems: An International Journal, ISSN: 1087-6553 online, Vol. 40, Issue 8, pp. 689-705, Taylor & Francis, UK, 2009.</li>
                <li>Kaushik Deb and Kang-Hyun Jo, “Vehicle License Plate Detection Algorithm Based on Color Space and Geometrical Properties”, Journal of Lecture Notes in Computer Science, ISSN: 0302-9743 print, Vol. 5754, pp. 555-564, Springer, Germany, 2009.</li>
                <li>Kaushik Deb, Hyun-Uk Chae, and Kang-Hyun Jo, "Vehicle License Plate Detection Method Based on Sliding Concentric Windows and Histogram", Journal of Computers, ISSN: 1796-203X, Vol. 4, No. 8, pp. 771-777, Academy Publisher, Finland, 2009.</li>
                <li>Kaushik Deb, Hee-Chul Lim, Suk-Ju Kang, and Kang-Hyun Jo, “An Efficient Method of Vehicle License Plate Detection Based on HSI Color Model and Histogram“, Journal of Lecture Notes in Artificial Intelligence, ISSN: 0302-9743 print, Vol. 5579, pp. 66-75, Springer, Germany, 2009.</li>
                <li>Kaushik Deb, Suk-Ju Kang, and Kang-Hyun Jo, ''Statistical Characteristics in HSI Color Model and Position Histogram based Vehicle License Plate Detection’’ Journal of Intelligent Service Robotics, ISSN: 1861-2776 print, Vol. 2, No. 3, pp. 173-186, Springer, Germany, 2009.</li>
                <li>Hee-Chul Lim, Kaushik Deb, and Kang-Hyun Jo, “Geometrical Reorientation of Distorted Road Sign using Projection Transformation for Road Sign Recognition”, Journal of Institute of Control, Robotics and Systems, Vol. 15, No., 11, pp. 1088-1095, Korea, 2009.</li>
                <li>Kaushik Deb and Kang-Hyun Jo, “Geometrical Property and Histogram Based Korean Vehicle License Plate Detection”, Computer Science & Engineering Research Journal, ISSN: 1990-4010, Vol. 4, pp. 56-62, Bangladesh, 2008.</li>
                <li>Kaushik Deb and Thomas Chowdhury, “Construction of a simple Robotic Arm and Control by Software”, Computer Science & Engineering Research Journal, ISSN: 1990-4010, Vol. 3, pp. 31-37, Bangladesh, 2006.</li>
                <li>M. S. Arefin, M. M. Hoque, M.T. Islam, and Kaushik Deb, “Structural Operational Semantics of Inheritance and Object creation in JAVA”, Computer Science & Engineering Research Journal, ISSN: 1990-4010, Vol. 2, pp. 29-35, Bangladesh, 2005.</li>
            </ul>


            <hr>
            <h5 align="center"><b>Conferences</b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>Soumen Chakraborty, Mohammed Nasir Uddin and Kaushik Deb, “Bangladeshi Road Sign Recognition Based on DtBs Vector and Artificial Neural Network”, In Proceedings of the International Conference on Electrical, Computer & Communication Engineering (ECCE 2017), Cox's Bazar, Bangladesh, Feb. 16-18, 2017.</li>
                <li>Md. Khaliluzzaman and Kaushik Deb, “Zebra-crossing Detection Based on Geometric Feature and Vertical Vanishing Point”, In Proceeding of the IEEE 3rd International Conference on Electrical Engineering and Information & Communication Technology (iCEEiCT 2016), MIST, Dhaka, Bangladesh, September 22-24, 2016.</li>
                <li>Shaiyan Ameen Chowdhury, M. N. Uddin, M.S. Kowsar and Kaushik Deb, "Occlusion handling and human detection based on Histogram of Oriented Gradients for automatic video surveillance," IEEE International Conference on Innovations in Science, Engineering and Techno</li>
                <li>Tahmina Khanam and Kaushik Deb, “Human and Carried Baggage Detection & Classification based on RSD-HOG in Video Frame", In Proceedings of the 9th International Conference on Electrical & Computer Engineering (ICECE 2016), pp. 415 - 418, BUET, Dhaka, Bangladesh, Dec. 20-22, 2016.</li>
                <li>Md. Khaliluzzaman, Deepak Kumar Chy., Kaushik Deb, “Analyzing Image Transmission Quality using Filter and C-QAM”, In Proceeding of the 1st International Conference on Advancement of computer Communication & Electrical Technology (ACCET 2016), Murshidabad, INDIA, October 21-22, Taylor & Francis, 2016.</li>
                <li>Md. Khaliluzzaman and Kaushik Deb, “Support Vector Machine for Overcoming the Problem of Vanishing Point during Stairways Detection”, In Proceeding of the 2nd International Conference on Electrical, Computer & Telecommunication Engineering (ICECTE 2016), RUET, Rajshahi, Bangladesh, December 8-10, IEEE, 2016.</li>
                <li>Md. Khaliluzzaman and Kaushik Deb, “Analyzing MRI Segmentation Based on Wavelet and BEMD using Fuzzy C-Means Clustering”, In Proceeding of the International Workshop on COMPUTATIONAL Intelligence (IWCI 2016), pp. 15-20, Jahangirnagar University, Dhaka, Bangladesh, December 12-13, IEEE, 2016.</li>
                <li>Animash Kar and Kaushik Deb, “Moving cast shadow detection and removal from Video based on HSV color space”, In Proceedings of the 2nd International Conference on Electrical Engineering & Information Communication Engineering (ICEEICT 2015), Dhaka, Bangladesh, May 21-23, 2015.</li>
                <li>Soumen Chakraborty and Kaushik Deb, “Bangladeshi road sign detection based on YCbCr color model and DtBs vector”, In Proceedings of the International Conference on Computer & Information Engineering (ICCIE 2015), pp. 158 - 161, Rajshahi, Bangladesh, Nov. 26-27, 2015.</li>
                <li>Kazi Anika Hossain Naima, Tahmina Khhanm and Kaushik Deb, “Color Image Watermarking Based on LU Decomposition”, In Proceedings of the 1st International Conference on Mathematics and Its Applications (ICMA 2015), pp. 47-52, Khulna, Bangladesh, December 23, 2015</li>
                <li>Arpita Chowdhury, Tahmina Khhanm and Kaushik Deb, “An Efficient Framework for Detecting Face Region Based on Skin Color Modeling and SVM Classifier”, In Proceedings of the 1st International Conference on Mathematics and Its Applications (ICMA 2015), pp. 42-46, Khulna, Bangladesh, December 23, 2015</li>
                <li>Md. Rahat Mahmud, Tahmina Khanam and Kaushik Deb, “Video Watermarking Scheme Using Second Level Discrete Wavelet Transformation (DWT) and Fast Fourier Transformation”, In Proceedings of the 3rd International Conference on Mechanical Engineering and Renewable Energy(ICMERE 2015), Chittagong, Bangladesh, December 26-29, 2015.</li>
                <li>Prianka Banik and Kaushik Deb, “Detecting Books from Bookshelf of Multiple Cells”, In Proceedings of the International Conference on Engineering Research, Innovation & Education (ICERIE), pp. 417-421, Sylhet, Bangladesh, Jan. 11-13, 2013.</li>
                <li>Md. Moshiul Hoque, Kaushik Deb, Dipankar Das, Y. Kobayashi, and Y. Kuno, “An Intelligent Human-Robot Interaction Framework to Control the Human Attention”, In Proceedings of the 2nd International Conference on Informatics, Electronics & Vision (ICIEV), Dhaka, Bangladesh, May 17-18, 2013.	</li>
                <li>Swati Nigam, Kaushik Deb, and Ashish Khare, “Moment Invariants Based Object Recognition for Different Pose and Appearances in Real Scenes”, In Proceedings of the 2nd International Conference on Informatics, Electronics & Vision (ICIEV), Dhaka, Bangladesh, May 17-18, 2013.</li>
                <li>Kaushik Deb, Md. Ibrahim Khan, Anik Saha, and Kang-Hyun Jo, ''An Efficient Method of Vehicle License Plate Recognition Based on Sliding Concentric Windows and Artificial Neural Network” , In Proceeding of the 2nd International Conference on Computer, Communication, Control and Information Technology (C3IT 2012), pp. 42, West Bengal, India, Feb. 25-26, 2012.</li>
                <li>Sujan Chowdhury, Kayasar Md. Mahedi, and Kaushik Deb, “Designing a Semantic Web Ontology of agricultural domain”, In Proceedings of the 7th International Forum on Strategic Technology (IFOST), Tomsk, Russia, Sept. 18-21, 2012.</li>
                <li>Kaushik Deb, Sajib Al-Seraj, Md. Saki Kowser, Iqbal Hasan Sarker, “A joint DWT-DCT Based Watermarking Technique for Avoiding Unauthorized Replication”, In Proceedings of the 7th International Forum on Strategic Technology (IFOST), Tomsk, Russia, Sept. 18-21, 2012.</li>
                <li>Kaushik Deb, Muhammad Kamal Hossen, Md. Ibrahim Khan, and Md. Rafiqul Alam, “Bangladeshi Vehicle License Plate Detection Method Based on HSI Color Model and Geometrical Properties”, In Proceedings of the 7th International Forum on Strategic Technology (IFOST), Tomsk, Russia, Sept. 18-21, 2012.</li>
                <li>Md. Moshiul Hoque and Kaushik Deb, “Robotic System for Making Eye Contact Pro-activity with Humans”, In Proceedings of the 7th IEEE International Conference on Electrical & Computer Engineering (ICECE), pp. 125-128, Dhaka, Bangladesh, Dec. 20-22, 2012.</li>
                <li>Kaushik Deb, Md. Sajib Al-Seraj, Md. Moshiul Hoque and Md. Iqbal Hasan Sarker, “Combined DWT-DCT Based Digital Watermarking Technique for Copyright Protection”, In Proceedings of the 7th IEEE International Conference on Electrical & Computer Engineering (ICECE), pp. 458-461, Dhaka, Bangladesh, Dec. 20-22, 2012.</li>
                <li>Kaushik Deb, Md. Ibrahim Khan, Md. Rafiqul Alam, and Kang-Hyun Jo, ''Optical Recognition of Vehicle License Plates” , In Proceeding of the 6th IEEE International Forum on Strategic Technology (IEEE IFOST 2011), pp. 743-748 Harbin, China, Aug. 22-24, 2011. (Received Best Paper Award)</li>
                <li>Anik Saha and Kaushik Deb, ''Bangla Character Recognition by using Artificial Neural Network '' In Proceeding of the International Conference on Mechanical Engineering and Renewable Energy (ICMERE 2011), CUET, Chittagong, Bangladesh, Dec. 22-24, 2011.</li>
                <li>Sujan Chowdhury and Kaushik Deb, ''Developing a Bangla Spell Checker With Possible Suggestions by using Longest Common Sub Sequence and Soundex algorithm '' In Proceeding of the International Conference on Mechanical Engineering and Renewable Energy (ICMERE 2011), CUET, Chittagong, Bangladesh, Dec. 22-24, 2011.</li>
                <li>Andrey Vavilin, Kaushik Deb, Tae-Ho Kim, and Kang-Hyun Jo, ''Road Sign Detection Method based on Fast HDR Image Generation Technique'' In Proceeding of the 25th IEEE International Conference of Image and Vision Computing New Zealand (IVCNZ 2010), Queenstown, New Zealand, Nov. 08-09, 2010.</li>
                <li>My-Ha Le, Kaushik-Deb, and Kang-Hyun Jo, ''Recognizing Outdoor Scene Objects Using Texture Features and Probabilistic Appearance Model'', In Proceeding of the 10th International Conference on Control Automation and Systems (ICCAS 2010), pp. 1440-1444, Kintex, Gyeonggi-do, Korea, Oct. 27-30, 2010.	</li>
                <li>Kaushik-Deb, Andrey Vavilin, Jung-Won Kim, and Kang-Hyun Jo, ''Vehicle License Plate Tilt Correction Based on the Straight Line Fitting Method '', In proceeding of the 17th ITS (Intelligent Transportation Systems) World Congress, Busan, Korea, Oct. 25-29, 2010. (Received Outstanding Paper Award)</li>
                <li>Pranab Kumar Dhar, Kaushik-Deb, and P. M. Mahamudul Hassan, ''A Modified New Reno for performance Enhancement of TCP in Wireless Network'', In Proceeding of the 5th IEEE International Forum on Strategic Technology (IEEE IFOST 2010), Ulsan, S. Korea, Oct. 13-15, 2010.</li>
                <li>Md. Ibrahim Khan, Sujon Chowdhury, Alok Kumar Chowdhury, and Kaushik-Deb, ''An Efficient Algorithm for finding the Control Point of 3rd order Bezier Curve'', In Proceeding of the 5th IEEE International Forum on Strategic Technology (IEEE IFOST 2010), Ulsan, S. Korea, Oct. 13-15, 2010.	</li>
                <li>Kaushik-Deb, Andrey Vavilin, Jung-Won Kim, and Kang-Hyun Jo, ''Projection and Least Square Fitting with Perpendicular Offsets based Vehicle License Plate Tilt Correction'', In Proceeding of the IEEE SICE (The Society of Instrument and Control Engineers) 2010, Taipei, Taiwan, Aug. 18-21, 2010.</li>
                <li>Kaushik-Deb, Andrey Vavilin, and Kang-Hyun Jo, ''An Efficient Method for Correcting Vehicle License Plate Tilt'', In Proceeding of the IEEE International Conference on Granular Computing (IEEE GrC 2010), Silicon Valley, USA, Aug. 14-16, 2010.</li>
                <li>Andy Vavilin, Kaushik Deb and Kang-Hyun Jo, ''License Plate Detection Method Based on HDR Image Generation from Multiple Cameras '', In Proceeding of the Image Processing and Image Understanding (IPIU 2010), JeJu, Korea, Jan. 27-29, 2010.</li>
                <li>Kaushik Deb, Suk-Ju Kang, and Kang-Hyun Jo, “Korean Vehicle License Plate Detection Based on Statistical and Geometrical Properties in HSI Color Space”, In proceeding of the 15th Japan-Korea Joint workshop on frontiers of Computer Vision (FCV 2009), pp. 18-23, Andong, Korea, Feb. 5-7, 2009</li>
                <li>Kaushik-Deb, Hee-Chul Lim and Kang-Hyun Jo, ''Vehicle License Plate Extraction Based on Color and Geometrical Features'', In Proceeding of the IEEE International Symposium on Industrial Electronics (ISIE 2009), pp.1650-1655, Seoul, Korea, Jul. 5-8, 2009.</li>
                <li>Kaushik Deb, Kang, Suk-Ju, Kang-Hyun Jo, "A Hybrid Vehicle License Plate Detection Method Based on HSI Color Model and Histogram," In proceeding of the IEEE International conf. on Ubiquitous Robots and Ambient Intelligence (URAI 2008), pp. 262-267, Seoul, Korea, 2008.</li>
                <li>Kaushik Deb, Heechul Lim, Kang-Hyun Jo, "Korean Vehicle License Plate Extraction Based on HSI Color Model and Histogram", In proceeding of the Annual conference on Korean Society of Automotive Engineers (KSAE 2008), pp. 132, Daejeon, Korea, 2008.</li>
                <li>Kaushik Deb and Kang-Hyun Jo, "HSI Color based Vehicle License Plate Detection", In proceeding of the IEEE International Conference on Control, Automation and Systems (ICCAS 2008), pp.687-691,Seoul, Korea, 2008.	</li>
                <li>Kaushik Deb, Hyun-Uk Chae, and Kang-Hyun Jo, “Parallelogram and Histogram based Vehicle License Plate Detection”, In proceeding of the IEEE International Conf. on Smart Manufacturing Application (ICSMA 2008), pp 349-353, Gyeonggi-do, Korea 2008.</li>
                <li>Kaushik Deb and Kang-Hyun Jo, “Korean Vehicle License Plate Detection Based on Geometrical and Colour Properties”, In proceeding of the 14th Korea-Japan Joint Workshop on Frontiers of Computer Vision (FCV 2008), pp.118-123, Beppu, Oita, Japan 2008.</li>
                <li>M.A.H. Bhuiyan and Kaushik Deb, “A Localized Queuing Based Router-Centric Approach to Congestion Avoidance and Recovery for Connectionless datagram Service”, In proceeding of the IEEE First International Conf. on Next-Generation Wireless Systems (ICNEWS 2006), pp 335-339, Dhaka, Bangladesh 2006.</li>
                <li>Kaushik Deb and V. L. Tokarev, “Information System for Medical Firm”, In proceeding of the International Scientific and Technical Conference of the Young Scientist and Experts ‘XXVI Gagarin’s Lecture’, Vol. 19, pp. 507-510, Moscow, Russia, 2000.</li>
                <li>Kaushik Deb, “Verbs in Russian Terminology of Computer Facilities and Computer Science”, In proceeding of the 7th Interuniversity Student Readings on Socially-Humanitarian Problems, The Science and the Society, Vol. 7, pp. 121-122, Tula, Russia, 1998.</li>




            </ul>

        </div>




    </div>
    <div class="footer" style="margin-top: 7800px; height:90px; background-color: #afd9ee; ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>



</div>



</body>
</html>