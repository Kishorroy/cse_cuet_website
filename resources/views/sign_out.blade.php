@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                            @foreach($teacher_datas as $key=> $value)
                                <h3> {!! $value->t_name !!} <br> </h3>
                                <i> {!! $value->t_designation!!} </i>
                            @endforeach

                            <br>
                          {{-- email: {!!$email !!} <br>
                            status: {!!$checker  !!}

--}}

                            <table class="table table-bordered table table-striped" >

                                <th style="width: 25px; background-color: #4bb1b1">Day/Time</th>
                                <th style="width: 50px; background-color: #259d6d">9.00am - 9.50am</th>
                                <th style="width: 50px; background-color: #259d6d">9.50am - 10.40am</th>
                                <th style="width: 50px; background-color: #259d6d"> &nbsp; &nbsp;</th>
                               <!-- <th style="width: 50px; background-color: #259d6d"> &nbsp; &nbsp; <br> B <br> R <br> E <br> A<br> K<br></th> -->
                                <th style="width: 50px; background-color: #259d6d">11.00am - 11.50am</th>
                                <th style="width: 50px; background-color: #259d6d">11.50am - 12.40pm</th>
                                <th style="width: 50px; background-color: #259d6d">12.40pm - 1.30pm</th>
                                <th style="width: 50px; background-color: #259d6d"> &nbsp; &nbsp;</th>
                                <th style="width: 50px; background-color: #259d6d">2.30pm - 5.00pm</th>

                                @if(isset($schedule_data))
                                    @foreach($schedule_data as $key=> $value)
                                        <tr>
                                            @if($value['Day']=='Monday')
                                            <td> {!! $value['Day'] !!} </td>
                                            <td> {!! $value['Time'] !!} </td>
                                            <td> {!! $value['Course'] !!} </td>
                                            <td> {!! $value['Level'] !!} </td>
                                            <td> {!! $value['Term'] !!} </td>
                                            <td> {!! $value['Section'] !!} </td>
                                            @endif
                                        </tr>
                                        {{--{!! $value->rank !!}<br>--}}
                                    @endforeach
                                    @endif
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6" style="height: 50px; background-color: #2ca02c; margin-left: 310px; width: 750px;">
       <center><a href="Research/create"><button type="button" class="btn btn-primary" style="margin-top: 5px;">Add new paper</button></a></center>


    </div>
@endsection