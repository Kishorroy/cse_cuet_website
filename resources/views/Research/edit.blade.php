
@extends('../master1')

@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Edit Form(Paper Information)</h3>
            <hr>

            {!! Form::open(['url'=>'/Research/update']) !!}

            {!! Form::label('author_name','Author Name:') !!}
            {!! Form::text('author_name',$oneData['author_name'],['class'=>'form-control', 'required'=>'required']) !!}

            <br>
            {!! Form::label('paper_name','Paper Name:') !!}
            {!! Form::text('paper_name',$oneData['paper_name'],['class'=>'form-control', 'required'=>'required']) !!}
            <br>
            {!! Form::label('type',' Type:') !!}
            {{ Form::select('type', [
            'Please Select One',
               'Journal' => 'Journal',
               'Conference' => 'Conference']
            ) }}
            <br>
            {!! Form::label('journal_name','Journal/Conference Name:') !!}
            {!! Form::text('journal_name',$oneData['journal_name'],['class'=>'form-control', 'required'=>'required']) !!}
            <br>
            {!! Form::label('publication_year','Publication Year:') !!}
            {!! Form::text('publication_year',$oneData['publication_year'],['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::text('id',$oneData['id'],['hidden'=>'hidden' ]) !!}


            {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}
            <br><br>

            {!! Form::close() !!}

        </div>
    </div>

@endsection