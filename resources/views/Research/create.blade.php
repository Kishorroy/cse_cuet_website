@extends('../master1')

@section('content')

    <div class="row" style="margin-top: 50px; background-color: #c5b143; width: 1000px;">



        <div class="col-md-6 col-md-offset-3" style="width: 700px; margin-left: 250px;">

            <h3>Add New Paper</h3>
            <hr>

            {!! Form::open(['url'=>'/Research/store']) !!}

            {!! Form::label('author_name','Author Name:') !!}
            {!! Form::text('author_name','',['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::label('paper_name','Paper Name:') !!}
            {!! Form::text('paper_name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('type',' Type:') !!}
            {{ Form::select('type', [
            'Please Select One',
               'Journal' => 'Journal',
               'Conference' => 'Conference']
            ) }}
            <br>
            {!! Form::label('journal_name','Conference/Journal Name:') !!}
            {!! Form::text('journal_name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>
            {!! Form::label('publication_year','Publication Year:') !!}
            {!! Form::text('publication_year','',['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::label('t_id','Teacher ID:') !!}
            {!! Form::text('t_id','',['class'=>'form-control', 'required'=>'required']) !!}
            <br>
            <center>
                {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}
            </center>

            <br><br>


            {!! Form::close() !!}

        </div>

    </div>

@endsection