<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>
    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:710px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 430px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="notice.blade.php">Notice Board</a></li>
                    <li><a href="upcomingevent.blade.php">Upcoming Events</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li class="active"><a href="contact.blade.php">Contact Info</a></li>
                </ul>


            </div>
        </nav>

    </div>
    <div class="map">
        <div class="col-lg-4" style="height: 400px; background: linear-gradient(#90caf9 ,#bbdefb , #e3f2fd );  margin-top: 30px;">
        <h3 style="color: white;">Contact Info</h3><hr>
            <p style="font-size: 18px;">
               <b>Registar</b><br>
                Chittagong University of Engineering and Technology (CUET)<br>
                Chittagong - 4349<br>
                Bangladesh<br>
                Tel: +880-31-714946, +880-31-714911<br>
                Fax: +88-0302556151, +880-31-714910<br>
                E-mail: registar@cuet.ac.bd, registar_cuet@yahoo.com, po_to_registar@cuet.ac.bd<br>
            </p>
        </div>
        <div class="col-lg-4" style="height: 400px; background: linear-gradient(#90caf9 ,#bbdefb , #e3f2fd );  margin-top: 30px;">
            <h3 style="color: white;">Contact Info</h3><hr>
            <p style="font-size: 18px;">
                <b>Departmental Head</b><br>
                Department of Computer Science & Engineering<br>
                Chittagong University of Engineering and Technology (CUET)<br>
                Chittagong - 4349<br>
                Bangladesh<br>
                Telephone: +880-031-723336 Fax:+880-031-714910<br>
                E-mail: headcse@cuet.ac.bd
            </p>
        </div>
        <div class="col-lg-4" style="height: 400px; background: linear-gradient(#90caf9 ,#bbdefb , #e3f2fd ); margin-top: 29px;">
            <h3 style="color: white;">Google Map</h3><hr>
            <p>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d29497.407199548572!2d91.9578681052827!3d22.460023299999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30ad2fca34ae5549%3A0x35c88a37b3e90e97!2sChittagong+University+of+Engineering+%26+Technology!5e0!3m2!1sen!2sbd!4v1504246544516" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
            </p>
        </div>

        </div>
    <div class="footer" style="height:70px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>