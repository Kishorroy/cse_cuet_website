<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>
    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:710px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 780px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .center {
            text-align: center;
        }

        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #000;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {background-color: #ddd;}
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li class="active"><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>
    <div class="map">
        <div class="center" >
            <br>
            <table id="customers">
                <tr>
                    <th>Publication Type</th>
                    <th><center>Publication Details</center></th>
                    <th>Year</th>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Mahfuzul Haque Chowdhury, Asaduzzaman, M.F.Kader and M.O. Rahman, "Design of an Efficient MAC Protocol for Opportunistic Cognitive Radio Networks", International Journal of Computer Science & Information Technology (IJCSIT) , , 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Enamul Hoque, O. Hoeber, M. Gong, "CIDER: Concept-based Image Diversification, Exploration, and Retrieval", Journal of Information Processing & Management , , 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Swati Nigam, Kaushik Deb, and Ashish Khare, "Moment Invariants Based Object Recognition for Different Pose and Appearances in Real Scenes", In Proceedings of the 2nd International Conference on Informatics, Electronics & Vision (ICIEV) , Bangladesh, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Md. Moshiul Hoque, Kaushik Deb, Dipankar Das, Y. Kobayashi, and Y. Kuno , "An Intelligent Human-Robot Interaction Framework to Control the Human Attention", In Proceedings of the 2nd International Conference on Informatics, Electronics & Vision (ICIEV) , Bangladesh, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Prianka Banik and Kaushik Deb, , "Detecting Books from Bookshelf of Multiple Cells", In Proceedings of the International Conference on Engineering Research, Innovation & Education (ICERIE) pp. 417-421, Bangladesh, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>D. Das, Md. Moshiul Hoque, Y. Kobayashi, Y. Kuno, "Attention control system considering the target person's attention level ", ACM/IEEE International Conference on Human Robot Interaction (HRI) pp. 111-112, Japan, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Zhichao Chang, Mohammad Shamsul Arefin, Yasuhiko Morimoto, , "Hotel Recommendation Based On Surrounding Environments", ACIT 2013 , Japan, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Mohammad Shamsul Arefin and Yasuhiko Morimoto, "Skyline Queries for Sets of Spatial Objects by Utilizing Surrounding Environments", LNCS 7813, Springer pp. 293-309, , 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Mohammad Shamsul Arefin, Mohammad Amir Sharif, Yasuhiko Morimoto, , "BEBS: A Framework for Bilingual Summary Generation", 2nd International Conference on Informatics, Electronics & Vision (IEEE Conf.) , Bangladesh, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Pranab Kumar Dhar and Tetsuya Shimamura, "Entropy-Based Audio Watermarking Using Singular Value Decomposition and Log-Polar Transformation", In Proc. of the IEEE 56th International Midwest Symposium on Circuits and Systems (MWSCAS 2013) , Columbus, Ohio, USA, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Pranab Kumar Dhar and Tetsuya Shimamura, "An SVD-Based Audio Watermarking Using Variable Embedding Strength and Exponential-Log Operations", In Proc. of the 2nd International Conference on Informatics, Electronics, and Vision(ICIEV 2013) , Bangladesh, 2013</td>
                    <td>2013</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Md. Iqbal H. Sarker and M.I.Khan, "An Improved Blind Watermarking Method in Frequency Domain for Image Authentication", 2nd International Conference on Informatics, Electronics & Vision (ICIEV), IEEE , , 2013</td>
                    <td>2013</td>
                </tr>
            </table>
            <div class="pagination">
                <a href="research1.blade.php">&laquo;</a>
                <a href="research1.blade.php">1</a>
                <a href="research2.blade.php" class="active" >2</a>
                <a href="research3.blade.php">3</a>
                <a href="research4.blade.php">4</a>
                <a href="research5.blade.php">5</a>
                <a href="research6.blade.php">6</a>
                <a href="research7.blade.php">7</a>
                <a href="research8.blade.php">8</a>
                <a href="research9.blade.php">9</a>
                <a href="research10.blade.php">10</a>
                <a href="research3.blade.php">&raquo;</a>
            </div>
        </div>
    </div>
    <div class="footer" style="height:70px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>