<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dr. Mohammed Moshiul Hoque</title>
    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height: 1600px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .nopadding{
            padding: 0;
            margin-top: 30px;
            background: linear-gradient(white ,white , #afd9ee);
        }
        .nopadding h4{
            background: linear-gradient( #afd9ee,white , #afd9ee);
            padding: 10px 15px;
        }

        .nopadding ul li{

            background: url("/public/images/arrow.jpg") no-repeat left -1.6rem;
            padding-left: 10px;
            background-size: 25px 35px;
            margin: 15px 17px;
        }





    </style>
</head>
<body>

<div class="container">

    <div class="header">
        <<center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li class="active"><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>
    </div>

    <div class="col-lg-10" style="margin-left: 100px ">
        <div class="col-md-4">
            <img src="{{URL::asset('/images/teachers/moshiul.jpg')}}" style="width: 300px;height: 300px;margin-top: 20px">
        </div>
        <div class="col-md-8">
            <h1 style="margin-bottom: 0">Dr. Mohammed Moshiul Hoque</h1>
            <br>
            <h2 style="margin: 0;padding: 0;">ডঃ মোহাম্মদ মশিউল হক</h2><hr>
            <h4>Professor</h4>
            <b>E-mail: </b>		moshiulh@yahoo.com; moshiul_240@cuet.ac.bd; mmoshiulh@gmail.com
            <br><b>Contact: </b>	8801716-133019<hr>
            <b>Research Interests:</b>
            Human-Robot/Computer Interaction,  	Robotic Vision,   Natural Language Processing, 	Ubiqutous Computing
            <hr>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Education :</b></h4>
            <ul>
                <li><b>B.Sc.</b><br>Electrical & Electronics Enineering<br>Chittagong University Of Engineering & Technology<b>(CUET)</b><br>Bangladesh</li>
                <li><b>M.Sc.</b><br>Computer Science<br>Bangladesh University Of Engineering & Technology<b>(BUET)</b><br>Bangladesh</li>
                <li><b>PhD</b><br>Japan</li>
            </ul>
        </div>


        <div class="col-sm-12 nopadding">
            <h4><b>Related Courses(Undergraduate):</b></h4>
            <div class="col-md-6">
                <ul>
                    <li>Human Computer Interaction</li>
                    <li>Social Robotics</li>
                    <li> Theory of Computing	</li>
                    <li>Electrical Drives & Instrumentation</li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul>
                    <li>Natural Language Processing</li>
                    <li>Artificial Intelligence</li>
                    <li> Compiler Design</li>
                    <li> Microprocessor & Assembly Language Programming</li>
                </ul>
            </div>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Scholarships & Awards :</b></h4>
            <ul>
                <li> Monbukagakusho Scholarship, Ministry of Education, Culture, Sports, Science and Technology, JAPAN, for Doctoral Program at Saitama University, Japan, Oct. 2009 ~ Sept. 2012.</li>
                <li> Best Paper Presentation Award, International Conference on Human System Integration (HSI, 2011), Japan.</li>
                <li>Korean Government Scholarship for IT International Students for PhD Research, Kyung Hee University, South Korea, Aug. 2008~Sept. 2009.</li>
                <li>President Scholarship for Supporting Tuition Fees in Kyung Hee University (Aug. 2008~Sept. 2009</li>
                <li>Higher Study Scholarship, Chittagong University of Engineering & Technology, Apr. 2002~Apr. 2004.</li>
                <li>Technical Scholarship, Chittagong University of Engineering & Technology, June 1996~Dec. 2000.</li>

            </ul>

            <div class="col-sm-12 nopadding">
                <h4><b>Workshops/ Seminar Talks:</b></h4>
                <ul>
                    <li> An Integrated Approach to Controlling Human Attention through Non-verbals Behaviors of Robots, Presented Seminar on ICT, Organized by Department of CSE, CUET, November 2012.</li>
                    <li>An Empirical Framework to Control Human Attention by Robot, CREST Symposium, November 27, 2010, NII, Tokyo, Japan</li>
                    <li>Attracting and Controlling of Human Attention through Robot’s Behaviors Suited to the Situations, CREST Symposium, October 19, 2011, NII, Tokyo, Japan</li>
                    <li>An Empirical Framework for Parsing of Bangla Sentences” Presented Seminar on ICT, Organized by Department of CSE and CSE Association, CUET, Sept. 2006.</li>
                    <li> Cryptography and Bangla: An Experience with RSA Algorithm”, Presented Seminar on ICT, Organized by Department of CSE, CUET, May 2007.</li>

                </ul>
            </div>

            <div class="col-sm-12 nopadding">
                <h4><b>Professional Membership:</b></h4>
                <ul>
                    <li>Member IEEE (Institute of Electrical & Electronic Engineers)</li>
                    <li>Student Member of RSJ (Robotics Society of Japan)</li>
                    <li>Fellow IEB (Institute of Engineers, Bangladesh)</li>
                </ul>
            </div>



            <div class="col-sm-12 nopadding" style="background: linear-gradient( white ,#afd9ee, #afd9ee);">
            <h4><b>Publications :</b></h4>
            <h5 align="center"><b>Journals </b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>M. M. Hoque, Q. D. Hossian, K. Deb, D. Das, Y. Kobayashi, and Y. Kuno, An Empirical Robotic Framework for Initiating Interaction with the Target Human in Multiparty Settings, Journal of Circuits, Systems and Computers, Vol. 24, No. 02, 1540003 (2015) [25 pages] DOI: 10.1142/S0218126615400034.</li>
                <li>M. A.F.M. R. Hasan, R. Yasmin, D. Das, M. M. Hoque, M. I. Pramanik, and M. S. Rahman, Fundamental Frequency Extraction of Noisy Speech Signals, Rajshahi University Journal of Science & Engineering, Vol. 43, pp. 51-61, 2015.</li>
                <li>M. M. Hoque, Q. D. Hossain, and K. Deb, “Effects of Robotic Blinking Behavior for Making Eye Contact with Humans”, Smart Innovation, Systems and Technologies, Vol. 27, pp. 621-628, Springer International Publishing, Switzerland, January 2014.</li>
                <li>M. M. Hoque, Y. Kobayashi, and Y. Kuno, A Proactive Approach of Robotic Framework for Making Eye Contact with Humans, Advances in Human-Computer Interaction, vol. 2014, Article ID 694046, 19 pages, 2014. doi:10.1155/2014/694046.</li>
                <li>M. F. Kader, Assaduzzaman, M. M. Hoque, Outage Capacity Analysis of a Cooperative Relaying Scheme in Interference Limited Cognitive Radio Networks, Wireless Personal Communications, Springer, 2014, doi 10.1007/s11277-014-1976-8.</li>
                <li>M. M. Hoque, T. Onuki, Y. Kobayashi, and Y. Kuno, Effect of Robot’s Gaze Behaviors for Attracting and Controlling Human Attention, Journal of Advanced Robotics, vo. 27, no. 11, pp. 1-17, Taylor & Francis, Japan, July 2013</li>
                <li>Md Fazlul Kader, Asaduzzaman, Md. Moshiul Hoque, Hybrid Spectrum Sharing with Cooperative Secondary User Selection in Cognitive Radio Networks, KSII Transactions on Internet and Information Systems, Vol. 07, N0. 09, pp. 2081-2100, Sept. 2013.</li>
                <li>M. M. Hoque, A Robotic Framework for Controlling Human’s Attention, Paladyn Journal of Behavioral Robotics (JBR), vol. 03, no. 02, pp. 92-101, Springer, UK, 2012.</li>
                <li>M. M. Hoque, D. Das, T. Onuki, Y. Kobayashi, and Y. Kuno, Robotic System Controlling Target Human’s Attention, Intelligent Computing Theories and Applications, LNCS, Vol. 7390, pp. 534-544, Springer, Heidelberg, Germany, 2012./li>
                <li>M. M. Hoque, Y. Kobayashi, Y. Kuno, and K. Deb, Design a Robotic Head to Attract Human Attention by considering viewing situations, Computer Science and Engineering Research Journal (CSERJ), vol. 08, pp. 29-38, 2012, Bangladesh.</li>
                <li>M. M. Hoque, T. Onuki, E. Tsuburaya, Y. Kobayashi, Y. Kuno, T. Sato, and S. Kodama, An Empirical Framework to Control Human Attention by Robot. In ACCV 2010 Workshops, Part I, vol. 6468, pp. 430-4392. LNCS, Springer, Heidelberg, Germany, 2011.</li>
                <li>H. M. Moshiul, A. M. Mahbub, “An Efficient Framework for Network Intrusion Detection”, Georgian Electronic Scientific Journal: Computer Science and Telecommunications, Vol. 24, No. 1, pp. 32-42, January, 2010.</li>
                <li>M. M. Hoque and M. K. Hassan, “English to Bangla Statistical Machine Translation using A* Search Algorithm”, Georgian Electronic Scientific Journal: Computer Science and Telecommunications, Vol. 18, No. 1, pp. 65-78, January, 2009.</li>
                <li>M. M. Hoque and M. K. Hassan, “An Empirical Framework for Parsing of Bangla Natural Language Sentences”, International Journal of Translation (IJT), Vol. 20, No. 1-2, pp. July-August, 2008.</li>
                <li>M. M. Hoque, and M. K. Hassan, “An Empirical Framework for Translating of Phrasal Verbs of English Sentence into Bangla” Internal Journal of Computer Sciences, Systems and Information Technology (IJCSEIT), Vol.1, No. 1, pp. 1-6, June 2008.</li>
                <li>A. N. M. R. Karim, M. G. R. Alam and M. M. Hoque, “A New Model of Digital Signature Using LSB Hiding, RSA and Merkle-Hellman Cryptosystem” Computer Science and Engineering Research Journal, Vol. 4, PP. 39-44, 2007.</li>
                <li>M. M. Hoque, M. O. Faruk, M. M. U. Hasan, M. K. Hassan and M. M. U. Karim, “An Empirical Framework for Statistical Parsing of Bangla Sentences”, Computer Science and Engineering Research Journal, Vol. 4, PP. 29-38, 2007.</li>
                <li>M. S. Arefin, R. Paul, M. M. Hoque and M. A. Sarif, “Developing an Intelligent agent to support negotiation in an electronic market place”, Computer Science and Engineering Research Journal (CSERJ), Vol. 3, PP. 30-37, ISSN: 1990-4010, 2006.</li>
                <li>M.M. Hoque, M. S. Arefin and M. O. Rahman, “Semantic Analysis and Transfer: An Experience with the Bangla Natural Language Sentences”, Computer Science and Engineering Research Journal (CSERJ), Vol. 3, PP. 17-23, ISSN: 1990-4010, 2006.</li>
                <li>M. M. Hoque, M. M. Islam and M. M. Ali, “An efficient fuzzy method for Bangla Handwritten numerals recognition” In Proc. International Conference on Electrical and Computer Engineering ICECE’ 06, PP. 197-200, 2006.</li>
                <li>M. M. Hoque, M. S. Arefin and M. S. Kowser, “Context-free Grammars and Parsing for Simple Sentences of Bangla” Computer Science and Engineering Research Journal (CSERJ), Vol. 2, PP. 53-60, ISSN: 1990-4010, 2005.</li>
                <li>M. S. Arefin, M. M. Hoque, M. T. Islam and K. Dev, "Structural Operational Semantics of Inheritance and Object creation in JAVA", Computer Science and Engineering Research Journal (CSERJ), Vol. 2, PP. 29-35, ISSN: 1990-4010, 2005.</li>

            </ul>
            <hr>
            <h5 align="center"><b>Conferences</b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>S. Bhowmick, and M. M. Hoque, Design an Eye-Based Human Computer Interface for Controlling Computer Applications and Electrical Appliances, In Proc. 9th International Forum on Strategic Technology (IFOST), pp. 589-594, Bali, Indonesia, 2015.</li>
                <li>P. Shome, and M. M. Hoque, Speech Based Activity Reminder System for Older People, In Proc. 9th International Forum on Strategic Technology (IFOST), pp. 583-587, Bali, Indonesia, 2015.</li>
                <li>M. A. F. M. R. Hasan , R. Yasmin, D. Das , M. M. Hoque, and M. S. Rahman, Pitch Detection in Speech Signal under Noisy Conditions, In Proc. International Conference on Materials, Electronics & Information Engineering (ICMEIE), Rajshahi, Bangladesh, 2015.</li>
                <li>L. Alam, and M. M. Hoque, Developing an Expressive Intelligent Agent, In Proc. International Conference on Materials, Electronics & Information Engineering (ICMEIE), Rajshahi, Bangladesh, 2015.</li>
                <li>L. Alam, and M. M. Hoque, “The Design of Expressive Intelligent Agent For Human-Computer Interaction”, In Proc. International Conference on Electrical Engineering and Information Communication Technology (ICEEICT), pp. 1-6, Dhaka, Bangladesh, 2015</li>
                <li>M. S. Arefin, M. M. Hoque, M. O. Rahman, and M. S. Arefin, “A Machine Translation Framework for Translating Bangla Assertive, Interrogative And Imperative Sentences Into English, In Proc. International Conference on Electrical Engineering and Information Communication Technology (ICEEICT), pp. 1-6, Dhaka, Bangladesh, 2015.</li>
                <li>M. H. Ullah, R. H. Samit, and M. M. Hoque, “Design a Robotic Head to Interact with Humans using Non-verbal Gaze Following”, In Proc. International Conference on Mechanical Engineering and Renewable Energy, pp.67, Chittagong, Bangladesh, 2015</li>
                <li>M. Manuruzzaman, L. Alam, M. M. Hoque, and M. S. Arefin, “Design an Expressive Intelligent Agent to Interacting With Humans”, In Proc. International Conference on Physics for Sustainable Development and Technology (ICPSDT), Chittagong, Bangladesh, 2015.</li>
                <li>M. S. Arefin, L. Alam. S. Sharmin, and M. M. Hoque, “An Empirical Framework for Parsing Bangla Assertive, Interrogative and Imperative Sentences”, In Proc. International Conference on Computer and Information Engineering (ICCIE), pp. 122-125, Rajshahi, Bangladesh, 2015.</li>
                <li>S. Haque, M. S. Arefin, M. M. Hoque, “BQAS: A Bilingual Question Answering System”, In Proc. International Conference on Electrical Information and Communication Technology (EICT), pp. 586-591, Khulna, Bangladesh, 2015.</li>
                <li>M. Rahman, M. Debnath, S. Sharmin, L. Alam, S. Arefin, M. Hoque, “Designing an Empirical Framework to Measure the Level of Interest of Human, In Proc. International Conference On Electrical Information And Communication Technology (EICT), pp. 581-585, Khulna, Bangladesh, 2015</li>
                <li>Y. A. Khan, M. Ahmed, and M. M. Hoque, “Isolated Bangla Word Recognition and Speaker Detection by Semantic Modular Time Delay Neural Network (MTDNN)”, In Proc. International Conference on Computer and Information Technology (ICCIT), pp. , Dhaka, Bangladesh, 2015.</li>
                <li>M. M. Hoque, Y. Kobayashi, and Y. Kuno, “A Robotic Framework for Shifting the Target Human’s Attention in Multi-Party Setting”, In Proc. International Conference on Informatics, Electronics, and Vision (ICIEV), Dhaka, Bangladesh, May 2014.</li>
                <li>M. M. Hoque, D. Das, Y. Kobayashi, Y. Kuno, K Deb, Proactive Approach of Making Eye Contact with the Target Human in Multi-Party Settings, International Conference on Computer and Information Technology (ICCIT’ 2013), pp. 191-195, Khulna, Bangladesh, March 2014.</li>
                <li>M. A. Rahman, Q. D. Hossain, M. A. Hossain, M. M. Hoque, E. Nishiyama, I. Toyoda, “Design of a Dual Circular Polarization Microstrip Patch Array Antenna”, International Forum On Strategic Technology (IFOST), pp. 187-190, Bangladesh, 2014.</li>
                <li>P. P. Purohit, M. M. Hoque, An Empirical Framework for Semantic Analysis of Bangla Sentences, International Forum on Strategic Technology (IFOST), pp. 34-39, Bangladesh, 2014.</li>
                <li>Y. A. Rahmann, M. M. Hoque, Helping-Hand: A Data Glove Technology for Rehabilitation of Monoplegia Patients, International Forum on Strategic Technology (IFOST), pp. 199-204, Bangladesh, 2014.</li>
                <li>D. Das, M. M. Hoque, J. Ferdousi, M. A. F. M R. Hasan, Automatic Face Parts Extraction and Facial Expression Recognition, International Forum on Strategic Technology (IFOST), pp. 128-131, Bangladesh, 2014.</li>
                <li>P. P. Purohit, M. M. Hoque, M. K. Hasaan, “Features based Semantic Analyzer for Parsing Bangla Complex and Compound Sentences” 8th International Conference on Software, Knowledge, Information Management and Applications (SKIMA 2014), pp. 1-7, Bangladesh, 2014.</li>
                <li>S. Rahman, M. S. Arefin, and M. M. Hoque, Developing a Framework for Translation Among Different Local Languages of Bangla, In Proc. 9th International Forum on Strategic Technology (IFOST), pp. 30-33, Cox’sBazar, Bangladesh 2014.</li>
                <li>M. M. Hoque, K. Deb, D. Das, Y. Kobayashi, Y. Kuno, “An Intelligent Human-Robot Interaction Framework to Control the Human Attention”, International Conference on Informatics, Electronics, and Vision (ICIEV), May 2013, Bangladesh.</li>
                <li>D. Das, M. M. Hoque, Y. Kobayashi, Y. Kuno, “Attention control system considering the target person's attention level” 111-112”, ACM/IEEE International Conference on Human Robot Interaction (HRI), pp. 111-112, 2013, Japan.</li>
                <li>A. H. Sunny, K. Deb, P. Biswas, M. M. Hoque, Shadow Detection and Removal Based on YCbCr Color Space, National Conference on Intelligent Computing & Information Technology, pp. 54-59, Bangladesh.</li>
                <li>M. M. Hoque, Q. D. Hossian, D. Das, Y. Kobayashi, Y. Kuno, K. Deb, An Empirical Robotic Framework for Interacting with Multiple Humans, International Conference on Electrical Information and Communication Technology (EICT 2013), pp. 422-426, Bangladesh [Received Best Paper Award].</li>
                <li>M. M. Hoque, D. Das, T. Onuki, Y. Kobayashi, Y. Kuno, “Model for Controlling a Target Human's Attention in Multi-Party Settings”, The 21st IEEE International Symposium on Robot and Human Interactive Communication (RO-MAN), pp. 476-483, 2012, France.</li>
                <li>D. Das, M. M. Hoque, T. Onuki, Y. Kobayashi, Y. Kuno, “Vision-based Attention Control System for Socially Interactive Robots”, The 21st IEEE International Symposium on Robot and Human Interactive Communication (RO-MAN), pp. 96-502, 2012, France.</li>
                <li>M. M. Hoque, D. Das, T. Onuki, Y. Kobayashi, Y. Kuno, “An Integrated Approach of Attention Control of Target Human by Nonverbal Behaviors of Robots in Different Viewing Situations”, IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS), pp. 1399-1406, 2012, Portugal</li>
                <li>M. M. Hoque, T. Onuki, Y. Kobayashi, and Y. Kuno, “Attracting and Controlling Human Attention through Robot’s Behaviors Suited to the Situation”, International Conference on Human Robot Interaction (HRI’12), pp. 149-150, 2012, USA.</li>
                <li>M. M. Hoque, K. Deb, “Robotic System for Making Eye Contact Pro-actively with Humans”, 7th International Conference on Electrical and Computer Engineering (ICECE), pp. 125-128, 2012, Bangladesh.</li>
                <li>M. M. Hoque, K. Deb, D. Das, Y. Kobayashi, Y. Kuno, “Design An Intelligent Robotic Head to Interacting with Humans”, 15th International Conference on Computer and Information Technology (ICCIT), pp. 539-546, 2012, Bangladesh.</li>
                <li>M. M. Hoque, D. Das, Y. Kobayashi, Y. Kuno, K. Deb, “Design an Intelligent Robotic Head for Human Robot Interaction”, International Conference on Electrical, Computer and Telecommunication Engineering (ICECTE), pp. 565-568, 2012, Bangladesh.</li>
                <li>K.Deb, M.S. Al-Seraj, M.M. Hoque, M.I.H. Sarker, “Combined DWT-DCT Based Digital Image Watermarking Technique for Copyright Protection”, International Conference on Electrical & Computer Engineering (ICECE), IEEE, Dhaka, Bangladesh, 2012.</li>
                <li>M. M. Hoque, T. Onuki, Y. Kobayashi, Y. Kuno, “Controlling Human Attention by Robot's Behavior Depending on his/her Viewing Situations”, International Conference on Social Robotics (ICSR), pp. 37-40, 2011, Netherlands.</li>
                <li>M. M. Hoque, T. Onuki, Y. Kobayashi, and Y. Kuno, “Controlling Human Attention through Robot’s Gaze Behaviors”, In Proc. International Conference on Human System Interaction (HSI'11), pp. 195–202, 2011 (Best Presentation Award).</li>
                <li>M. M. Hoque, M. R. karim, M. G. Hossian. M. S. Arefin and M. M. U. Hasan, “Bangla Numeral Recognition Engine (BNRE)”, International Conference on Electrical and Computer Engineering ICECE’ 08, PP. 644-647, 2008.</li>
                <li>M. M. Hoque, M. M. Ahmed, M. S. Arefin, M. M. U. Hasan and M. G. Hossain, “An Empirical Framework for Translating of Phrasal Verbs of English Sentence into Bangla”, International Conference on Information and Communication Technology (ICCIT’2008), PP. 183-188, 2008.</li>
                <li>M. M. Hoque, M. A. Muhit, M. A. Ullah. And M. K. Karim, “Bangla Vowel Sign Recognition by Extracting the Fuzzy Features”, International Conference on Information and Communication Technology (ICCIT’2008), PP. 306-311, 2008.</li>
                <li>A. A. Farook, M. S. Arefin and M. M. Hoque, “Structural Operational Semantics of Packages in Java” International Conference on Information and Communication Technology (ICCIT’2008), PP. 40-45, 2008.</li>
                <li>M. M. Hoque, M. A. Muhit, M. A. Ullah. And M. K. Karim, “Network Intrusion Detection System Using Fuzzy Logic”, International Conference on Electronics, Computer and Communication (ICECC), PP. 63, 2008.</li>
                <li>S. Khastagir, M. A. Ullah, M. A. S. Shikder and M. M. Hoque, “Cognitive Radio in GSM”, International Conference on Electronics, Computer and Communication (ICECC), PP. 17, 2008.</li>
                <li>M. M. Hoque, M. J. Rahman and P. K. Dhar, “Lexical Semantics: A New Approach to Analyze the Bangla Sentence with Semantic Features”, International Conference on Information and Communication Technology ICICT’07, PP. 87-91, March 2007.</li>
                <li>M. A. A. Dewan, M. J. Hossian, M. M. Hoque and O. Chae, "Contaminated ECG Artifact Detection and Elimination from EEG using Energy Function Based Transformation", International Conference on Information and Communication Technology ICICT’07, PP.52-56, March 2007.</li>
                <li>M. M. Hoque and M. M. Hasan, “Encryption and Decryption: An Empirical Framework for Bangla With RSA Algorithm”, National Conference on Electronics, Information and Telecommunication, PP. 195, Rajshahi, Bangladesh, 2007.</li>
                <li>M. M. Hoque and S. M. F. Rahman, “Fuzzy features extraction from Bangla handwritten numerals”, International Conference on Information and Communication Technology, ICICT’ 07, PP. 72-75, March 2007.</li>
                <li>M. M. Hoque, M. K. Hossian, M. M. S. Kowsar, K. Deb and M. S. Arefin, “An Empirical Framework for Network Intrusion Detection using Fuzzy Logic”, In Proc. International Conference on Computer and Information Technology, ICCIT’06, PP. 392-397, 2006.</li>
                <li>M. M. Hoque and M. O. Karim, “Extraction of meaningful fuzzy features for recognizing of Bangla handwritten numerals” In Proc. International Conference on Computer and Information Technology, ICCIT’06, PP. 61-66, 2006.</li>
                <li>M. M. Hoque, M. M. Haque and P. K. Dhar, "Semantic Analysis of Bangla Sentences: A Compositional Semantic Approach", In Proc. International Conference on Computer Processing of Bangla, ICCPB’06, (Dhaka, Bangladesh), PP. 106-112, 2006.</li>
                <li>M. O. Karim and M. M. Hoque, "Design of a CART Classifier for Bangla Handwritten Character Recognition", In Proc. International Conference on Computer and Information Technology, ICCIT’05, (Gazipur, Bangladesh), PP. 993-997, 2005.</li>
                <li>M. M. Hoque and M. M. Ali, "Semantic Features and Redundancy Rules for Analyzing of Bangla Sentences", In Proc. International Conference on Computer and Information Technology, ICCIT’05, (Gazipur, Bangladesh), PP. 1198-1201, 2005.</li>
                <li>S. M. F. Rahman, M. M. Islam and M. M. Hoque, "Extraction of Meaningful Fuzzy Features for Bangla Online Handwriting", In Proc. International Conference on Computer and Information Technology, ICCIT’05, (Gazipur, Bangladesh), PP. 661-666, 2005.</li>
                <li>T. C. Das and M. M. Hoque, "An Evolutionary Power Optimization Technique Using Fuzzy Logic Controller", In Proc. International Conference on Computer and Information Technology, ICCIT’05, (Gazipur, Bangladesh), PP. 919-923, 2005.</li>
                <li>M. M. Islam, S. M. F. Rahman and M. M. Hoque, "Online Bengali Handwriting Recognition with Automatically Generated Fuzzy Linguistic Rules", In Proc. International Conference on Computer and Information Technology, ICCIT’05, (Gazipur, Bangladesh), PP. 673-678, 2005.</li>
                <li>M. M. Hoque and M. M. Ali, “Context-sensitive Phrase Structure rules for Structural Representation of Bangla Natural Language Sentences ”, In Proc. International Conference on Computer and Information Technology, ICCIT’04, (Dhaka, Bangladesh), PP. 615-620, 2004.</li>
                <li>M. M. Hoque and M. M. Ali, “A Parsing Methodology for Bangla Natural Language Sentences”, In Proc. International Conference on Computer and Information Technology, ICCIT’03, (Dhaka, Bangladesh), PP. 277-282, 2003.</li>

            </ul>

        </div>




    </div>
    </div>

        <div class="footer" style="margin-top: 6250px; height:90px; background-color: #afd9ee; ">
            <br>
            <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

        </div>

</div>



</body>
</html>