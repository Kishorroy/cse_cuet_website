<!DOCTYPE html>
<html>
<head>

    <title>CUET CSE</title>
    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:710px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }

    </style>

    <script>
        $(document).ready(function(){

                $("#div1").fadeIn();
                $("#div2").fadeIn("slow");
                $("#div3").fadeIn(3000);

        });
    </script>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li class="active"><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="notice.blade.php">Notice Board</a></li>
                    <li><a href="upcomingevent.blade.php">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

            </div>
        </nav>

    </div>
    <div class="info" style="height: 460px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">

<center>

    <br><br>
    <div id="div1" style="width:800px;height:100px;display:none;background: linear-gradient(#ef9a9a ,#e57373 , #ef5350);"><a href="undergraduate.blade.php" style="color: white; font-size: 25px; margin-top: 10px;"><b><br>Undergraduate Admission</b></a> </div><br><br>
    <div id="div2" style="width:800px;height:100px;display:none;background: linear-gradient(#a5d6a7 ,#81c784 , #66bb6a);"><a href="master.blade.php" style="color: white;font-size: 25px;margin-top: 10px;"><b><br>M.sc Admission program</b></a></div><br><br>
    <div id="div3" style="width:800px;height:100px;display:none;background: linear-gradient(#4fc3f7 ,#29b6f6 , #039be5);"><a href="phd.blade.php" style="color: white;font-size: 25px;margin-top: 10px;"><b><br>P.hd Admission Program</b></a></div>
</center>
    </div>
    <div class="footer" style="height:60px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>
