<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:710px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 950px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .center {
            text-align: center;
        }

        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #000;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {background-color: #ddd;}
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li class="active"><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>
    <div class="map">
        <div class="center" >
            <br>
            <table id="customers">
                <tr>
                    <th>Publication Type</th>
                    <th><center>Publication Details</center></th>
                    <th>Year</th>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Chen Hongbo, Chen Zhiming, Mohammad Shamsul Arefin and Yasuhiko Morimoto, "Place Recommendation from Check-in Spots on Location-Based Online Social Networks", Third International Conference on Networking and Computing (IEEE, DBLP) pp. 143-148, Japan, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Chen Zhiming, Mohammad Shamsul Arefin and Yasuhiko Morimoto, "Skyline Queries for Spatial Objects: A Method for Selecting Spatial Objects Based on Surrounding Environments", Third International Conference on Networking and Computing (IEEE, DBLP) pp. 215-220, Japan, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Xu Jinhao, Mohammad Shamsul Arefin, Chen Zhiming and Yasuhiko Morimoto, "Real Estate Recommender: Location Query for Selecting Spatial Objects", Third International Conference on Networking and Computing (IEEE, DBLP) pp. 278-282, Japan, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Mohammad Shamsul Arefin and Yasuhiko Morimoto, "Skyline Sets Queries from Databases with Missing Values", 22nd International Conference on Computer Theory and Applications (IEEE) pp. 24-29, Egypt, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Mohammad Shamsul Arefin, Yasuhiko Morimoto, Chen Zhiming, "Developing a Framework for Generating Bilingual English-Bangla Dictionaries from Parallel Text Corpus", International Conference on Electrical, Computer and Telecommunication Engineering pp. 549-552, Bangladesh, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Pranab Kumar Dhar and Tetsuya Shimamura, "Audio Watermarking Method in Transform Domain Based on Singular Value Decomposition and Quantization", in Proc. of the 18thIEEEAsia Pacific Conference on Communication (APCC 2012) pp. 516-521, Jeju Island, Republic of Korea, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Pranab Kumar Dhar and Tetsuya Shimamura, "An Audio Watermarking Scheme Using Discrete Fourier Transformation and Singular Value Decomposition and Quantization", in Proc. of the35th IEEE International Conference on Telecommunication and Signal Processing (TSP 2012) pp. 789-794, Prague, Czech Republic, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Thomas Chowdhury, Asaduzzaman, "Performance Analysis of Cooperative LDPC coding for Wireless Network", IEEE ANTS 2012 pp-137-140, Bengaluru, India. , 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Khandaker Mustakimur Rahman, Tauhidul Alam and Mahfuzulhoq Chowdhury, "Location Based Early Disaster Warning and Evacuation System on Mobile Phones Using OpenStreetMap", IEEE Conference on Open Systems , Malaysia, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Tauhidul Alam , S. A. Majumder, M. K. Akon, I. M. Bokhary, "OpenStreetMap for Promoting Tourism in Bangladesh", ICACE , Bangladesh, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Md. Mustofa Kamal, Kazi Zakia Sultana, "A Comprehensive Tool for Text Categorization and Text Summarization in Bioinformatics", 15th International Conference on Computer & Information Technology (ICCIT), University of Chittagong, Chittagong , Bangladesh, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Sujan Chowdhury, Md. MehediKaysar, Kaushik Deb, "Designing a Semantic Web Ontology of gricultural Domain", International Forum on Strategic Technology (IFOST) , Russia, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Kaushik Deb, My Ha Le, Byung-Seok Woo and Kang-Hyun Jo, "Automatic Vehicle Identification by Plate Recognition for Intelligent Transportation Systems", Journal of Lecture Notes in Artificial Intelligence, ISSN: 0302-9743, Part II Vol. 6704, pp. 163-172, Springer, Germany, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Kaushik Deb and Kang-Hyun Jo, "Vehicle License Plate Detection Method Based on HDR Image Generation from Multiple Computers", Computer Science & Engineering Research Journal, ISSN: 1990-4010 Vol. 6, pp. 27-34, Bangladesh, 2011</td>
                    <td>2011</td>
                </tr>
            </table>
            <div class="pagination">
                <a href="research5.blade.php">&laquo;</a>
                <a href="research1.blade.php">1</a>
                <a href="research2.blade.php"  >2</a>
                <a href="research3.blade.php" >3</a>
                <a href="research4.blade.php" >4</a>
                <a href="research5.blade.php">5</a>
                <a href="research6.blade.php" class="active">6</a>
                <a href="research7.blade.php">7</a>
                <a href="research8.blade.php">8</a>
                <a href="research9.blade.php">9</a>
                <a href="research10.blade.php">10</a>
                <a href="research7.blade.php">&raquo;</a>
            </div>
        </div>
    </div>
    <div class="footer" style="height:70px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>