<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CUET CSE</title>
    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height: 1600px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .slide{
            height: 410px;
            background: linear-gradient(white ,#bbdefb , #90caf9);
        }
        .inside{
            height: 350px;
            width: 750px;
            margin-left: 310px;
        }
        .body{
            height: 450px;
            background: linear-gradient(#e3f2fd ,#bbdefb , #90caf9);
        }
        .col-lg-5{
            margin-left: 30px;
            height: 370px;
            background: linear-gradient(#90caf9 ,#bbdefb , #e3f2fd );
        }
        .body1{
            height: 450px;
            background: linear-gradient(#90caf9 ,#bbdefb , #e3f2fd);
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;<li class="active"><a href="home1.blade.php">Home</a></li>
                        <li><a href="admission.blade.php">Admission</a></li>
                        <li><a href="faculty.blade.php">Faculty Members</a> </li>
                        <li><a href="#">Notice Board</a></li>
                        <li><a href="#">Upcoming Events</a></li>
                        <li><a href="class_routine.blade.php">Class Routine</a></li>
                        <li><a href="research.blade.php">Research</a></li>
                        <li><a href="#">Alumni</a></li>
                        <li><a href="contact.blade.php">Contact Info</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest

                            <li><a href="{{ route('login') }}">Admin</a></li>

                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                @endguest
                    </ul>
                </div>
            </div>
        </nav>

    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            <h3> Level 4 Term 1 Routine(Section A)</h3>
            <hr>

            {!! Form::open(['url'=>'/FouroneA/store']) !!}

            {!! Form::label('sub_title','Subject Code Title:') !!}
            {!! Form::text('sub_title','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('teacher_name','Teacher Name:') !!}
            {{ Form::select('teacher_name', [
            'Please Select One',
               'MD. Shamsul Arefin' => 'MD. Shamsul Arefin',
               'MD. Ibrahim Khan' => 'MD. Ibrahim Khan',
               'Kaushik Deb' => 'Kaushik Deb',
               'MD. Moshiul Hoque' => 'MD. Moshiul Hoque',
               'Asaduzzaman' => 'Asaduzzaman',
               'Pranab kumar Dhar' => 'Pranab kumar Dhar',
               'MD. Mokammel Haque' => 'MD. Mokammel Haque',
               'Abu Hasnat Mohammad Ashfak Habib' => 'Abu Hasnat Mohammad Ashfak Habib',
               'MD. Obaidur Rahman' => 'MD. Obaidur Rahman',
                'MD. Saki Kowsar' => ' MD. Saki Kowsar',
                'MD. Kamal Hossen' => 'MD. Kamal Hossen',
                 'Rahma Bintey Mufiz Mukta' => 'Rahma Bintey Mufiz Mukta',
                 'Lamia Alam' => 'Lamia Alam',
                'Shayla Sharmin' => 'Shayla Sharmin',
                'MD. Sabir Hossain' => 'MD. Sabir Hossain',
                'Farzana Yasmin' => 'Farzana Yasmin',
               'Animesh Chandra Roy' => 'Animesh Chandra Roy',
               'MD. Shafiul Alam Forhad ' => 'MD. Shafiul Alam Forhad ',
               'Jibon Naher' => 'Jibon Naher',
               'Sharmistha Chandra Tista' => 'Sharmistha Chandra Tista',
               'Tanzina Akter' => 'Tanzina Akter']
            ) }}
            <br>
            {!! Form::label('day',' Day:') !!}
            {{ Form::select('day',['Please Select One',
   'Sunday' => 'Sunday',
   'Monday' => 'Monday',
   'Tuesday' => 'Tuesday',
   'Wednesday' => 'Wednesday',
   'Thursday' => 'Thursday']
) }}
            {!! Form::label('time',' Time:') !!}
            {{ Form::select('time',  ['Please Select One',
   '9.00 a.m-9.50 a.m' => '9.00-9.50',
   '9.50 a.m-10.40 a.m' => '9.50-10.40',
   '11.00 a.m-11.50 a.m' => '11.00-11.50',
   '11.50 a.m-12.40 p.m' => '11.50-12.40',
   '12.40 p.m-1.30 p.m' => '12.40-1.30',
   '2.30 p.m-3.20 p.m' => '2.30-3.20',
   '3.20 p.m-4.10 p.m' => '3.20-4.10',
   '4.10 p.m-5.00 p.m' => '4.10-5.00']
) }}
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}
            <br><br>
            <a href="index"><button type="button" class="btn btn-primary">Full Routine</button></a>

            {!! Form::close() !!}

        </div>
    </div>
    <div class="footer" style="height:90px; background-color: #afd9ee; ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>

</div>
</body>
</html>

