<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CUET CSE</title>
    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height: 1600px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .slide{
            height: 410px;
            background: linear-gradient(white ,#bbdefb , #90caf9);
        }
        .inside{
            height: 350px;
            width: 750px;
            margin-left: 310px;
        }
        .body{
            height: 450px;
            background: linear-gradient(#e3f2fd ,#bbdefb , #90caf9);
        }
        .col-lg-5{
            margin-left: 30px;
            height: 370px;
            background: linear-gradient(#90caf9 ,#bbdefb , #e3f2fd );
        }
        .body1{
            height: 450px;
            background: linear-gradient(#90caf9 ,#bbdefb , #e3f2fd);
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;<li class="active"><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a> </li>
                    <li><a href="notice.blade.php">Notice Board</a></li>
                    <li><a href="upcomingevent.blade.php">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest

                        <li><a href="{{ route('login') }}">Admin</a></li>

                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @endguest
                </ul>
            </div>
    </div>
        </nav>

    </div>

    <div class="slide">
        <div class="inside">
            <center>
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="{{URL::asset('/images/slide/meyor.jpg')}}" alt="First slide"><br>
                        <h4 style="color: blue"> NCPC Ending Ceremony organised by Department of CSE,CUET.</h4>
                    </div>
                    <div class="item">
                        <img src="{{URL::asset('/images/slide/NHSPC1.JPG')}}" alt="Second slide"><br>
                        <h4 style="color: blue"> National High School Programming Contest organised by CSE,CUET.</h4>
                    </div>
                    <div class="item">
                        <img src="{{URL::asset('/images/slide/slide4.jpg')}}" alt="Third slide"><br>
                        <h4 style="color: blue"> Computer Club Programming Class.</h4>
                    </div>
                    <div class="item">
                        <img src="{{URL::asset('/images/slide/slide2.jpg')}}" alt="fourth slide"><br>
                        <h4 style="color: blue"> Digital Multimedia Lab. </h4>
                    </div>
                    <div class="item">
                        <img src="{{URL::asset('/images/slide/slide3.jpg')}}" alt="fifth slide"><br>
                        <h4 style="color: blue"> CUET Students in NCPC.</h4>
                    </div>
                </div>
                <a class="left carousel-control" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            </center>
        </div>
    </div>

    <div class="body">
        <br>
        <div class="col-lg-5" id="div1">
            <h3 style="color: white">About Department</h3>
            <hr>
            <center><img src="{{URL::asset('/images/body/department.jpg')}}"></center>
            <p> To face the 21st
            century's challenges and to confirm the institutes position at
            the highest level of research, learning and teaching, the computer science
            and engineering department has been established. Modern world is moving towards
            the machine and information world, from labor intensive to machine intensive, so the need
            for computer graduates is the demand of the time.
        </div>
        <div class="col-lg-5" style="margin-left: 150px; " id="div2">
            <h3 style="color: white">About Campus Life</h3>
            <hr>
            <center><img src="{{URL::asset('/images/body/campus.jpg')}}"></center>
            <p>
                The campus life at Chittagong University of Engineering & Technology
                is full of opportunities and activities to complement the regular academic
                undertakings along with various services and facilities.Being a part of the department
                also means being a part of the Dhaka University which has been a symbol of education and
                cultural progress in the country.
            </p>
        </div>
    </div>
    <div class="body1">
        <br>
        <div class="col-lg-5" id="div3" style="background: linear-gradient( #e3f2fd,#bbdefb , #90caf9);">
            <h3 style="color: black">Message From VC</h3>
            <hr>
            <div class="col-lg-4">
            <img src="{{URL::asset('/images/body/vc.jpg')}}">
            </div>
            <div class="col-lg-8">
                <p>
                    Your interest and enthusiasm to visit our website is highly appreciated.<br>
                    The Chittagong University of Engineering & Technology (CUET) started its journey with
                    the core mission of developing quality human resources in the field of engineering and
                    technology to serve the nation and the world.It has been accumulating this kudos over
                    the period of 47 years since its inception as engineering college in 1968. At present,
                    the university  has 15 <a href="vc.blade.php"><b>Continue Reading..</b></a>
                </p>
            </div>

        </div>
        <div class="col-lg-5" id="div4" style="margin-left: 150px; background: linear-gradient( #e3f2fd,#bbdefb , #90caf9);">
            <h3 style="color: black">Message From Departmental Head</h3>
            <hr>
            <div class="col-lg-4">
            <img src="{{URL::asset('/images/body/dh.jpg')}}">
            </div>
            <div class="col-lg-8">
                <p>
                    Greetings! On behalf of the faculty members, staffs, and students of Department of Computer Science & Engineering of Chittagong University of Engineering & Technology (CUET), I welcome you all to the creative world of IT (Information Technology). I am really happy for the excitement periods of computer scientists for the last decade and the time ahead. Because I believe CSE discipline has been widely recognized as an essential source and technique for the advancements in all spheres of human endeavor now and in future. <a href="dh.blade.php"><b>Continue Reading..</b></a>
                </p>
            </div>

        </div>

    </div>
    <div class="footer" style="height:90px; background-color: #afd9ee; ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>

</div>
</body>
</html>