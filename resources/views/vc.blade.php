<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:900px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 1040px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .center {
            text-align: center;
        }

        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #000;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {background-color: #ddd;}
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
        .body{

            height: 1190px;
            width: 700px;
            background-color: #1b6d85;
            border: solid black;

            margin-left: 300px;
            margin-top: 15px;
            border-width: .5px;
            padding-left: 10px;
            padding-top: 10px;
            padding-right: 10px;
            text-align: justify;
            color: white;

        }

        .footer{
            width: 680px;
            height: 50px;
            background-color: #46b8da;
            color: black;
            padding-top: 15px;
            text-align: center;
            font-size: smaller;

        }



    </style>
</head>
<body style="background: linear-gradient(white , white, #afd9ee); height: 1450px">
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li ><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>

<div class="body">

    <h2>  Message from VC </h2>
    <hr style="background-color: white">
    <img src="{{URL::asset('/images/body/vc.jpg')}}" style="margin-left: 240px"><br> <br> <br>

    <p>
        Your interest and enthusiasm to visit our website is highly appreciated. <br> <br>

        The Chittagong University of Engineering & Technology (CUET) started its journey with the core mission of developing quality human resources in the field of engineering and technology to serve the nation and the world. It has been accumulating this kudos over the period of 47 years since its inception as engineering college in 1968. At present, the university has 15 departments and 2 research centers. About 700 undergraduate students get admission into 9 degree awarding departments every year and the graduate programs (MSc, MEng, MPhill, PhD) are running under 8 departments.

        <br> <br>
        To fulfill our central mission, we are inducing the brightest students in our undergraduate academic programs, through highly competitive admission test. Academic programs are complemented with the commitment to provide our students a global career by the frequently updated curricula. In addition to our dedicated faculty, the students are able to gather hands-on industry’s experience as CUET is enjoying geographical advantages as the solitary engineering university at the south-east region of the country. Chittagong region holds its eminence for the country’s premier sea-port, myriad of heavy and light industries.

        <br> <br>
        In addition to the above, we are equally putting emphasis on joint collaboration in research programs with the number of internationally leading universities and institutions. As a disaster prone country, we have already started research based graduate program in disaster and environmental engineering, and set up an earthquake engineering research center with the assistance of our global partners. Nevertheless, CUET is invariably focusing on more industry-academia relationships for the sake of invention and building leadership. I am pleased to apprise you that our alumni are now at the helm of significant number of prestigious national and international positions.

        <br> <br>
        More importantly, we have taken strategic development plan to set up total 18 departments, 3 institutes, 3 research centers, 4 directorates and 1 central research-lab by the year 2020. As Vice Chancellor, I cordially invite our alumni, donor agencies/organizations, philanthropists and industrialists to join us in order to fully harness the potentiality of CUET and thus to contribute to the nation’s socio-economic development as well as the progress of mankind.

        <br> <br>
        Finally, I look forward to warm welcoming you at naturally pristine CUET campus.

        <br> <br> <br>
        Prof. Dr. Mohammad Rafiqul Alam <br>
        Vice-Chancellor

        <br> <br>



        <div class="footer" style="height:70px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
    <br>
    <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

</div>
</div>
</body>
</html>