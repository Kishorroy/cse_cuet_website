<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dr. Md. Ibrahim Khan</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height: 1600px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .nopadding{
            padding: 0;
            margin-top: 30px;
            background: linear-gradient(white ,white , #afd9ee);
        }
        .nopadding h4{
            background: linear-gradient( #afd9ee,white , #afd9ee);
            padding: 10px 15px;
        }

        .nopadding ul li{

            background: url("/public/images/arrow.jpg") no-repeat left -1.6rem;
            padding-left: 10px;
            background-size: 25px 35px;
            margin: 15px 17px;
        }


    </style>
</head>
<body>

<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li class="active"><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>
    </div>

    <div class="col-lg-10" style="margin-left: 100px ">
        <div class="col-md-4">
            <img src="{{URL::asset('/images/teachers/ibrahim.jpg')}}" style="width: 300px;height: 300px;margin-top: 20px">
        </div>
        <div class="col-md-8">
            <h1 style="margin-bottom: 0">Dr. Md. Ibrahim Khan</h1>
            <br>
            <h2 style="margin: 0;padding: 0;">ডঃ মোঃ ইব্রাহিম খান</h2><hr>
            <h4>Professor</h4>
            <b>E-mail:</b> 	muhammad_ikhancuet@yahoo.com
            <br><b>Contact:</b> 01713-018506<hr>
            <b>Research Interests:</b>
            Human Robot Interaction	,Computer Graphics, Image Processing
            <hr>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Education :</b></h4>
            <ul>
                <li><b>B.Sc.</b><br>Electrical & Electronics Engineering<br>Bangladesh University Of Enggineering And Technology<b>(BUET)</b><br>Bangladesh</li>
                <li><b>M.Sc.</b><br>Computer Science<br>Bangladesh University Of Enggineering And Technology<b>(BUET)</b><br>Bangladesh</li>
                <li><b>PhD</b><br>Jahangirnagar University, <br>Bangladesh</li>
            </ul>
        </div>


        <div class="col-sm-12 nopadding">
            <h4><b>Related Courses(Undergraduate):</b></h4>
            <div class="col-md-6">
                <ul>
                    <li>Computer Graphics</li>

                </ul>
            </div>
            <div class="col-md-6">
                <ul>
                    <li>Basic Electrical</li>

                </ul>
            </div>
        </div>

        <div class="col-sm-12 nopadding" style="background: linear-gradient( white ,#afd9ee, #afd9ee);">
            <h4><b>Publications :</b></h4>
            <h5 align="center"><b>Journals </b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>Mohammad Ibrahim Khan, Md. Maklachur Rahman and Md. Iqbal Hasan Sarker, “Digital Watermarking for Image Authentication Based on Combined DCT, DWT and SVD Transformation”, International Journal of Computer Science Issues (IJCSI), Vol. 10, Issue 3, No 1, May 2013	</li>
                <li>Md. Iqbal Hasan Sarker, Muhammad Ibrahim Khan, Kaushik Deb, and Md. Faisal Faruque, ''FFT Based Audio Watermarking Method with a Gray Image for Copyright Protection”, International Journal of Advanced Science & Technology (IJAST), Vol. 47, Australia</li>
                <li>Muhammad Ibrahim Khan, Md. Iqbal Hasan Sarker, Kaushik Deb, and Md. Hasan Farhad, ''A New Audio Watermarking Method Based on Discrete Cosine Transform with a Gray Image”, International Journal of Computer Science & Information Technology (IJCSIT), Vol. 4, No. 4, pp. 01-10, AIRCC</li>
            </ul>


            <hr>
            <h5 align="center"><b>Conferences</b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
               <li>Md. Iqbal H. Sarker and M.I.Khan, “An Improved Blind Watermarking Method in Frequency Domain for Image Authentication “ , 2nd International Conference on Informatics, Electronics & Vision (ICIEV), IEEE, 2013.</li>

            </ul>

        </div>




    </div>

    <div class="footer" style="margin-top: 1320px; height:90px; background-color: #afd9ee; ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>

</div>



</body>
</html>