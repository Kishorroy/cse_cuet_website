<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:710px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 880px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .center {
            text-align: center;
        }

        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #000;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {background-color: #ddd;}
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li class="active"><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>
    <div class="map">
        <div class="center" >
            <br>
            <table id="customers">
                <tr>
                    <th>Publication Type</th>
                    <th><center>Publication Details</center></th>
                    <th>Year</th>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Md. Moshiul Hoque, Y. Kobayashi, Y. Kuno, and Kaushik Deb, "Design a Robotic Head to Attract Human Attention by considering viewing situations", Computer Science and Engineering Research Journal (CSERJ) Vol. 08, pp. 29-38, , 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Y. Morimoto, Mohammad Shamsul Arefin and M. A. Siddique, "Agent-based Anonymous Skyline Set Computation in Cloud Databases", Int. J. Computational Science and Engineering Vol. 7, No. 1, pp. 73-81, , 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Mohammad Shamsul Arefin and Y. Morimoto, "Management of Multilingual Contents in Web Environment", International Journal of Computing Communication and Networking Research Vol. 1, Issue-1, pp. 18-37, , 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Mahfuzul Hoq Chowdhury, Asaduzzaman, M.F.Kader and Mohammad Obaidur Rahman, "Design of an Efficient MAC Protocol for Opportunistic Cognitive Radio Networks", International Journal of Computer Science & Information Technology (IJCSIT) Vol 4, No 5, , 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Hussain Md. Mehedul Islam and Mohammad Obaidur Rahman, "Algorithm for Linear Number Partitioning into Maximum Number of Subsets", International Journal of Computer Applications (0975 – 8887) Vol 54 – No.10, , 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar, Mohammad Ibrahim Khan, Ashoke Kumar Sen Gupta and Jong-Myon Kim, "An Efficient Real Time Moving Object Detection Method for Video Surveillance System", International Journal of Signal Processing, Image Processing and Pattern Recognition (IJSIP) Vol. 5. No. 3, pp. 93-110, , 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Md. Iqbal Hasan Sarker, Muhammad Ibrahim Khan, Kaushik Deb, and Md. Faisal Faruque, "FFT Based Audio Watermarking Method with a Gray Image for Copyright Protection", International Journal of Advanced Science & Technology (IJAST) Vol. 47, Australia, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Enamul Hoque, O. Hoeber, M. Gong, "Balancing the trade-offs between diversity and precision for Web image search using concept-based query expansion", Journal of Emerging Technologies in Web Intelligence Vol: 4, No 1, pp. 26-34, , 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Jounal</td>
                    <td>Anik Saha, Dipanjan Das Roy, Tauhidul Alam and Kaushik Deb, "Automated Road Lane Detection for Intelligent Vehicles", Global Journal of Computer Science & Technology(GJCST) Vol. 12, Issue 6 (Ver. 1), pp. 10-15, , 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Kaushik Deb, Md. Sajib Al-Seraj, Md. Moshiul Hoque and Md. Iqbal Hasan Sarker, "Combined DWT-DCT Based Digital Watermarking Technique for Copyright Protection", In Proceedings of the 7th IEEE International Conference on Electrical & Computer Engineering (ICECE) pp. 458-461, Bangladesh, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Md. Moshiul Hoque and Kaushik Deb, "Robotic System for Making Eye Contact Pro-activity with Humans", In Proceedings of the 7th IEEE International Conference on Electrical & Computer Engineering (ICECE), pp. 125-128, Bangladesh, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Kaushik Deb, Muhammad Kamal Hossen, Md. Ibrahim Khan, and Md. Rafiqul Alam, "Bangladeshi Vehicle License Plate Detection Method Based on HSI Color Model and Geometrical Properties", In Proceedings of the7th International Forum on Strategic Technology (IFOST) , Tomsk, Russia, 2012</td>
                    <td>2012</td>
                </tr>
            </table>
            <div class="pagination">
                <a href="research3.blade.php">&laquo;</a>
                <a href="research1.blade.php">1</a>
                <a href="research2.blade.php"  >2</a>
                <a href="research3.blade.php" >3</a>
                <a href="research4.blade.php" class="active">4</a>
                <a href="research5.blade.php">5</a>
                <a href="research6.blade.php">6</a>
                <a href="research7.blade.php">7</a>
                <a href="research8.blade.php">8</a>
                <a href="research9.blade.php">9</a>
                <a href="research10.blade.php">10</a>
                <a href="research5.blade.php">&raquo;</a>
            </div>
        </div>
    </div>
    <div class="footer" style="height:70px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>