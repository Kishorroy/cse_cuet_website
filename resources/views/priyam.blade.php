<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Priyam Biswas</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height: 1600px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .nopadding{
            padding: 0;
            margin-top: 30px;
            background: linear-gradient(white ,white , #afd9ee);
        }
        .nopadding h4{
            background: linear-gradient( #afd9ee,white , #afd9ee);
            padding: 10px 15px;
        }

        .nopadding ul li{

            background: url("/public/images/arrow.jpg") no-repeat left -1.6rem;
            padding-left: 10px;
            background-size: 25px 35px;
            margin: 15px 17px;
        }





    </style>
</head>
<body>

<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li class="active"><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>
    </div>

    <div class="col-lg-10" style="margin-left: 100px ">
        <div class="col-md-4">
            <img src="{{URL::asset('/images/teachers/priyam.jpg')}}" style="width: 300px;height: 300px;margin-top: 20px">
        </div>
        <div class="col-md-8">
            <h1 style="margin-bottom: 0">Priyam Biswas</h1>
            <br>
            <h2 style="margin: 0;padding: 0;">প্রিয়াম বিশ্বাস</h2><hr>
            <h4>Lecturer</h4>
            <b>E-mail: </b>priyam_cse@yahoo.com pbiswas@cuet.ac.bd
            <br><b>Contact: </b>+88-01710805255<hr>
            <b>Research Interests:</b>
            Machine Learning,  	Bioinformatics,  Intelligent User Interfaces, 	Wireless Networking

            <hr>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Education :</b></h4>
            <ul>
                <li><b>B.Sc.</b><br>Computer Science<br>Bangladesh University Of Engineering & Technology<b>(BUET)</b><br>Bangladesh</li>
            </ul>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Related Courses(Undergraduate):</b></h4>
            <div class="col-md-6">
                <ul>
                    <li>Artificial Intelligence </li>
                    <li>Structured Programming</li>

                </ul>
            </div>
            <div class="col-md-6">
                <ul>
                    <li>Algorithm Analysis & Design</li>
                    <li> Computer Networks</li>

                </ul>
            </div>
        </div>

        <div class="col-sm-12 nopadding" style="background: linear-gradient( white ,#afd9ee, #afd9ee);">
            <h4><b>Awards :</b></h4>

            <ul>
                <li> Khadizatunessa Scholarship, BUET </li>
                <li>Dean’s list award for academic excellence in level 3 and level 4, BUET </li>
                <li>University Merit Scholarship for excellence in three terms, BUET </li>
                <li>Chittagong Education Board Scholarship for excellence in HSC examination</li>
                <li>Chittagong Education Board Scholarship for excellence in SSC examination </li>
            </ul>

        </div>


        <div class="col-sm-12 nopadding" style="background: linear-gradient( white ,#afd9ee, #afd9ee);">
            <h4><b>Publications :</b></h4>
            <h5 align="center"><b>Journals</b></h5>
            <hr>
            <ul>
                <li>Kaushik Deb, Sayem Mohammad Imtiaz, and Priyam Biswas, “A Motion Region Detection and Tracking Method”, SmartComputing Review, DOI: 10.6029/smartcr.2014.01.008, Vol. 4, No.1, pp. 79-90, The Korea Academia-Industrial Cooperation Society (KAIS), South Korea, 2014</li>
                <li>Md. Asaduzzaman Sabuj and Priyam Biswas, “Colon Cancer Prediction based on Artificial Neural Network”, Global Journal of Computer Science and Technology, Vol: 13, Issue 3(Ver. 1.0), pp. 23-27, USA, 2013.</li>
                <li>Md. Monjurul Islam, Md. Abdullah Bin Amin and Priyam Biswas “Increasing Speech Ability of the Autistic Children by an Interactive Computer Game” Global Journal of Computer Science and Technology, Vol: 13, Issue 9(Ver. 1.0), pp. 23-27, USA, 2013.</li>
            </ul>

            <hr>
            <h5 align="center"><b>Conferences</b></h5>
            <hr>
            <ul>
                <li>Priyam Biswas, A.K.M. Ashikur Rahman and Kaushik Deb. “Theoretical Aspects of Broadcasting Algorithms in Ad Hoc Wireless Network”, in Proc. of the 8th International Forum on Strategic Technology (IFOST), V- II, pp. 17-21, 2013</li>
                <li>A. H. Suny, K. Deb, P. Biswas and M. M. Hoque, “Shadow Detection and Removal Based on YCbCr Color Space”, In Proc. of the National Conference on Intelligent Computing & Information Technology(NCICIT), pp. 54-59, CUET, Chittagong, Bangladesh, October 2013.</li>
            </ul>

        </div>




    </div>

    <div class="footer" style="margin-top: 1500px; height:90px; background-color: #afd9ee; ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>

</div>



</body>
</html>