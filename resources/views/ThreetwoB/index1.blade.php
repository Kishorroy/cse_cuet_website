<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<head>

    <style>

        td{

            width:100px;
            height:50px


        }

        .container{
            position: absolute;
            left: 50px;
            top: 150px;

        }


    </style>

</head>




<div class="container">




    Total: {!! $allData->total() !!} Record(s) <br>

    Showing: {!! $allData->count() !!} Record(s) <br>

    {!! $allData->links() !!}




    <table class="table table-bordered table table-striped" >
        <th>Day</th>
        <th>Time</th>
        <th>Subject Code</th>
        <th>Teacher Name</th>




        @foreach($allData as $oneData)

            <tr>
                <td>  {!! $oneData['day'] !!} </td>
                <td>  {!! $oneData['time'] !!} </td>
                <td>  {!! $oneData['sub_title'] !!} </td>
                <td>  {!! $oneData['teacher_name'] !!} </td>




            </tr>


        @endforeach


    </table>
    {!! $allData->links() !!}
</div>
