<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>

        .container{
            height:900px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 1100px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .center {
            text-align: center;
        }

        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #000;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {background-color: #ddd;}
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li class="active"><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>
    <div class="map">
        <div class="center" >
            <br>
            <table id="customers">
                <tr>
                    <th>Publication Type</th>
                    <th><center>Publication Details</center></th>
                    <th>Year</th>
                </tr>

                <tr>
                    <td>Conference</td>
                    <td>Pranab Kumar Dhar, Mohammad Ibrahim Khan andAl Amin Bhuyia, "Watermarking Method in Frequency Domain for Digital Audio", 2nd International Conference on Computational Intelligence Application (ICCIA-2011) pp. 45-49, Nashik, India, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Enamul Hoque, G. Strong, O. Hoeber, M. Gong, "Conceptual query expansion and visual search results exploration for Web image retrieval", Atlantic Web Intelligence Conference pp. 73-82, Fribourg, Switzerland, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Enamul Hoque, O. Hoeber, M. Gong, "Evaluating the trade-offs between diversity and precision for Web image search using concept-based query expansion", IEEE/WIC/ACM International Conference on Web Intelligence - Workshops (International Workshop on Intelligent Web Interaction) pp. 130-133, , Lyon, France, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Enamul Hoque, O. Hoeber, M. Gong, "Concept-based interactive visualization approach to Web image search (interactive poster)", Compendium of the IEEE Information Visualization Conference , Providence, RI, USA, 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Anik Saha, Kaushik Deb, Mohammad Ibrahim Khan, and Kang-Hyun Jo, "Bangla Character Recognition Using Artificial Neural Network", ICMERE-2011 , , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Alan A. Dombkowski, Kazi Zakia Sultana, Douglas B. Craig, Hasan Jamil, "In silico Analysis of combinatorial microRnA Activity Reveals Target Genes and pathways Associated with Breast cancer Metastasis", Cancer Informatics 2011 10:1329, doi:10.4137/CIN.S6631, , 2011</td>
                    <td>2011</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Kaushik Deb, Andrey Vavilin, Jung-Won Kim, and Kang-Hyun Jo, "Vehicle License Plate Tilt correction Based on the Straight Line Fitting Method and Minimizing Variance of Coordinates of Projection Points ", International Journal of Control, Automations and Systems, ISSN: 2005-4092 (Electronic) Vol. 8, No.5, pp. 27-34, Springer, Germany, 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar, Mohammed Ibrahim Khan, Kaushik Deb, and Jong-Myon Kim, "A Modified Spectral Modeling Synthesis Algorithm for Whale Sound", International Journal of Computer Science and Network Security ISSN: 1738-7906, pp. 34-41, Vol. 10, No.9, S. Korea, 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Andrey Vavilin, Kaushik Deb, and Kang-Hyun Jo, "Fast HDR Image Generation Technique based on Exposure Blending", Journal of Lecture Notes in Artificial Intelligence ISSN: 0302-9743 print, Part III, Springer, Germany, 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>M. M. Moshiul, A. M. Mahbub, "An Efficient Framework for Network Intrusion Detection", Georgian Electronic Scientific Journal: Computer Science and Telecommunications Vol. 24, No. 1, pp. 32-42, Germany, 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Hyung Yun Kong and Asaduzzaman, "Distributed Clustering Algorithm to Explore Selection Diversity in Wireless Sensor Networks", IEICE Transactions on Communication Vol. E93-B, no. 5, pp. 1232–1239.(http://ieice.org) 2010/IEICE, , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Asaduzzaman and Hyung Yun Kong , "Distributed Cooperative Routing Algorithm for Multi-hop Multi-relay Wireless Networks", IEICE Transactions on Communication Vol. E93-B, No.04, pp.1049–1052.(http://ieice.org), 2010/IEICE, , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Asaduzzaman and Hyung Yun Kong , "Performance analysis of code combining based cooperative protocol with amplified-and-forward (AF) relaying", IEICE Transactions on Communication vol. E93-B, no. 2, pp. 2275–2278, (http://ieice.org), 2010/IEICE, , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Asaduzzaman and Hyung Yun Kong , "Energy Efficient Cooperative LEACH Protocol for Wireless Sensor Networks", Journal of Communications and Networking, Vol. 12, Iss. 4, pp. 358–365. (http://ieeexplore.ieee.org),2010/IEEE, , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Nur Mohammad, Md. Asiful Islam, Abdul Khaleque and M. S. Arefin, "Dispersion Control Characteristics of the Dielectric Cylindrical Waveguide And a Novel Approach of Dispersion Compensation", International Journal of Engineering and Technology , , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar, Mohammad Ibrahim Khan and Asif Ahmad, "A New DCT-Based Watermarking Method for Copyright Protection of Digital Audio", International Journal of Computer Science and Information Technology (IJCSIT) Vol. 2, No. 5, pp. 91-101, , 2010</td>
                    <td>2010</td>
                </tr>

            </table>
            <div class="pagination">
                <a href="research8.blade.php">&laquo;</a>
                <a href="research1.blade.php">1</a>
                <a href="research2.blade.php"  >2</a>
                <a href="research3.blade.php" >3</a>
                <a href="research4.blade.php" >4</a>
                <a href="research5.blade.php">5</a>
                <a href="research6.blade.php">6</a>
                <a href="research7.blade.php">7</a>
                <a href="research8.blade.php">8</a>
                <a href="research9.blade.php" class="active">9</a>
                <a href="research10.blade.php">10</a>
                <a href="research10.blade.php">&raquo;</a>
            </div>
        </div>
    </div>
    <div class="footer" style="height:70px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>