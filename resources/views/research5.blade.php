<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:710px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 950px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .center {
            text-align: center;
        }

        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #000;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {background-color: #ddd;}
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li class="active"><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>
    <div class="map">
        <div class="center" >
            <br>
            <table id="customers">
                <tr>
                    <th>Publication Type</th>
                    <th><center>Publication Details</center></th>
                    <th>Year</th>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Kaushik Deb, Sajib Al-Seraj, Md. Saki Kowser, Iqbal Hasan Sarker, "A joint DWT-DCT Based Watermarking Technique for Avoiding Unauthorized Replication", In Proceedings of the 7th International Forum on Strategic Technology (IFOST) , Tomsk, Russia, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Sujan Chowdhury, Kayasar Md. Mahedi, and Kaushik Deb, "Designing a Semantic Web Ontology of agricultural domain", In Proceedings of the7th International Forum on Strategic Technology (IFOST) , Tomsk, Russia, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Kaushik Deb, Md. Ibrahim Khan, Anik Saha, and Kang-Hyun Jo, "An Efficient Method of Vehicle License Plate Recognition Based on Sliding Concentric Windows and Artificial Neural Network", In Proceeding of the 2nd International Conference on Computer, Communication, Control and Information Technology (C3IT 2012), Procedia Technology Vol. 4, pp. 812-819, West Bengal, India, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Md. Moshiul Hoque, D. Das, T. Onuki, Y. Kobayashi, Y. Kuno, "Model for Controlling a Target Human's Attention in Multi-Party Settings", The 21st IEEE International Symposium on Robot and Human Interactive Communication (RO-MAN) pp. 476-483, France, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>D. Das, Md. Moshiul Hoque, T. Onuki, Y. Kobayashi, Y. Kuno, "Vision-based Attention Control System for Socially Interactive Robots", The 21st IEEE International Symposium on Robot and Human Interactive Communication (RO-MAN) pp. 96-502, France, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Md. Moshiul Hoque, D. Das, T. Onuki, Y. Kobayashi, Y. Kuno, "An Integrated Approach of Attention Control of Target Human by Nonverbal Behaviors of Robots in Different Viewing Situations", IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS) pp. 1399-1406, Portugal, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Md. Moshiul Hoque, T. Onuki, Y. Kobayashi, and Y. Kuno, "Attracting and Controlling Human Attention through Robot’s Behaviors Suited to the Situation", International Conference on Human Robot Interaction (HRI’12) pp. 149-150, USA, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Md. Moshiul Hoque,Kaushik Deb, "Robotic System for Making Eye Contact Pro-actively with Humans", 7th International Conference on Electrical and Computer Engineering (ICECE) pp. 125-128,, Bangladesh, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Kaushik Deb, M.S. Al-Seraj, Md. Moshiul Hoque, M.I.H. Sarker, "Combined DWT-DCT Based Digital Image Watermarking Technique for Copyright Protection", International Conference on Electrical & Computer Engineering (ICECE) , Bangladesh, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Md. Moshiul Hoque, Kaushik Deb, D. Das, Y. Kobayashi, Y. Kuno, "Design An Intelligent Robotic Head to Interacting with Humans", 15th International Conference on Computer and Information Technology (ICCIT) pp. 539-546, Bangladesh, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>58 Md. Moshiul Hoque, D. Das, Y. Kobayashi, Y. Kuno, Kaushik Deb, "Design an Intelligent Robotic Head for Human Robot Interaction", International Conference on Electrical, Computer and Telecommunication Engineering (ICECTE) pp. 565-568, Bangladesh, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>M. F. Kader, Asaduzzaman and M. Chowdhury, "Cooperative Secondary User Selection as a Relay for the Primary System in Underlay Cognitive Radio Networks", Proc IEEE 15th International Conference on Computer and Information Technology(ICCIT’2012) pp. 275–278., Bangladesh, 2012</td>
                    <td>2012</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Ma Geng, Mohammad Shamsul Arefin and Morimoto Yasuhiko, "A Spatial Skyline Query for a Group of Users Having Different Positions", The Third International Conference on Networking and Computing (IEEE, DBLP) pp. 137-142, , 2012</td>
                    <td>2012</td>
                </tr>
            </table>
            <div class="pagination">
                <a href="research4.blade.php">&laquo;</a>
                <a href="research1.blade.php">1</a>
                <a href="research2.blade.php"  >2</a>
                <a href="research3.blade.php" >3</a>
                <a href="research4.blade.php" >4</a>
                <a href="research5.blade.php" class="active">5</a>
                <a href="research6.blade.php">6</a>
                <a href="research7.blade.php">7</a>
                <a href="research8.blade.php">8</a>
                <a href="research9.blade.php">9</a>
                <a href="research10.blade.php">10</a>
                <a href="research6.blade.php">&raquo;</a>
            </div>
        </div>
    </div>
    <div class="footer" style="height:70px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>