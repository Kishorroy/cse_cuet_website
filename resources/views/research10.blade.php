<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height:900px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .map{
            height: 1040px;
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .center {
            text-align: center;
        }

        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #000;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {background-color: #ddd;}
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

    </style>
</head>
<body>
<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li class="active"><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li ><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>

    </div>
    <div class="map">
        <div class="center" >
            <br>
            <table id="customers">
                <tr>
                    <th>Publication Type</th>
                    <th><center>Publication Details</center></th>
                    <th>Year</th>
                </tr>

                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar, Mohammad Ibrahim Khan and P. M. Mahmudul Hasan, "Performance Enhancement of TCP for Wireless Network", Global Journal of Computer Science and Technology (GJCST) Vol. 10, No.12, pp. 32-37, , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar, Mohammad Ibrahim Khan, Kaushik Deb and Jong-Myon Kim, "A Modified Spectral Modeling Synthesis Algorithm for Whale Sound", International Journal of Computer Science and Network Security (IJCSNS) Vol. 10, No. 9, pp. 34-41, , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar and Jong-Myon Kim, "Image Watermarking Method using Efficient Systematic Linear Block Codes in DCT Domain", Journal ofthe Engineering and Arts Society in Korea (JEASKO) Vol. 2, No. 1, pp. 95-109, , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar, Mohammad Ibrahim Khan and Jong-Myon Kim, "A New Audio Watermarking System Based on Discrete Fourier Transform for Copyright Protection", International Journal of Computer Science and Network Security?(IJCSNS) Vol. 10, No. 6, pp. 35-40, , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar, Choel-Hong Kim and Jong-Myon Kim, "Robust Audio Watermarking Scheme Based on Spectral Modeling Synthesis", Journal of Acoustical Society of America (JASA) Vol. 127, No. 3, pp. 1983, , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Pranab Kumar Dhar and Jong-Myon Kim, "Robust Audio Watermarking in Frequency Domain for Copyright Protection", Korea Society of Computer and Information (KSCI) Transaction Vol. 15, No. 2, pp. 109-117, Korea, 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Hee-Sung Jun, Pranab Kumar Dhar, Choel-Hong Kim and Jong-Myon Kim, "Baleen Whale Sound Synthesis using a Modified Spectral Modeling", Korea Information Processing Society (KIPS) Transaction Vol. 17 B, No. 1, pp. 69-78, Korea, 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>M. A. Islam, M. K. Hossen, and M. S. A. Chowdhury, "An Efficient Method of Optical Character Recognition System for Bangla Numerals using Fuzzy Logic", Computer Science and Engineering Research Journal(CSERJ) Vol.-06, Page No:-68-80, Bangladesh, 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>M. M. Rahman, M. A. Islam, and M. K. Hossen, "Upper Leg and Knee Design for a Humanoid Walking Robot", Computer Science and Engineering Research Journal (CSERJ) Vol.-06, Page No: -81-89, , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Mohammad Ibrahim Khan, Muhammad Kamal Hossen, Md. Sabbir Ali, "Cell Segmentation from Cellular Image", Global Journal of Computer Science and Technology (GJCST) Vol. 10 Issue 13 (Ver. 1.0) , Page No: 55-60, , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Kazi Zakia Sultana, Anupam Bhattacharjee, and Hasan Jamil, "Querying KEGG Pathways in Logic", International Journal of Data Mining and Bioinformatics (IJDMB) , , 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Journal</td>
                    <td>Andrey Vavilin, Kaushik Deb, Tae-Ho Kim, and Kang-Hyun Jo, "Road Sign Detection Method based on Fast HDR Image Generation Technique", In Proceeding of the 25th IEEE International Conference of Image and Vision Computing New Zealand (IVCNZ) , Queenstown, New Zealand, 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>My-Ha Le, Kaushik-Deb, and Kang-Hyun Jo, "Recognizing Outdoor Scene Objects Using Texture Features and Probabilistic Appearance Model", In Proceeding of the 10th International Conference on Control Automation and Systems (ICCAS 2010) pp. 1440-1444, Kintex, Gyeonggi-do, Korea, 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Kaushik-Deb, Andrey Vavilin, Jung-Won Kim, and Kang-Hyun Jo, "Vehicle License Plate Tilt Correction Based on the Straight Line Fitting Method ", In Proceeding of the 17th ITS (Intelligent Transportation Systems) , World Congress, Busan, Korea, 2010</td>
                    <td>2010</td>
                </tr>
                <tr>
                    <td>Conference</td>
                    <td>Kaushik-Deb, Andrey Vavilin, Jung-Won Kim, and Kang-Hyun Jo, "Projection and Least Square Fitting with Perpendicular Offsets based Vehicle License Plate Tilt Correction", In Proceeding of the IEEE SICE (The Society of Instrument and Control Engineers) , Taipei, Taiwan, 2010</td>
                    <td>2010</td>
                </tr>
            </table>
            <div class="pagination">
                <a href="research9.blade.php">&laquo;</a>
                <a href="research1.blade.php">1</a>
                <a href="research2.blade.php"  >2</a>
                <a href="research3.blade.php" >3</a>
                <a href="research4.blade.php" >4</a>
                <a href="research5.blade.php">5</a>
                <a href="research6.blade.php">6</a>
                <a href="research7.blade.php">7</a>
                <a href="research8.blade.php">8</a>
                <a href="research9.blade.php" >9</a>
                <a href="research10.blade.php" class="active">10</a>

            </div>
        </div>
    </div>
    <div class="footer" style="height:70px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>
</div>
</body>
</html>