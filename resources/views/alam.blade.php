<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tauhidul Alam</title>
    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .container{
            height: 1200px;
            width: 1350px;
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }
        .nopadding{
            padding: 0;
            margin-top: 30px;
            background: linear-gradient(white ,white , #afd9ee);
        }
        .nopadding h4{
            background: linear-gradient( #afd9ee,white , #afd9ee);
            padding: 10px 15px;
        }

        .nopadding ul li{

            background: url("/public/images/arrow.jpg") no-repeat left -1.6rem;
            padding-left: 10px;
            background-size: 25px 35px;
            margin: 15px 17px;
        }





    </style>
</head>
<body>

<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>
    </div>

    <div class="col-lg-10" style="margin-left: 100px ">
        <div class="col-md-4">
            <img src="{{URL::asset('/images/teachers/taua.jpg')}}" style="width: 300px;height: 300px;margin-top: 20px">
        </div>
        <div class="col-md-8">
            <h1 style="margin-bottom: 0">Tauhidul Alam</h1>
            <br>
            <h2 style="margin: 0;padding: 0;">তৌহিদুল আলম</h2><hr>
            <h4>Assistant Professor</h4>
            <b>E-mail: </b>	tauhid_cuet@cuet.ac.bd, tauhid03_cuet@yahoo.co.uk
            <br><b>Contact: </b><hr>
            <b>Research Interests:</b>
            Mobile Computing,  Data Mining,  Natural Language Processing
            <hr>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Education :</b></h4>
            <ul>
                <li><b>B.Sc.</b><br>Computer Science<br>Chittagong University Of Engineering & Technology<b>(CUET)</b><br>Bangladesh</li>
            </ul>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Related Courses(Undergraduate):</b></h4>
            <div class="col-md-6">
                <ul>
                    <li>Operating System</li>

                </ul>
            </div>
            <div class="col-md-6">
                <ul>
                    <li> 	Database Management System</li>

                </ul>
            </div>
        </div>

        <div class="col-sm-12 nopadding" style="background: linear-gradient( white ,#afd9ee, #afd9ee);">
            <h4><b>Publications :</b></h4>
            <h5 align="center"><b>Journals </b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>Anik Saha, Dipanjan Das Roy, Tauhidul Alam and Kaushik Deb, “Automated Road Lane Detection for Intelligent Vehicles”, Global Journal of Computer Science & Technology(GJCST), USA, Vol. 12, Issue 6 (Ver. 1), pp. 10-15, March, 2012.</li>
                <li>Kaushik Deb, Tauhidul Alam, Md. Mohammad Ali and Kang-Hyun Jo, “Lossless Image Compression Technique Based on Snake Ordering”, Computer Science & Engineering Journal, Bangladesh vol. 7, pp. 30-36, 2011.</li>
            </ul>

            <h5 align="center"><b>Conferences</b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>T. Alam, S. A. Majumder, M. K. Akon, I. M. Bokhary, “OpenStreetMap for Promoting Tourism in Bangladesh”, ICACE, Bangladesh, 11-12, December, 2012.</li>
                <li>Khandaker Mustakimur Rahman, Tauhidul Alam and Mahfuzulhoq Chowdhury, “Location Based Early Disaster Warning and Evacuation System on Mobile Phones Using OpenStreetMap”, IEEE Conference on Open Systems, Malaysia, 21-22, October, 2012.</li>
            </ul>

        </div>




    </div>

    <div class="footer" style="margin-top: 1100px; height:90px; background-color: #afd9ee; ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>

</div>



</body>
</html>