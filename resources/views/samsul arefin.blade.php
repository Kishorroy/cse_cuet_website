<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MD. samsul arefin</title>


    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    .container{
        height: 1600px;
        width: 1350px;
    }

    .header{
        height: 200px;
        background: white; /* For browsers that do not support gradients */
        background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
        background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
        background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
        background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
    }
    .navbar-default{
        background: linear-gradient(white ,#afd9ee , #afd9ee);
    }
    .nopadding{
        padding: 0;
        margin-top: 30px;
        background: linear-gradient(white ,white , #afd9ee);
    }
    .nopadding h4{
        background: linear-gradient( #afd9ee,white , #afd9ee);
        padding: 10px 15px;  
    }

    .nopadding ul li{

        background: url("/public/images/arrow.jpg") no-repeat left -1.6rem;
        padding-left: 10px;
        background-size: 25px 35px;
        margin: 15px 17px;
    }




    
</style>
</head>
<body>

<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Admission</a></li>
                    <li class="active"><a href="faculty.blade.php">Faculty Members</a></li>
                    <li><a href="#">Notice Board</a></li>
                    <li><a href="#">Upcoming Events</a></li>
                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>
                    <li><a href="#">Alumni</a></li>
                    <li><a href="contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>
    </div>

    <div class="col-lg-10" style="margin-left: 100px ">
        <div class="col-md-4">
            <img src="{{URL::asset('/images/teachers/dh.jpg')}}" style="width: 300px;height: 300px;margin-top: 20px">
        </div>
        <div class="col-md-8">
            <h1 style="margin-bottom: 0">Dr. MD Samsul Arefin</h1>
            <br>
            <h2 style="margin: 0;padding: 0;">ডঃ মোঃ শামসুল আরেফিন</h2><hr>
            <h4>Professor & Head of the department</h4>
            <b>E-mail:</b> sarefin@cuet.ac.bd, sarefin_406@yahoo.com
            <br><b>Contact:</b><hr>
            <b>Research Interests:</b>
                Data Privacy and Data Mining, Distributed Systems and Cloud Computing,Big Data Management,Semantic web,Multilingual Data Management ,Object Oriented System Development
            <hr>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Education :</b></h4>
            <ul>
                <li><b>B.Sc.</b><br>Computer Science<br>Khulna University<br>Bangladesh</li>
                <li><b>M.Sc.</b><br>Computer Science<br>Bangladesh University Of Enggineering And Technology<b>(BUET)</b><br>Bangladesh</li>
                <li><b>PhD</b><br>Computer Science<br>Hiroshima University, <br>Japan</li>
            </ul>
        </div>
        <div class="col-sm-12 nopadding">
            <h4><b>Related Courses(Postgraduate):</b></h4>
            <ul>
                <li>Data Mining</li>
                <li>Cloud Computing</li>
                <li>Advanced Database Systems</li>
                <li>Parallel and Distributed Systems</li>
            </ul>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Related Courses(Undergraduate):</b></h4>
            <div class="col-md-6">
                <ul>
                    <li>Information Security and Control</li>
                    <li>Database Systems</li>
                    <li>Computer Networks</li>
                    <li>Algorithms Analysis and Design</li>
                    <li>Object Oriented Programming</li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul>
                    <li>Artificial Intelligence</li>
                    <li> Operating Systems</li>
                    <li>Data Structures</li>
                    <li> Computer Basics and Programming</li>
                    <li>Computer Graphics</li>
                </ul>
            </div>
        </div>
        <div class="col-sm-12 nopadding">
        <h4><b>Reviewer :</b></h4>
        <ul>
            <li>KSII Transactions on Internet and Information Systems (ISSN: 1976-7277).</li>
            <li>International Journal of Computer Science & Information Technology (ISSN: 0975 – 3826)</li>
            <li>Computer Science and Engineering Research Journal (ISSN: 1990-4010).</li>
            <li>International Conference on Electrical & Electronic Engineering (ICEEE 2015), to be held during 4-6 November, 2015 at RUET.</li>
            <li>International Conference on Computer and Information Engineering (ICCIE 2015), to be held during 16-17 November at RUET</li>
            <li>2nd International Conference on Electrical Engineering and Information Communication Technology (ICEEICT 2015).</li>
            <li>9th International Forum on Strategic Technology (IFOST 2014) organized by CUET, Bangladesh.</li>
            <li>National Conference on Electrical & Communication Engineering and Renewable Energy (ECERE 2014) organized by Dept. of EEE, CUET.</li>
            <li>National Conference on Intelligent Computing and Information Technology (2013) organized by Dept. of CSE, CUET, Bangladesh</li>
        </ul>
    </div>
        <div class="col-sm-12 nopadding" style="background: linear-gradient( white ,#afd9ee, #afd9ee);">
            <h4><b>Publications :</b></h4>
            <h5 align="center"><b>Journals </b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>Mohammad Shamsul Arefin, Zhichao Chang, and Yasuhiko Morimoto, "Recommending Hotels by Social Conditions of Locations",	Intelligent Systems Reference Library (Springer, ISSN: 1868-4394), pp. 91-106, 2015</li>
                <li>Linkon Chowdhury and Mohammad Shamsul Arefin, "A Statistical Framework for Detecting Diabetes Types",	Smart Computing Review (ISSN: 2234-4624), vol. 5, no. 2, 2015</li>
                <li>Mohammad Shamsul Arefin, Khondoker Nazibul Hossain, and Yasuhiko Morimoto,"Efficiency of NoSQL Databases under a Moderate Application Load,	LNCS 8999, Springer, pp. 213–227, 2015.</li>
                <li>Md. Osman Goni, Mohammad Shamsul Arefin, and Mohammad Monirul Islam, "Developing a Framework for Determining Students Excellence and Recommending Field of Study", Under Review, Smart Computing Review, South Korea.</li>
                <li>Mohammad Shamsul Arefin, Rahma Bintey Mufiz Mukta, and Yasuhiko Morimoto,"Agent-Based Privacy Aware Feedback System",	LNCS 8933, Springer, pp. 725-738, 2014.</li>
                <li>Mohammad Shamsul Arefin, Geng Ma, and Yasuhiko Morimoto,"A Spatial Skyline Query for a Group of Users", Journal of Software (ISSN: 1796-217X, Academy Publisher) vol. 9, no. 11, pp. 2938-2947, 2014.</li>
                <li>Mohammad Shamsul Arefin, and Yasuhiko Morimoto, "Privacy Aware Parallel Computation of Skyline Sets Queries from Distributed Databases", Computing and Informatics (ISSN:1335-9150), vol. 33, no. 4, pp. 831-856	2014.</li>
                <li>Mohammad Shamsul Arefin, Xu Jinhao, Chen Zhiming, and Yasuhiko Morimoto, "Skyline Query for Selecting Spatial Objects by Utilizing Surrounding Objects", Accepted, Journal of Computers, ISSN: 1796-203X</li>
                <li>Mohammad Shamsul Arefin, Yasuhiko Morimoto, and Mohammad Amir Sharif, “BAENPD: A Bilingual Plagiarism Detector”, Accepted, Journal of Computers. ISSN: 1796-203X</li>
                <li>Mohammad Shamsul Arefin, Xu Jinhao, Chen Zhiming, and Yasuhiko Morimoto,"Skyline Query for Selecting Spatial Objects by Utilizing Surrounding Objects",	Journal of Computers (ISSN: 1796-203X, Academy Publisher), vol.8, no. 7, pp. 1742-1749	2013.</li>
                <li>Y. Morimoto, M. S. Arefin and M. A. Siddique, “Agent-based Anonymous Skyline Set Computation in Cloud Databases”, Int. J. Computational Science and Engineering, Vol. 7, No. 1, pp. 73-81, 2012. (ISSN: 1742-7193)</li>
                <li>M. S. Arefin and Y. Morimoto, “Management of Multilingual Contents in Web Environment”, International Journal of Computing Communication and Networking Research, Vol. 1, Issue-1, pp. 18-37, 2012. (ISSN: 1839-7212).</li>
                <li>Nur Mohammad, Md. Asiful Islam, Abdul Khaleque and M. S. Arefin, “Dispersion Control Characteristics of the Dielectric Cylindrical Waveguide And a Novel Approach of Dispersion Compensation”, International Journal of Engineering and Technology, 2010.</li>
                <li>Dr. Abu Sayed Md. Latiful Hoque and Mohammad Shamsul Arefin, “Multilingual Data Management in Database Environment”, Malaysian Journal of Computer Science (ISSN 0127-9084) , Vol. 22, No. 1, pp 44-63, 2009.</li>
                <li>M. S. Arefin, C. Sheel and M. M. Islam, “Development and Implementation of an Efficient Database Compression Algorithm”, Computer Science and Engineering Research Journal (ISSN: 1990-4010), Vol. 5, pp 23-31, 2008.</li>
                <li>M .M. Hoque, M. S. Arefin and M. O. Rahman, “Semantic Analysis and Transfer: An Experience with the Bangla Natural Language Sentences”, Computer Science and Engineering Research Journal (ISSN: 1990-4010), Vol. 3, pp. 17-23, 2005.</li>
                <li>M. S. Arefin, R. Paul, M. M. Hoque and M. A. Sarif, “Developing an Intelligent Agent to Support Negotiation in an Electronic Market Place”, Computer Science and Engineering Research Journal (ISSN: 1990-4010), Vol. 3, pp. 30-37, 2005.</li>
                <li>M. S. Arefin,“Designing a 34-Segment Display for Bengali and English Numerical Digits and Characters”, Asian Journal of Information Technology (ISSN: 1682-3915), Vol. 4, No. 5, pp. 478-484, 2005.</li>
            </ul>


            <hr>
            <h5 align="center"><b>Conferences</b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                <li>Md. Rakibul Hasan, Mohammad Shamsul Arefin, and Md. Rezaul Karim, "Developing a Framework for Searching and Browsing over Bilingual Domain",	10th International Forum on Strategic Technology (IFOST 2015), June 3-5, 2015.</li>
                <li>M. Moniruzzaman, L. Alam, M. M. Hoque, "Design an Expressive Intelligent to Interacting with Humans", In proc. of Ist International Conference on Physics for Sustainable Development and Technology, Chittagong, Bangladesh, 2015</li>
                <li>Tanjila Aftab Khanam, Mohammad Shamsul Arefin and Md. Shahedul Islam, "Developing a Framework for Analysing Web Data to Generate Recommendation", Accepted for 1st International Conference on Computer & Information Engineering, RUET, Rajshahi, Bangladesh.</li>
                <li>Md. Sanjidul Hoque, Mohammad Shamsul Arefin, and Mohammad Moshiul Hoque, "BQAS: A Bilingual Question Answering System", Accepted for 2nd International Conference on Electrical Information and Communication Technology to be held on Dec. 10-12, 2015.</li>
                <li>Samiur Rahman, Mohammad Shamsul Arefin, and Mohammed Moshiul Hoque, "Developing a Framework for Translation Among Different Local Languages of Bangla",	9th International Forum on Strategic Technology (IFOST 2014), pp. 30-33.	2014.</li>
                <li>Md. Fazla Rabbi Opu, Mohammad Shamsul Arefin, and Yasuhiko Morimoto, "Developing a Framework for Ontology Generalization",	3rd International Conference on Informatics, Electronics and Vision, pp. 1-4,	2014.</li>
                <li>Mohammad Shamsul Arefin and Yasuhiko Morimoto, "Skyline Queries for Sets of Spatial Objects by Utilizing Surrounding Environments", LNCS 7813, Springer, pp. 293-309.</li>
                <li>Mohammad Shamsul Arefin, Mohammad Amir Sharif, Yasuhiko Morimoto, "BEBS: A Framework for Bilingual Summary Generation", accepted, 2nd International Conference on Informatics, Electronics & Vision (IEEE Conf.)</li>
                <li>Ma Geng, Mohammad Shamsul Arefin and Morimoto Yasuhiko, "A Spatial Skyline Query for a Group of Users Having Different Positions", The Third International Conference on Networking and Computing (IEEE, DBLP), pp. 137-142, 2012</li>
                <li>Chen Hongbo, Chen Zhiming, Mohammad Shamsul Arefin and Yasuhiko Morimoto, "Place Recommendation from Check-in Spots on Location-Based Online Social Networks", Third International Conference on Networking and Computing (IEEE, DBLP), pp. 143-148, Japan, 2012.</li>
                <li>Chen Zhiming, Mohammad Shamsul Arefin and Yasuhiko Morimoto, "Skyline Queries for Spatial Objects: A Method for Selecting Spatial Objects Based on Surrounding Environments", Third International Conference on Networking and Computing (IEEE, DBLP), pp. 215-220, Japan, 2012</li>
                <li>Xu Jinhao, Mohammad Shamsul Arefin, Chen Zhiming and Yasuhiko Morimoto, "Real Estate Recommender: Location Query for Selecting Spatial Objects", Third International Conference on Networking and Computing (IEEE, DBLP), pp. 278-282, Japan, 2012</li>
                <li>Mohammad Shamsul Arefin and Yasuhiko Morimoto, "Skyline Sets Queries from Databases with Missing Values", 22nd International Conference on Computer Theory and Applications (IEEE), pp. 24-29, Egypt, 2012</li>
                <li>Mohammad Shamsul Arefin, Yasuhiko Morimoto, Chen Zhiming, "Developing a Framework for Generating Bilingual English-Bangla Dictionaries from Parallel Text Corpus", International Conference on Electrical, Computer and Telecommunication Engineering, pp. 549-552, Bangladesh, 2012.</li>
                <li>Yasuhiko Morimoto, Md. Anisuzzaman Siddique, and Md. Shamsul Arefin, "Information Filtering by Using Materialized Skyline View", LNCS 7108, pp. 179–189, 2011.</li>
                <li>Mohammad Shamsul Arefin and Yasuhiko Morimoto, "Privacy Aware Parallel Computation of Skyline Sets Queries from Distributed Databases", Second International Conference on Networking and Computing (IEEE, DBLP), pp. 186-192, Japan, 2011.</li>
                <li>M. S. Arefin, Y. Morimoto, and A. Yasmin, "Multilingual Content Management in Web Environment", International Conference on Information Science and Applications (ICISA), pp. 1-9, South Korea, 2011.</li>
                <li>M. S. Arefin, Y. Morimoto, and M. A. Sharif, "Bilingual plagiarism detector", 14th International Conference on Computer and Information Technology (ICCIT), Bangladesh, 2011</li>
                <li>Y. Morimoto, M. A. Siddique, and M. S. Arefin, "Agent-Based Convex Skyline Set Query for Cloud Computing Environment", 5th International Workshop on Data-Mining and Statistical Science, March 29-30, 2011, Japan.</li>
                <li>Abdullah Al Farook, Mohammad Shamsul Arefin and Md. Moshiul Hoque, “Structural Operational Semantics of Packages in Java”, In the proceedings of 11th International Conference on Computer & Information Technology (ICCIT 2008) (IEEE Conf.), pp. 40-45, December 25-27, 2008, Khulna, Bangladesh.</li>
                <li>13.	Mohammad Sarwar Kamal, Mohammed Moshiul Hoque, Md. Monjur Ul Hasan and Mohammad Shamsul Arefin, “Bangla Vowel Sign Recognition by Extracting the Fuzzy Features”, in the proceedings of 11th International Conference on Computer & Information Technology (ICCIT 2008), pp. 306-311, December 25-27, 2008, Khulna, Bangladesh. (IEEE Catalog Number: CFP0817D, ISBN: 978-1-4244-2136-7, Library of Congress: 2008900703)</li>
                <li>Mohammed Moshiul Hoque, Md Mokaddem Ahamad, Mohammad Shamsul Arefin, Md. Monjur-Ul-Hasan and Md. Gahangir Hossain, “An Empirical Framework for Translating of Phrasal Verbs of English Sentence into Bangla”, in the proceedings of 11th International Conference on Computer & Information Technology (ICCIT 2008), pp. 183-188, December 25-27, 2008, Khulna, Bangladesh. (IEEE Catalog Number: CFP0817D, ISBN: 978-1-4244-2136-7, Library of Congress: 2008900703)</li>
                <li>M. M. Hoque, M. R. Karim, M. G. Hossain, Mohammad Shamsul Arefin and M. M. Hasan, “Bangla Numeral Recognition Engine (BNRE)”, in the proceedings of 5th International Conference on Electrical & Computer Engineering, pp. 644-647, December 20-22, 2008, Dhaka, Bangladesh. (IEEE Catalog Number: CFP0868A-PRT, ISBN: 978-1-4244-2014-8, Library of Congress: 2007943881)</li>
                <li>S. M. Mahfuzur Rahman, M. S. Islam, M. S. Arefin and M. T. Islam, “Control of a Mobile Robot Through Voice User Interface”, In the proceedings of 4th BSME-ASME International Conference on Thermal Engineering, pp. 873-877, 27 – 29 December, 2008, Dhaka, Bangladesh.</li>
                <li>S. Azad and M. S. Arefin, “Bangla Documents Analyzer”, In the proceedings of International Conference on Electronics, Computer and Communication, pp. 438-442, June 27 – 29, 2008, Rajshahi, Bangladesh. (ISBN: 984-300-002131-3)</li>
                <li>M. S. Arefin, R. Karim, G. B. Aziz and S. M. Zakaria, “Current Mobile Message Editing Analysis and Adopting Customizing Facility in SMS”, In the proceedinds of International Conference on Electronics, Computer and Communication, pp. 154-157, June 27 – 29, 2008, Rajshahi, Bangladesh. (ISBN: 984-300-002131-3)</li>
                <li>Sarifunnahar, M. S. Arefin and M. R. Ahmed, “Implementation of Intelligent Feature in Bangla”, In the proceedinds of International Conference on Electronics, Computer and Communication, pp. 154-157, June 27 – 29, 2008, Rajshahi, Bangladesh. (ISBN: 984-300-002131-3)</li>
                <li>M. A. A. Dewan, M. S. Arefin, M. A. Ullah and O. Chae, “Automatic Extraction of Features from Retinal Fundus Image”, In the proceeding of International Conference on Information and Communication Technology, pp. 47-51, March 7 – 9, 2007, Dhake, Bangladesh. (ISBN: 984-32-3394-8, INSPEC Accession Number: 9737809).</li>
                <li>M. S. Arefin, A. N. M. Rezaul Karim, A. S. M. Kayes and M. A. Alam, “A New Approach for Inter_BSC Handover Process in GSM Network”, In the proceeding of National Conference on Electronics, Information and Telecommunication, pp. 127 (Abstract only), June 29 – 30, 2007, Rajshahi, Bangladesh.</li>
                <li>M. S. Arefin and M. S. Rahman, “Developing an Algorithm for Sharing Semantic Knowledge on Peer to Peer Semantic Web Environment”, In the proceeding of 9th International Conference on Computer and Information Technology (ICCIT06), December 21-23, 2006, Dhaka, Bangladesh.</li>
                <li>M. M. Hoque, M. K. Hossian, M. M. S. Kowsar, K. Deb and M. S. Arefin, “An Empirical Framework for Network Intrusion Detection using Fuzzy Logic”, In the proceeding of 9th International Conference on Computer and Information Technology, December 21-23, 2006, Dhaka, Bangladesh.</li>


            </ul>

        </div>




    </div>

    <div class="footer" style="margin-top: 4700px; height:90px; background-color: #afd9ee; ">
        <br>
        <p style="color: white;"><center><b> Copyright &#169; Department of CSE, CUET| 2017</b> </center></p>

    </div>

</div>



</body>
</html>