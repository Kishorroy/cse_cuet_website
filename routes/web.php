<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home1');
});

Route::get('/sign_out','ProfileController@index');


Route::get('/TeacherRegistration','admin@teacherRegsForm');
Route::post('/TeacherRegistrationS','admin@teacherRegs');


Route::get('/faculty.blade.php', function () {
    return view('faculty');
});

Route::get('/notice.blade.php', function () {
    return view('notice');
});

Route::get('/upcomingevent.blade.php', function () {
    return view('upcomingevent');
});

Route::get('/home1.blade.php', function () {
    return view('home1');
});
Route::get('/Abu Hasnat Mohammad Ashfak Habib.blade.php', function () {
    return view('Abu Hasnat Mohammad Ashfak Habib');
});
Route::get('/admission.blade.php', function () {
    return view('admission');
});
Route::get('/Animesh Chandra Roy.blade.php', function () {
    return view('Animesh Chandra Roy');
});
Route::get('/alam.blade.php', function () {
    return view('alam');
});
Route::get('/research.blade.php', function () {
    return view('research');
});
Route::get('/research1.blade.php', function () {
    return view('research1');
});
Route::get('/research2.blade.php', function () {
    return view('research2');
});
Route::get('/research3.blade.php', function () {
    return view('research3');
});
Route::get('/research4.blade.php', function () {
    return view('research4');
});
Route::get('/research5.blade.php', function () {
    return view('research5');
});
Route::get('/research6.blade.php', function () {
    return view('research6');
});
Route::get('/research7.blade.php', function () {
    return view('research7');
});
Route::get('/research8.blade.php', function () {
    return view('research8');
});
Route::get('/research9.blade.php', function () {
    return view('research9');
});
Route::get('/research10.blade.php', function () {
    return view('research10');
});
Route::get('/vc.blade.php', function () {
    return view('vc');
});
Route::get('/dh.blade.php', function () {
    return view('dh');
});
Route::get('/tista.blade.php', function () {
    return view('tista');
});
Route::get('/tanzina.blade.php', function () {
    return view('tanzina');
});
Route::get('/shayla.blade.php', function () {
    return view('shayla');
});
Route::get('/priyam.blade.php', function () {
    return view('priyam');
});
Route::get('/sujan.blade.php', function () {
    return view('sujan');
});
Route::get('/mukta.blade.php', function () {
    return view('mukta');
});
Route::get('/contact.blade.php', function () {
    return view('contact');
});
Route::get('/obaidur.blade.php', function () {
    return view('obaidur');
});
Route::get('/moshiul.blade.php', function () {
    return view('moshiul');
});

Route::get('/moka.blade.php', function () {
    return view('moka');
});
Route::get('/pranab.blade.php', function () {
    return view('pranab');
});
Route::get('/phd.blade.php', function () {
    return view('phd');
});
Route::get('/undergraduate.blade.php', function () {
    return view('undergraduate');
});
Route::get('/kamal.blade.php', function () {
    return view('kamal');
});
Route::get('/thomas.blade.php', function () {
    return view('thomas');
});
Route::get('/Asaduzzaman.blade.php', function () {
    return view('Asaduzzaman');
});
Route::get('/Kaushik Deb.blade.php', function () {
    return view('Kaushik Deb');
});
Route::get('/Farzana Yasmin.blade.php', function () {
    return view('Farzana Yasmin');
});
Route::get('/Ibrahim Khan.blade.php', function () {
    return view('Ibrahim Khan');
});
Route::get('/Jibon Naher.blade.php', function () {
    return view('Jibon Naher');
});
Route::get('/samsul arefin.blade.php', function () {
    return view('samsul arefin');
});
Route::get('/Lamia Alam.blade.php', function () {
    return view('Lamia Alam');
});
Route::get('/Mahfuzulhoq Chowdhury.blade.php', function () {
    return view('Mahfuzulhoq Chowdhury');
});
Route::get('/master.blade.php', function () {
    return view('master');
});
Route::get('/Enamul Hoque Prince.blade.php', function () {
    return view('Enamul Hoque Prince');
});
Route::get('/Iqbal Hasan Sarker.blade.php', function () {
    return view('Iqbal Hasan Sarker');
});
Route::get('/Monjur-Ul-Hasan.blade.php', function () {
    return view('Monjur-Ul-Hasan');
});
Route::get('/Monjurul Islam.blade.php', function () {
    return view('Monjurul Islam');
});
Route::get('/Sabir Hossain.blade.php', function () {
    return view('Sabir Hossain');
});
Route::get('/Shafiul Alam Forhad.blade.php', function () {
    return view('Shafiul Alam Forhad');
});
Route::get('/Saki Kowsar.blade.php', function () {
    return view('Saki Kowsar');
});
Route::get('/class_routine.blade.php', function () {
    if(Auth::user()!=null){
        return redirect('/sign_out');
    }else
    return view('class_routine');
});
Route::get('/add_routine.blade.php', function () {
    return view('add_routine');
});
Route::get('/routine_form.blade.php', function () {
    return view('routine_form');
});

/*level one term one section A*/

Route::get('Oneone/create',function (){

    return view('Oneone/create');
});
Route::post('/Oneone/store', ['uses'=>'OneoneController@store']);
Route::get('/Oneone/index', 'OneoneController@index')->name('Oneoneroutine');
Route::get('/Oneone/index1', 'OneoneController@index1');
Route::get('/Oneone/edit/{id}', ['uses'=>'Oneonecontroller@view4Edit']);
Route::post('/Oneone/update', ['uses'=>'Oneonecontroller@update']);
Route::get('/Oneone/delete/{id}', ['uses'=>'Oneonecontroller@delete']);

/*level one term one section B*/

Route::get('/Oneoneb/create',function (){

    return view('Oneoneb/create');
});
Route::post('/Oneoneb/store', ['uses'=>'OneonebController@store']);
Route::get('/Oneoneb/index', 'OneonebController@index')->name('Oneonebroutine');
Route::get('/Oneoneb/index1', 'OneonebController@index1');
Route::get('/Oneoneb/edit/{id}', ['uses'=>'Oneonebcontroller@view4Edit']);
Route::post('/Oneoneb/update', ['uses'=>'Oneonebcontroller@update']);
Route::get('/Oneoneb/delete/{id}', ['uses'=>'Oneonebcontroller@delete']);

/*level one term two section A*/

Route::get('/Onetwoa/create',function (){

    return view('Onetwoa/create');
});
Route::post('/Onetwoa/store', ['uses'=>'OnetwoaController@store']);
Route::get('/Onetwoa/index', 'OnetwoaController@index')->name('Onetwoaroutine');
Route::get('/Onetwoa/index1', 'OnetwoaController@index1');
Route::get('/Onetwoa/edit/{id}', ['uses'=>'Onetwoacontroller@view4Edit']);
Route::post('/Onetwoa/update', ['uses'=>'Onetwoacontroller@update']);
Route::get('/Onetwoa/delete/{id}', ['uses'=>'Onetwoacontroller@delete']);


/*level one term two section B*/

Route::get('/Onetwob/create',function (){

    return view('Onetwob/create');
});
Route::post('/Onetwob/store', ['uses'=>'OnetwobController@store']);
Route::get('/Onetwob/index', 'OnetwobController@index')->name('Onetwobroutine');
Route::get('/Onetwob/index1', 'OnetwobController@index1');
Route::get('/Onetwob/edit/{id}', ['uses'=>'Onetwobcontroller@view4Edit']);
Route::post('/Onetwob/update', ['uses'=>'Onetwobcontroller@update']);
Route::get('/Onetwob/delete/{id}', ['uses'=>'Onetwobcontroller@delete']);

/*level Two term one section A*/

Route::get('/Twoonea/create',function (){

    return view('Twoonea/create');
});
Route::post('/Twoonea/store', ['uses'=>'TwooneaController@store']);
Route::get('/Twoonea/index', 'TwooneaController@index')->name('Twoonearoutine');
Route::get('/Twoonea/index1', 'TwooneaController@index1');
Route::get('/Twoonea/edit/{id}', ['uses'=>'Twooneacontroller@view4Edit']);
Route::post('/Twoonea/update', ['uses'=>'Twooneacontroller@update']);
Route::get('/Twoonea/delete/{id}', ['uses'=>'Twooneacontroller@delete']);

/*level Two term one section A*/

Route::get('/Twooneb/create',function (){

    return view('Twooneb/create');
});
Route::post('/Twooneb/store', ['uses'=>'TwoonebController@store']);
Route::get('/Twooneb/index', 'TwoonebController@index')->name('Twoonebroutine');
Route::get('/Twooneb/index1', 'TwoonebController@index1');
Route::get('/Twooneb/edit/{id}', ['uses'=>'Twoonebcontroller@view4Edit']);
Route::post('/Twooneb/update', ['uses'=>'Twoonebcontroller@update']);
Route::get('/Twooneb/delete/{id}', ['uses'=>'Twoonebcontroller@delete']);

/*level Two term Two section A*/

Route::get('/Twotwoa/create',function (){

    return view('Twotwoa/create');
});
Route::post('/Twotwoa/store', ['uses'=>'TwotwoaController@store']);
Route::get('/Twotwoa/index', 'TwotwoaController@index')->name('Twotwoaroutine');
Route::get('/Twotwoa/index1', 'TwotwoaController@index1');
Route::get('/Twotwoa/edit/{id}', ['uses'=>'Twotwoacontroller@view4Edit']);
Route::post('/Twotwoa/update', ['uses'=>'Twotwoacontroller@update']);
Route::get('/Twotwoa/delete/{id}', ['uses'=>'Twotwoacontroller@delete']);


/*level Two term Two section B*/

Route::get('/Twotwob/create',function (){

    return view('Twotwob/create');
});
Route::post('/Twotwob/store', ['uses'=>'TwotwobController@store']);
Route::get('/Twotwob/index', 'TwotwobController@index')->name('Twotwobroutine');
Route::get('/Twotwob/index1', 'TwotwobController@index1');
Route::get('/Twotwob/edit/{id}', ['uses'=>'Twotwobcontroller@view4Edit']);
Route::post('/Twotwob/update', ['uses'=>'Twotwobcontroller@update']);
Route::get('/Twotwob/delete/{id}', ['uses'=>'Twotwobcontroller@delete']);

/* Level 4...&.......*/
Route::get('/ThreeoneA/create',function (){

    return view('ThreeoneA/create');
});

Route::post('/ThreeoneA/store', ['uses'=>'ThreeoneAcontroller@store']);

Route::get('/ThreeoneA/edit/{id}', ['uses'=>'ThreeoneAcontroller@view4Edit']);
Route::get('/ThreeoneA/delete/{id}', ['uses'=>'ThreeoneAcontroller@delete']);

Route::get('/ThreeoneA/index', 'ThreeoneAcontroller@index')->name('ThreeoneAroutine');
Route::get('/ThreeoneA/index1', 'ThreeoneAcontroller@index1');

Route::post('/ThreeoneA/update', ['uses'=>'ThreeoneAcontroller@update']);



Route::get('/ThreeoneB/create',function (){

    return view('ThreeoneB/create');
});

Route::post('/ThreeoneB/store', ['uses'=>'ThreeoneBcontroller@store']);

Route::get('/ThreeoneB/edit/{id}', ['uses'=>'ThreeoneBcontroller@view4Edit']);
Route::get('/ThreeoneB/delete/{id}', ['uses'=>'ThreeoneBcontroller@delete']);

Route::get('/ThreeoneB/index', 'ThreeoneBcontroller@index')->name('ThreeoneBroutine');
Route::get('/ThreeoneB/index1', 'ThreeoneBcontroller@index1');

Route::post('/ThreeoneB/update', ['uses'=>'ThreeoneBcontroller@update']);





Route::get('/ThreetwoA/create',function (){

    return view('ThreetwoA/create');
});

Route::post('/ThreetwoA/store', ['uses'=>'ThreetwoAcontroller@store']);

Route::get('/ThreetwoA/edit/{id}', ['uses'=>'ThreetwoAcontroller@view4Edit']);
Route::get('/ThreetwoA/delete/{id}', ['uses'=>'ThreetwoAcontroller@delete']);

Route::get('/ThreetwoA/index', 'ThreetwoAcontroller@index')->name('ThreetwoAroutine');
Route::get('/ThreetwoA/index1', 'ThreetwoAcontroller@index1');

Route::post('/ThreetwoA/update', ['uses'=>'ThreetwoAcontroller@update']);




Route::get('/ThreetwoB/create',function (){

    return view('ThreetwoB/create');
});

Route::post('/ThreetwoB/store', ['uses'=>'ThreetwoBcontroller@store']);

Route::get('/ThreetwoB/edit/{id}', ['uses'=>'ThreetwoBcontroller@view4Edit']);
Route::get('/ThreetwoB/delete/{id}', ['uses'=>'ThreetwoBcontroller@delete']);

Route::get('/ThreetwoB/index', 'ThreetwoBcontroller@index')->name('ThreetwoBroutine');
Route::get('/ThreetwoB/index1', 'ThreetwoBcontroller@index1');

Route::post('/ThreetwoB/update', ['uses'=>'ThreetwoBcontroller@update']);




Route::get('/FouroneA/create',function (){

    return view('FouroneA/create');
});

Route::post('/FouroneA/store', ['uses'=>'FouroneAcontroller@store']);

Route::get('/FouroneA/edit/{id}', ['uses'=>'FouroneAcontroller@view4Edit']);
Route::get('/FouroneA/delete/{id}', ['uses'=>'FouroneAcontroller@delete']);

Route::get('/FouroneA/index', 'FouroneAcontroller@index')->name('FouroneAroutine');
Route::get('/FouroneA/index1', 'FouroneAcontroller@index1');

Route::post('/FouroneA/update', ['uses'=>'FouroneAcontroller@update']);



Route::get('/FouroneB/create',function (){

    return view('FouroneB/create');
});

Route::post('/FouroneB/store', ['uses'=>'FouroneBcontroller@store']);

Route::get('/FouroneB/edit/{id}', ['uses'=>'FouroneBcontroller@view4Edit']);
Route::get('/FouroneB/delete/{id}', ['uses'=>'FouroneBcontroller@delete']);

Route::get('/FouroneB/index', 'FouroneBcontroller@index')->name('FouroneBroutine');
Route::get('/FouroneB/index1', 'FouroneBcontroller@index1');

Route::post('/FouroneB/update', ['uses'=>'FouroneBcontroller@update']);




Route::get('/FourtwoA/create',function (){

    return view('FourtwoA/create');
});

Route::post('/FourtwoA/store', ['uses'=>'FourtwoAcontroller@store']);

Route::get('/FourtwoA/edit/{id}', ['uses'=>'FourtwoAcontroller@view4Edit']);
Route::get('/FourtwoA/delete/{id}', ['uses'=>'FourtwoAcontroller@delete']);

Route::get('/FourtwoA/index', 'FourtwoAcontroller@index')->name('FourtwoAroutine');
Route::get('/FourtwoA/index1', 'FourtwoAcontroller@index1');

Route::post('/FourtwoA/update', ['uses'=>'FourtwoAcontroller@update']);




Route::get('/FourtwoB/create',function (){

    return view('FourtwoB/create');
});

Route::post('/FourtwoB/store', ['uses'=>'FourtwoBcontroller@store']);

Route::get('/FourtwoB/edit/{id}', ['uses'=>'FourtwoBcontroller@view4Edit']);
Route::get('/FourtwoB/delete/{id}', ['uses'=>'FourtwoBcontroller@delete']);

Route::get('/FourtwoB/index', 'FourtwoBcontroller@index')->name('FourtwoBroutine');
Route::get('/FourtwoB/index1', 'FourtwoBcontroller@index1');

Route::post('/FourtwoB/update', ['uses'=>'FourtwoBcontroller@update']);





Route::get('/Research/create',function (){

    return view('Research/create');
});
Route::post('/Research/store', ['uses'=>'ResearchController@store']);
Route::get('/Research/index', 'ResearchController@index')->name('Research');
Route::get('/Research/edit/{id}', ['uses'=>'Researchcontroller@view4Edit']);
Route::post('/Research/update', ['uses'=>'Researchcontroller@update']);
Route::get('/Research/delete/{id}', ['uses'=>'Researchcontroller@delete']);
/* Level 4...&.......*/

Auth::routes( );

Route::get('/home', 'HomeController@index')->name('home');
